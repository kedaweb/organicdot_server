//服務
app.factory('apiService', ['$http', function($http) {
    var doRequest = function(args) {
        return $http({
            method: 'GET',
            url: ApiUrl + args[0],
            params: args[1]
        }).error(function(data, status) {
            if(typeof(data) == "object") {
                alert(JSON.stringify(data));
            }else{
                alert(data);
            }
        });
    }
    var doRequestJsonp = function(args) {
        return $http.jsonp(ApiUrl + args[0] + '/?callback=JSON_CALLBACK', {
            params: args[1]
        });
    }
    return {
        getCateogry: function() {
            return doRequestJsonp(arguments);
            // return doRequest(arguments);
        },
        getRecommend: function() {
            // return doRequestJsonp(arguments);
            return doRequest(arguments);
        },
        getRec: function(num) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'products/rec/',
                params: {number: num}
            }).error(function(data,status) {
                alert('Fail: ' + status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getProduct: function() {
            // return doRequestJsonp(arguments);
            return doRequest(arguments);
        },
        getCateogryProduct: function() {
            return doRequest(arguments);
        },
        getMember: function (id, phone) {
            return $http({
                method: 'POST',
                url: ApiUrl + 'member/id/'+id,
                params: {phone: phone}
            }).success(function(data, status){            
                $api.setStorage('member', data.root);
            }).error(function(data,status) {
                alert('Fail: ' + status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },        
        signin: function (loginID, pwd) {
            return $http({
                method: 'POST',
                url: ApiUrl + 'auth/login',
                params: {email: loginID, password: pwd}
            }).error(function(data,status) {
                alert('Fail: ' + status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        createFavorites: function (pid, mid) {
            return $http({
                method: 'PUT',
                url: ApiUrl + 'member/favorites',
                params: {product_id: pid, member_id: mid}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        delFavorites: function (pid, mid) {
            return $http({
                method: 'DELETE',
                url: ApiUrl + 'member/favorites',
                params: {product_id: pid, member_id: mid}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },        
        getSysConfig: function () {
            return $http({
                method: 'GET',
                url: ApiUrl + 'sys/config',
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        createOrder: function (cart, order) {
            var newCart = [];
            angular.forEach(cart, function (item,key) {
                var data = {
                    name_cht: item.name_cht,
                    id: item.id,
                    qty: item.qty,
                    price: item.price
                }
                newCart.push(data);
            });
            return $http({
                method: 'PUT',
                url: ApiUrl + 'order/create',
                params:{"cart[]": newCart, order: order}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        updateOrder: function (order) {
            return $http({
                method: 'POST',
                url: ApiUrl + 'order/update',
                params:{order: order}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getShippingTime: function () {
            var arg = [];
            arg[0] = 'sys/shipping-time';
            return doRequestJsonp(arg).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getMyOrders: function (mid) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'member/orders',
                params:{member_id: mid}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getMyFavorites: function (mid) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'member/favorites',
                params:{member_id: mid}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },        
        getFarms: function () {
            return $http({
                method: 'GET',
                url: ApiUrl + 'farms',
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },        
        getFarmDetail: function (id) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'farms/id/' + id
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },        
        getCateogryById: function (id) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'category/id/' + id
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getPickup: function (id) {
            return $http({
                method: 'GET',
                url: ApiUrl + 'sys/pickup',
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        reg: function (regData) {
            regData.language = $api.getStorage('language');
            return $http({
                method: 'POST',
                url: ApiUrl + 'auth/reg',
                params:regData
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        editProfile: function (data) {
            return $http({
                method: 'POST',
                url: ApiUrl + 'member/profile',
                params:data
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        forgetPwd: function (email, locale) {
            return $http({
                method: 'POST',
                url: ApiUrl + 'auth/forget-password',
                params:{email: email,locale: locale}
            }).error(function(data, status) {
                alert(status);
                if(typeof(data) == "object") {
                    alert(JSON.stringify(data));
                }else{
                    alert(data);
                }
            });
        },
        getPaypalToken: function (cart, shipping_method, locale) {
            var newCart = [];
            angular.forEach(cart, function (item,key) {
                var data = {
                    name_cht: item.name_cht,
                    name_en: item.name_en,
                    id: item.id,
                    qty: item.qty,
                    price: item.price
                }
                newCart.push(data);
            });
            var arg = [];
            arg[0] = 'paypal/token';
            arg[1] = {'cart[]': newCart, shipping_method: shipping_method, locale: locale, returnurl: '/'+locale+"/paypal-return", cancelurl: '/'+locale+"/cart"};
            return doRequestJsonp(arg);
        },
        getOrderById: function (id) {
            var arg = [];
            arg[0] = 'order/id/'+id;
            return doRequestJsonp(arg);
        },
        getOrderByToken: function (token) {
            var arg = [];
            arg[0] = 'order/token/'+token;
            return doRequestJsonp(arg);
        },
        getLanguage: function (language) {
            var arg = [];
            arg[0] = 'sys/language-json/';
            arg[1] = {language: language}
            return doRequestJsonp(arg);
        },
    };
}]);