var pageParam;
var winHeight;

function toggleSlideBar() {
    $('.sidebar').toggleClass('active');
    $('.sidebar .tab-pane.active').removeClass('active');
    $('.sidebar .nav li.active a').attr('aria-expanded', 'false');
    $('.sidebar .nav li.active').removeClass('active');
}

function closeSlideBar() {
    $('.sidebar').removeClass('active');
    $('.sidebar .tab-pane.active').removeClass('active');
    $('.sidebar .nav li.active a').attr('aria-expanded', 'false');
    $('.sidebar .nav li.active').removeClass('active');
    $('.sidebar .tab-content').css("width", "0");
}

function getWinHeight(){
    return api.winHeight;
}

function apiAlert (title, msg, btns, callback) {
    // var functionCallback, btnsArr;
    // var lang = locale =='cn' ? language.cn : language.en;
    // btnsArr = btns ? btns : [lang.confirmBtn];
    
    // if(typeof(callback) == "function"){
    //     functionCallback = callback;
    // }else{
    //     functionCallback = function(ret,err){}
    // }

    alert(msg);
}

function openMainWin (winName, url, param) {
    api.openWin({
        name: winName,
        url: url,
        pageParam: param,
        bounces: false,
        reload: true,
        delay: 500,
        bgColor: '#FFFFFF',
        showProgress: true,
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: 'auto'
        }
    });
}

function OpenNavWin(winName, url, isReload) {
    closeSlideBar();
    isReload = isReload == undefined ? true : false;
    api.openWin({
        name: winName,
        url: url,
        bounces: false,
        reload: isReload,
        bgColor: '#FFFFFF',
        showProgress: true,
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: 'auto'
        }
    });
}

function closeWin(name) {
    window.location = '/cart';
}

function OpenCart(winName, url) {
    closeSlideBar();
    openMainWin(winName, url);
}

function openFrame (frmName, url, height) {
    height = height ? height : 0;
    api.openFrame({
        name: frmName,
        url: url,
        rect:{
            x:0,
            y:height,
            w:'auto',
            h:'auto'
        },
        showProgress: true,
        reload: true,
        bounces: false,
    });
}

function closeApp(){
    api.addEventListener({
        name: 'keyback'
    }, function(ret, err){
        var lang = $api.getStorage('language')=='cn' ? language.cn : language.en;
        api.confirm({
            title: lang.tip_title,
            msg: lang.msg4,
            buttons: [lang.confirmBtn, lang.cancelBtn]
        },function(ret,err){
            if(ret.buttonIndex == 1){
                $api.rmStorage('member');
                $api.rmStorage('cart');
                $api.rmStorage('currentOrder');
                api.closeWidget({silent:true});
            }else{
                return;
            }
        });
    });
}

function openApp(url) {
    openUrl = 'http://' + url.replace(/^http:\/\//i, '');
    api.openApp({
        androidPkg : 'android.intent.action.VIEW',
        mimeType : 'text/html',
        uri : openUrl
    }, function(ret, err) {

    });
}
function openAppForHttps (url) {
    api.openApp({
        androidPkg : 'android.intent.action.VIEW',
        mimeType : 'text/html',
        uri : url
    }, function(ret, err) {

    });
}