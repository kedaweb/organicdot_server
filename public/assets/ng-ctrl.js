var isDevEnv = false;
var domain_link = isDevEnv ? 'organicdot.app/' : 'organicdot.com/';
var domain = '/';
var ApiUrl = domain + 'api/';
var app = angular.module('myApp', ['ngSanitize']);

//全局
app.run(['$rootScope', 'apiService', function($rootScope, apiService) {
    $rootScope.facebookAppId = "146809412337845";
    $rootScope.locale = $api.getStorage('language') ? $api.getStorage('language') : 'en';
    $rootScope.member = $api.getStorage('member') ? $api.getStorage('member') : "";
    $rootScope.isLogin = $api.getStorage('member') ? true : false;
    
    $rootScope.categories = [];
    apiService.getCateogry('category', {
        parent_id: 0
    }).success(function(data, status) {
        $rootScope.categories = data.root;
    });
    
    $rootScope.islanguageLoaded = false;
    $rootScope.language = {};
    apiService.getLanguage($rootScope.locale).success(function(data, status) {
        $rootScope.language = data.root;
        $rootScope.islanguageLoaded = true;
    });

    $rootScope.openCategoryWin = function(cid) {
        closeSlideBar();
        $api.setStorage('category_id', cid);
        window.location = "/category/" + cid;
    };
    $rootScope.openProduct = function(pid) {
        closeSlideBar();
        $api.setStorage('product_id', pid);
        window.location = "/product/" + pid;
    };

    $rootScope.selectLocale = function(value) {
        $rootScope.locale = value ? value : "cn";
        apiService.getLanguage($rootScope.locale).success(function(data, status) {
            $rootScope.language = data.root;
        });
        $api.setStorage('language', value);
    };
    $rootScope.logout = function() {
        $api.rmStorage('member');
        $api.rmStorage('cart');
        $api.rmStorage('paypalToken');
        $api.rmStorage('shipping_method');
        $rootScope.isLogin = false;
        $rootScope.member = "";
        window.location = '/recommend';
    }
    $rootScope.openWebPage = function (url) {
        openUrl = 'http://' + url.replace(/^http:\/\//i, '');
        window.open(openUrl);
    }

    //检查货存
    $rootScope.checkStock = function (item, qty) {
        item.stock_tip = "";
        if(qty > item.stock){
            // unalert || apiAlert($rootScope.language.tip_title, $rootScope.language.tip22);
            item.stock_tip = $rootScope.language.tip22;
        }else{
            item.stock_tip = "";
        }
        return qty ? parseInt(qty) : 1;
    }

    $rootScope.minusQty = function (item) {
        var qty = parseInt(item.qty) - 1;
        item.qty = qty <= 0 ? 1 : qty;
        item.qty = $rootScope.checkStock(item, qty);
    }

    $rootScope.plusQty = function (item) {
        console.log(item.qty);
        var qty = parseInt(item.qty) + 1;
        item.qty = $rootScope.checkStock(item, qty);
    }
}]);

//頁面控制器
app.controller('OpeningStepCtrl', ['$scope', function($scope) {
    $api.rmStorage('member');
    $api.rmStorage('cart');
    $api.rmStorage('paypalToken');
    $api.rmStorage('shipping_method');
}]);
app.controller('FrmIndexCtrl', ['$scope', 'apiService', function($scope, apiService) {
    apiService.getRecommend('products/recommend/').success(function(data, status) {
        $scope.recommend = data.root;
    });
}]);

app.controller('CateogryCtrl', ['$scope', 'apiService', function($scope, apiService) {
    var category_id = $api.getStorage('category_id');

    if(category_id){    
        apiService.getCateogryById(category_id).success(function(data, status) {
            $scope.category = data.root;
        });
    }
}]);

app.controller('FrmContactCtrl', ['$scope', 'apiService', function($scope, apiService) {
}]);

app.controller('FrmPPCtrl', ['$scope', 'apiService', function($scope, apiService) {

}]);

app.controller('LoginCtrl', ['$scope', 'apiService', '$http',function($scope, apiService, $http) {
    $scope.isChecked = $api.getStorage('rememberMe') == 'true' ? true : false;
    $scope.loginID = $api.getStorage('loginID');
    apiService.getRec(4).success(function(data, status) {
        $scope.recommend = data.root;
    });
    $scope.login = function () {
        if($scope.loginID && $scope.password){
            apiService.signin($scope.loginID, $scope.password).success(function(data, status){
                if(data.success){                
                    $api.setStorage('member', data.root);
                    window.location = "/recommend";
                }else{
                    apiAlert("", $scope.language.tip2)
                }
            });
        }else{        
            apiAlert("", $scope.language.tip1);
        }
    }
    $scope.rememberMe = function () {
        if($scope.isChecked){
            $api.setStorage('rememberMe', true);
            $api.setStorage('loginID', $scope.loginID);
        }else{
            $api.setStorage('rememberMe', false);
            $api.setStorage('loginID', '');
        }
    }
    var watch = $scope.$watch('loginID',function(newValue,oldValue, scope){
        if($scope.isChecked) {
            $api.setStorage('loginID', $scope.loginID);
        }
    });
}]);
app.controller('regCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.errorInfo;
    $scope.reg = {};

    $scope.send = function () {
        apiService.reg($scope.reg).success(function(data, status) {
            if(data.success){
                apiAlert("", $scope.language.tip9);
                $api.setStorage('member', data.root);
                window.location = "/recommend";
            }else{
                $scope.errorInfo = data.info;
                $('#errorInfo').modal('show');
            }
        });
    }
}]);


app.controller('FrmPaypalCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.isHideMenu = true;
    $scope.isHideCart = true;
    $scope.token = $api.getStorage('paypalToken');
    $scope.shipping_method = $api.getStorage('shipping_method');

    $scope.backHome =function(){
        window.location = "/recommend";
    }

    $scope.nextProcees = function(){    
        $api.rmStorage('cart');
        if($scope.shipping_method == 1){
            window.location = "/shipping";
        }else if($scope.shipping_method == 2){
            window.location = "/pickup";
        }else{
            window.location = "/helpyourself";
        }
    }

    $scope.backCart = function(){
        apiService.getOrderByToken($scope.token).success(function(data, status) {
            $scope.order = data.root;
            //检查订单是否存在
            if($scope.order){
                $scope.nextProcees();
            }else{    
                window.location = "/cart";
            }
        });
    }
}]);

app.controller('shippingCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.order = undefined;
    $scope.sendBtnDisable = true;
    $scope.token = $api.getStorage('paypalToken');
    $scope.isReg = false;
    $scope.isHideMenu = true;
    $scope.isHideCart = true;
    $scope.sameAdressCheckbox = false;
    $scope.payment_method = $api.getStorage('payment_method');

    apiService.getShippingTime().success(function(data, status) {
        $scope.shippingTimes = data.root;
    });

    //检查表单运货时段是否已选
    $scope.checkSTID = function(){
        var bool = false;
        if($scope.order){        
            angular.forEach($scope.order.shipping_time_id, function(item, key){
                if(item){ 
                    bool = true;
                    return; 
                }
            });
        }
        return bool;
    }

    var getOrderByToken = function () {
        apiService.getOrderByToken($scope.token).success(function(data, status) {
            $scope.order = data.root;
            //检查订单是否存在
            if($scope.order){
                $scope.order.shipping_method = 1;
                if($scope.member.id){    
                    $scope.order.member_id = $scope.member.id;
                    $scope.order.email = $scope.member.email;
                    $scope.order.buyer_name = $scope.member.name;
                    $scope.order.phone = $scope.member.phone;
                    $scope.order.shipping_address = $scope.member.shipping_address;
                }
                $scope.sendBtnDisable = false;
            }else{    
                $scope.sendBtnDisable = true;
                $('#tip15').modal('show');
            }
        });
    }

    $scope.back = function(){
        if($scope.payment_method == 1){            
            if($scope.order){
                $('#tip20').modal('show');
            }else{    
                window.location = "/cart";
            }
        }else{
            window.location = '/cart';
        }
    }

    if($scope.payment_method == 1){
        getOrderByToken();
    }else{
        $scope.order = {};
        $scope.order.status = 4;
        $scope.order.shipping_method = 1;
        $scope.order.payment_method = $scope.payment_method;
        $scope.order.member_id = $scope.member.id ? $scope.member.id : null;
        $scope.order.email = $scope.member.email ? $scope.member.email : null;
        $scope.order.buyer_name = $scope.member.name ? $scope.member.name : null;
        $scope.order.phone = $scope.member.phone ? $scope.member.phone : null;
        $scope.order.shipping_address = $scope.member.shipping_address ? $scope.member.shipping_address : null;
        $scope.order.shipping_time_id = [];
        $scope.sendBtnDisable = false;
    }

    $scope.sameAdress = function (event) {
        var checkbox = event.target;
        $scope.order.bill_address = checkbox.checked ? $scope.order.shipping_address : "";
    }

    $scope.showLoginForm = function(){
        $('#loginModal').modal('show');
    }

    $scope.loginByModal = function () {
        if($scope.loginID && $scope.password){
            apiService.signin($scope.loginID, $scope.password).success(function(data, status){
                if(data.success){                
                    $('#loginModal').modal('hide');
                    $api.setStorage('member', data.root);
                    $scope.member = data.root;
                    $scope.isLogin = true;
                    $scope.isReg = false;
                    $scope.sameAdressCheckbox = false;
                    if($scope.order){
                        $scope.order.member_id = $scope.member.id;
                        $scope.order.email = $scope.member.email;
                        $scope.order.buyer_name = $scope.member.name;
                        $scope.order.phone = $scope.member.phone;
                        $scope.order.shipping_address = $scope.member.shipping_address;
                    }else{
                        $('#loginSuccess').modal('show');
                    }
                }else{
                    apiAlert("", $scope.language.tip2);
                }
            });
        }else{        
            apiAlert("", $scope.language.tip1);
        }
    }


    $scope.updateOrder = function(){
        if($scope.order){
            if($scope.payment_method == 1){            
                apiService.updateOrder($scope.order).success(function(data, status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }else{
                apiService.createOrder($api.getStorage('cart'), $scope.order).success(function (data,status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }
        }else{        
            window.location = "/recommend";
            return;
        }
    }

    $scope.sendOrder = function () {
        if($scope.isReg){
            //註冊資料
            var reg = {};
            reg.email = $scope.order.email;
            reg.name = $scope.order.buyer_name;
            reg.phone = $scope.order.phone;
            reg.password = 123456;
            reg.password_confirmation = 123456;
            reg.shipping_address = $scope.order.shipping_address;

            apiService.reg(reg).success(function(data, status) {
                if(data.success){
                    apiAlert("", $scope.language.tip9);
                    $api.setStorage('member', data.root);
                    $scope.order.member_id = data.root.id;
                    $scope.updateOrder();
                }else{
                    $scope.errorInfo = data.info;
                    $('#errorInfo').modal('show');
                }
            });
        }else{
            $scope.updateOrder();
        }
    }
}]);
app.controller('pickupCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.pickups;
    $scope.order = undefined;
    $scope.sendBtnDisable = true;
    $scope.token = $api.getStorage('paypalToken');
    $scope.isReg = false;
    $scope.isHideMenu = true;
    $scope.isHideCart = true;
    $scope.sameAdressCheckbox = false;
    $scope.payment_method = $api.getStorage('payment_method');

    apiService.getPickup().success(function(data, status) {
        $scope.pickups = data.root;
    });

    var getOrderByToken = function () {
        apiService.getOrderByToken($scope.token).success(function(data, status) {
            $scope.order = data.root;
            //检查订单是否存在
            if($scope.order){
                $scope.order.shipping_method = 2;
                if($scope.member.id){    
                    $scope.order.member_id = $scope.member.id;
                    $scope.order.email = $scope.member.email;
                    $scope.order.buyer_name = $scope.member.name;
                    $scope.order.phone = $scope.member.phone;
                    $scope.order.shipping_address = $scope.member.shipping_address;
                }
                $scope.sendBtnDisable = false;
            }else{    
                $scope.sendBtnDisable = true;
                $('#tip15').modal('show');
            }
        });
    }

    $scope.back = function(){
        if($scope.payment_method == 1){            
            if($scope.order){
                $('#tip20').modal('show');
            }else{    
                window.location = "/cart";
            }
        }else{
            window.location = '/cart';
        }
    }

    if($scope.payment_method == 1){
        getOrderByToken();
    }else{
        $scope.order = {};
        $scope.order.status = 4;
        $scope.order.shipping_method = 2;
        $scope.order.payment_method = $scope.payment_method;
        $scope.order.member_id = $scope.member.id ? $scope.member.id : null;
        $scope.order.email = $scope.member.email ? $scope.member.email : null;
        $scope.order.buyer_name = $scope.member.name ? $scope.member.name : null;
        $scope.order.phone = $scope.member.phone ? $scope.member.phone : null;
        $scope.order.shipping_address = $scope.member.shipping_address ? $scope.member.shipping_address : null;
        $scope.order.shipping_time_id = [];
        $scope.sendBtnDisable = false;
    }

    $scope.sameAdress = function (event) {
        var checkbox = event.target;
        $scope.order.bill_address = checkbox.checked ? $scope.order.shipping_address : "";
    }

    $scope.showLoginForm = function(){
        $('#loginModal').modal('show');
    }

    $scope.loginByModal = function () {
        if($scope.loginID && $scope.password){
            apiService.signin($scope.loginID, $scope.password).success(function(data, status){
                if(data.success){                
                    $('#loginModal').modal('hide');
                    $api.setStorage('member', data.root);
                    $scope.member = data.root;
                    $scope.isLogin = true;
                    $scope.isReg = false;
                    $scope.sameAdressCheckbox = false;
                    if($scope.order){
                        $scope.order.member_id = $scope.member.id;
                        $scope.order.email = $scope.member.email;
                        $scope.order.buyer_name = $scope.member.name;
                        $scope.order.phone = $scope.member.phone;
                        $scope.order.shipping_address = $scope.member.shipping_address;
                    }else{
                        $('#loginSuccess').modal('show');
                    }
                }else{
                    apiAlert("", $scope.language.tip2);
                }
            });
        }else{        
            apiAlert("", $scope.language.tip1);
        }
    }


    $scope.updateOrder = function(){
        if($scope.order){
            if($scope.payment_method == 1){            
                apiService.updateOrder($scope.order).success(function(data, status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }else{
                apiService.createOrder($api.getStorage('cart'), $scope.order).success(function (data,status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }
        }else{        
            window.location = "/recommend";
            return;
        }
    }

    $scope.sendOrder = function () {
        if($scope.isReg){
            //註冊資料
            var reg = {};
            reg.email = $scope.order.email;
            reg.name = $scope.order.buyer_name;
            reg.phone = $scope.order.phone;
            reg.password = 123456;
            reg.password_confirmation = 123456;
            reg.shipping_address = $scope.order.shipping_address;

            apiService.reg(reg).success(function(data, status) {
                if(data.success){
                    apiAlert("", $scope.language.tip9);
                    $api.setStorage('member', data.root);
                    $scope.order.member_id = data.root.id;
                    $scope.updateOrder();
                }else{
                    $scope.errorInfo = data.info;
                    $('#errorInfo').modal('show');
                }
            });
        }else{
            $scope.updateOrder();
        }
    }
}]);
app.controller('helpYourselfCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.order = undefined;
    $scope.sendBtnDisable = true;
    $scope.token = $api.getStorage('paypalToken');
    $scope.isReg = false;
    $scope.isHideMenu = true;
    $scope.isHideCart = true;
    $scope.sameAdressCheckbox = false;
    $scope.payment_method = $api.getStorage('payment_method');

    var getOrderByToken = function () {
        apiService.getOrderByToken($scope.token).success(function(data, status) {
            $scope.order = data.root;
            //检查订单是否存在
            if($scope.order){
                $scope.order.shipping_method = 3;
                if($scope.member.id){    
                    $scope.order.member_id = $scope.member.id;
                    $scope.order.email = $scope.member.email;
                    $scope.order.buyer_name = $scope.member.name;
                    $scope.order.phone = $scope.member.phone;
                    $scope.order.shipping_address = $scope.member.shipping_address;
                }
                $scope.sendBtnDisable = false;
            }else{    
                $scope.sendBtnDisable = true;
                $('#tip15').modal('show');
            }
        });
    }

    $scope.back = function(){
        if($scope.payment_method == 1){            
            if($scope.order){
                $('#tip20').modal('show');
            }else{    
                window.location = "/cart";
            }
        }else{
            window.location = '/cart';
        }
    }

    if($scope.payment_method == 1){
        getOrderByToken();
    }else{
        $scope.order = {};
        $scope.order.status = 4;
        $scope.order.shipping_method = 3;
        $scope.order.payment_method = $scope.payment_method;
        $scope.order.member_id = $scope.member.id ? $scope.member.id : null;
        $scope.order.email = $scope.member.email ? $scope.member.email : null;
        $scope.order.buyer_name = $scope.member.name ? $scope.member.name : null;
        $scope.order.phone = $scope.member.phone ? $scope.member.phone : null;
        $scope.order.shipping_address = $scope.member.shipping_address ? $scope.member.shipping_address : null;
        $scope.order.shipping_time_id = [];
        $scope.sendBtnDisable = false;
    }

    $scope.sameAdress = function (event) {
        var checkbox = event.target;
        $scope.order.bill_address = checkbox.checked ? $scope.order.shipping_address : "";
    }

    $scope.showLoginForm = function(){
        $('#loginModal').modal('show');
    }

    $scope.loginByModal = function () {
        if($scope.loginID && $scope.password){
            apiService.signin($scope.loginID, $scope.password).success(function(data, status){
                if(data.success){                
                    $('#loginModal').modal('hide');
                    $api.setStorage('member', data.root);
                    $scope.member = data.root;
                    $scope.isLogin = true;
                    $scope.isReg = false;
                    $scope.sameAdressCheckbox = false;
                    if($scope.order){
                        $scope.order.member_id = $scope.member.id;
                        $scope.order.email = $scope.member.email;
                        $scope.order.buyer_name = $scope.member.name;
                        $scope.order.phone = $scope.member.phone;
                        $scope.order.shipping_address = $scope.member.shipping_address;
                    }else{
                        $('#loginSuccess').modal('show');
                    }
                }else{
                    apiAlert("", $scope.language.tip2);
                }
            });
        }else{        
            apiAlert("", $scope.language.tip1);
        }
    }


    $scope.updateOrder = function(){
        if($scope.order){
            if($scope.payment_method == 1){            
                apiService.updateOrder($scope.order).success(function(data, status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }else{
                apiService.createOrder($api.getStorage('cart'), $scope.order).success(function (data,status) {
                    if(data.success){
                        $api.rmStorage('cart');
                        $('#msg2').modal('show');
                        $scope.sendBtnDisable = true;
                    }else{
                        apiAlert("", $scope.language.tip7);
                    }
                });
            }
        }else{        
            window.location = "/recommend";
            return;
        }
    }

    $scope.sendOrder = function () {
        if($scope.isReg){
            //註冊資料
            var reg = {};
            reg.email = $scope.order.email;
            reg.name = $scope.order.buyer_name;
            reg.phone = $scope.order.phone;
            reg.password = 123456;
            reg.password_confirmation = 123456;
            reg.shipping_address = $scope.order.shipping_address;

            apiService.reg(reg).success(function(data, status) {
                if(data.success){
                    apiAlert("", $scope.language.tip9);
                    $api.setStorage('member', data.root);
                    $scope.order.member_id = data.root.id;
                    $scope.updateOrder();
                }else{
                    $scope.errorInfo = data.info;
                    $('#errorInfo').modal('show');
                }
            });
        }else{
            $scope.updateOrder();
        }
    }
}]);
app.controller('farmsCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.farms;

    apiService.getFarms().success(function(data, status) {
        $scope.farms = data.root;
    });

    $scope.openFarm = function(id) {
        $api.setStorage('supplier_id', id);
        window.location = "/farm/" + id;
    };
}]);

app.controller('farmDetailCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.farm;
    $scope.isTabActive = [];
    $scope.imgs = [];

    var supplier_id = $api.getStorage('supplier_id');
    if(supplier_id){
        apiService.getFarmDetail(supplier_id).success(function(data, status) {
            $scope.farm = data.root;
            if(data.root.img1){
                $scope.imgs.push({imgsrc: data.root.img1});
            }
            if(data.root.img2){
                $scope.imgs.push({imgsrc: data.root.img2});
            }
            if(data.root.img3){
                $scope.imgs.push({imgsrc: data.root.img3});
            }
            // console.log($scope.imgs);
            angular.forEach($scope.farm.grouped, function(item, key) {
                $scope.isTabActive[key] = '';
            });
        });
    }
    $scope.toggle = function(index) {
        angular.forEach($scope.isTabActive, function(item, key) {
            $scope.isTabActive[key] = index == key ? 'active' : '';
        });
    };
}]);
app.controller('favoritesCtrl', ['$scope', 'apiService', function($scope, apiService) {

    if($scope.member.id){    
        apiService.getMyFavorites($scope.member.id).success(function(data, status) {
            $scope.favorites = data.root;
        });
    }

    $scope.cancelCollect = function (pid, index) {
        if(!$scope.member.id){
            apiAlert("", $scope.language.tip3);
            return;
        }
        $scope.favorites.splice(index, 1);
        apiService.delFavorites(pid, $scope.member.id).success(function(data,status){
            apiService.getMember($scope.member.id, $scope.member.phone);
        });
    };
}]);

app.controller('forgetpwdCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.email = "";
    $scope.errorInfo = "";
    
    apiService.getRec(4).success(function(data, status) {
        $scope.recommend = data.root;
    });

    $scope.send = function () {
        apiService.forgetPwd($scope.email, $scope.locale).success(function(data, status) {
            switch (data.error_code) {
                case 0:
                    apiAlert("", $scope.language.tip11);
                    break;
                case 404:
                    apiAlert("", $scope.language.tip12);
                    break;
                default:
                    $scope.errorInfo = data.info;
                    break;            
            }
        });
    }
}]);
app.controller('myOrdersCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.myOrders;

    if($scope.member.id){    
        apiService.getMyOrders($scope.member.id).success(function(data, status) {
            $scope.myOrders = data.root;
            angular.forEach($scope.myOrders, function(order, key) {
                var total = 0;
                $scope.myOrders[key].total = 0;
                angular.forEach(order.order_items, function(item) {
                    total += item.price*item.qty;
                });
                $scope.myOrders[key].total = total;
            });
        });
    }
}]);



app.controller('profileCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.send = function () {
        $scope.selectLocale($scope.locale);
        $scope.member.language = $scope.locale;
        apiService.editProfile($scope.member).success(function(data, status) {
            if(data.success){
                apiAlert("", $scope.language.tip11);
                $api.setStorage('member', data.root);
                window.location = '/' + $scope.locale + "/recommend";
            }else{
                $scope.errorInfo = data.info;
                $('#errorInfo').modal('show');
            }
        });
    }
}]);