function getCheckedBoxValues (inputName) {
    var chk_value = [];
    $('input[name^="'+inputName+'"]:checked').each(function(){
        chk_value.push($(this).val());
    });
    var params = {};
    params[inputName] = chk_value;
    return $.param(params);
}