//組件
app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '1';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function(scope) {
            scope.initCarousel = function(element) {
                var customOptions = scope.$eval($(element).attr('data-options'));
                //当轮换元素等于1个时,取消轮播效果
                if($(element).children().length == 1){
                    customOptions.loop = false;
                    customOptions.autoplay = false;
                }
                // init carousel
                $(element).owlCarousel(customOptions);
            };
        }
    };
}).directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
            // wait for the last item in the ng-repeat then call init
            if (scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);
app.directive('loading', ['$http', function($http) {
    return {
        restrict: 'A',
        link: function(scope, elm, attrs) {
            scope.isLoading = function() {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function(v) {
                if (v) {
                    elm.show();
                } else {
                    setTimeout(function(){
                        elm.hide();
                    }, 2000);
                }
            });
        }
    };
}]);

app.directive('tabA', [function() {
    return {
        restrict: 'A',
        link: function(scope, elm, attrs) {
            $(elm).on("click", function(e){
                e.preventDefault();
                console.log('tab');
                var target = $(this).attr('href');
                $(target).parent().css("width", "100%");
                $(this).tab('show');
            })
        }
    };
}]);