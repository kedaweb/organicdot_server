<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class OrganicdotSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // 系统资料
        Schema::create('system_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('value',255);
            $table->timestamps();
        });

        // 会员
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',128)->nullable();
            $table->string('email',128)->nullable();
            $table->string('phone',32)->nullable();
            $table->string('areacodes',32)->nullable();
            $table->string('password',255)->nullable();
            $table->text('shipping_address')->nullable();
            $table->text('bill_address')->nullable();
            $table->timestamps();
        });

        // 会员密码重置
        Schema::create('member_pwd_resets', function (Blueprint $table) {
            $table->string('email',255);
            $table->string('token',255);
            $table->timestamps();
        });


        // 类别table
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->integer('sorting');
            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->boolean('is_show')->default(1);
            $table->string('img', 255)->nullable();
            $table->timestamps();
            
        });

        // 认证
        Schema::create('certification', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type');
            $table->integer('sorting');
            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->boolean('is_show')->default(1);
            $table->string('img', 255)->nullable();
            $table->timestamps();

            
        });

        // 提货地址
        Schema::create('pickup_location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->string('address_en',255)->nullable();
            $table->string('address_cht',255)->nullable();
            $table->boolean('is_show')->default(1);
            $table->timestamps();

            
        });

        // 送货时段
        Schema::create('shipping_time', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->boolean('is_show')->default(1);
            $table->timestamps();
        });

        // 供应商
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->string('address_en',255)->nullable();
            $table->string('address_cht',255)->nullable();
            $table->string('content_en',255)->nullable();
            $table->string('content_cht',255)->nullable();
            $table->string('website',255)->nullable();
            $table->string('facebook',255)->nullable();
            $table->string('img1',255)->nullable();
            $table->string('img2',255)->nullable();
            $table->string('img3',255)->nullable();
            $table->boolean('is_show')->default(1);
            $table->integer('sorting')->default(1);
            $table->text('cert_ids')->nullable();
            $table->timestamps();

            
        });

        // 产品
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sorting')->default(1);
            $table->decimal('price',10,2);
            $table->decimal('shop_price',10,2);            

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('supplier_id')->unsigned();

            $table->tinyInteger('stock_status')->default(1); //1:尚有货存,0:補貨中
            $table->integer('stock')->unsigned();
            
            $table->text('cert_ids')->nullable();
            $table->tinyInteger('recommend')->default(1); //今日推介

            $table->string('img1',255)->nullable();
            $table->string('img2',255)->nullable();
            $table->string('img3',255)->nullable();

            $table->string('name_en',128)->nullable();
            $table->string('name_cht',128)->nullable();
            $table->string('weight_unit_en',16)->nullable();
            $table->string('weight_unit_cht',16)->nullable();

            $table->text('content_en')->nullable();
            $table->text('content_cht')->nullable();
            $table->text('content_choose_en')->nullable();
            $table->text('content_choose_cht')->nullable();
            $table->text('content_nutrition_en')->nullable();
            $table->text('content_nutrition_cht')->nullable();
            $table->text('content_effect_en')->nullable();
            $table->text('content_effect_cht')->nullable();
            $table->text('content_save_en')->nullable();
            $table->text('content_save_cht')->nullable();
            $table->text('shipping_detail_en')->nullable();
            $table->text('shipping_detail_cht')->nullable();

            $table->boolean('is_show')->default(1);
            $table->timestamps();

        });

        // 订单
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(1); //订单状态 0:已取消 1:处理中 2:搁置中 3:已完成 4:待支付
            $table->string('buyer_name',128)->nullable();
            $table->string('phone',32)->nullable();
            $table->integer('member_id')->nullable();
            $table->string('bill_address',255)->nullable();
            $table->string('shipping_address',255)->nullable();
            $table->tinyInteger('shipping_method'); //运送方式 1:一般运费 2:到提货点 3:自取
            $table->string('shipping_fee',255)->nullable(); //运费
            $table->string('shipping_time_id', 128)->nullable(); //运送时间
            $table->integer('delivery_id')->nullable(); //提货地址
            $table->datetime('buy_date');
            $table->string('paypal_token',255)->nullable();
            $table->string('payer_id',255)->nullable();
            $table->tinyInteger('is_paid')->default(0); //订单支付状态 0:未支付 1:已支付;
            $table->tinyInteger('payment_method')->default(1); //订单支付方式 1:paypal 2:COD 3:银行付款;
            $table->timestamps();
        });

        // 订单内物品
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('product_id')->unsigned();

            $table->string('product_name');

            $table->integer('qty');
            $table->decimal('price',10,2);
            $table->timestamps();
        });

        //用戶收藏夾
       Schema::create('favorites', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('order_items');
        Schema::drop('orders');
        Schema::drop('products');
        Schema::drop('system_config');
        Schema::drop('members');
        Schema::drop('category');
        Schema::drop('certification');
        Schema::drop('pickup_location');
        Schema::drop('shipping_time');
        Schema::drop('suppliers');
    }
}
