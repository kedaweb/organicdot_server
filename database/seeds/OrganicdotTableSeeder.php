<?php

use Illuminate\Database\Seeder;

class OrganicdotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function()
        {

            DB::table('system_config')->delete();
            DB::table('system_config')->insert(['name' => 'shipping_fee','value' => '10']);
            DB::table('system_config')->insert(['name' => 'shipping_discount','value' => '10']);

            DB::table('users')->delete();
            DB::table('users')->insert(
                ['username' => 'admin','email' => 'admin@admin.com','password' => bcrypt('123456')]
            );


            DB::table('roles')->delete();
            DB::table('roles')->insert([
                ['name' => 'superadmin','display_name' => 'superadmin','description' => '超級管理員'],
                ['name' => 'shop_manager','display_name' => 'shop manager','description' => '店鋪管理員'],
                ['name' => 'staff','display_name' => 'staff','description' => '員工']
            ]);

            DB::table('permissions')->delete();
            DB::table('permissions')->insert([
                ['name' => 'users-admin','display_name' => '後臺用戶管理','description' => '帳戶','url' => '/admin/users'],
                ['name' => 'products-admin','display_name' => '產品管理','description' => '產品管理','url' => '/admin/products'],
                ['name' => 'suppliers-admin','display_name' => '供應商管理','description' => '供應商管理','url' => '/admin/suppliers'],
                ['name' => 'category-admin','display_name' => '類別管理','description' => '類別管理','url' => '/admin/category'],
                ['name' => 'cert-admin','display_name' => '專業認證管理','description' => '專業認證','url' => '/admin/cert'],
                ['name' => 'orders-admin','display_name' => '交易資料管理','description' => '交易資料','url' => '/admin/orders'],
                ['name' => 'shippingtime-admin','display_name' => '送貨资料管理','description' => '送貨资料','url' => '/admin/shipping-time'],
                ['name' => 'pickup-admin','display_name' => '提貨地址管理','description' => '提貨地址','url' => '/admin/pickup-location'],
                ['name' => 'members-admin','display_name' => '會員資料管理','description' => '會員資料','url' => '/admin/members'],
                ['name' => 'roles-admin','display_name' => '角色權限管理','description' => '角色權限','url' => '/admin/roles']
            ]);

            DB::table('role_user')->delete();
            DB::table('role_user')->insert(['user_id' => '1','role_id' => '1']);

            DB::table('permission_role')->delete();
            for ($i=1; $i < 11; $i++) { 
                DB::table('permission_role')->insert(['permission_id' => $i,'role_id' => '1']);
            }

            DB::table('category')->delete();
            DB::table('category')->insert([
                ['name_en' => '時令蔬菜(英)', 'name_cht' => '時令蔬菜', 'img' => '/assets/images/ico1.png'],
                ['name_en' => '水果類(英)', 'name_cht' => '水果類', 'img' => '/assets/images/ico2.png'],
                ['name_en' => '糧油副食(英)', 'name_cht' => '糧油副食', 'img' => '/assets/images/ico3.png'],
                ['name_en' => '休閒食品(英)', 'name_cht' => '休閒食品', 'img' => '/assets/images/ico5.png'],
                ['name_en' => '養生食品(英)', 'name_cht' => '養生食品', 'img' => '/assets/images/ico7.png']
            ]);
            DB::table('category')->insert([
                ['name_en' => '蔬菜禮盒(英)', 'name_cht' => '蔬菜禮盒', 'parent_id' => 1],
                ['name_en' => '葉菜類(英)', 'name_cht' => '葉菜類', 'parent_id' => 1],
                ['name_en' => '瓜果類(英)', 'name_cht' => '瓜果類', 'parent_id' => 1],
                ['name_en' => '豆類(英)', 'name_cht' => '豆類', 'parent_id' => 1]
            ]);
            DB::table('shipping_time')->delete();
            DB::table('shipping_time')->insert([
                ['name_en' => '上午 10:00~14:00(英)', 'name_cht' => '上午 10:00~14:00'],
                ['name_en' => '中午 14:00~18:00(英)', 'name_cht' => '中午 14:00~18:00'],
                ['name_en' => '晚上 18:00~20:00(英)', 'name_cht' => '晚上 18:00~20:00']
            ]);
            DB::table('pickup_location')->delete();
            DB::table('pickup_location')->insert([
                ['name_en' => '7-11', 'name_cht' => '7-11','address_en' => 'HK', 'address_cht' => '香港']
            ]);
            DB::table('certification')->delete();
            DB::table('certification')->insert([
                ['name_en' => '產品認證(英)', 'name_cht' => '產品認證','type' => '1', 'img' => '/assets/images/cert2.png'],
                ['name_en' => '供應商認證(英)', 'name_cht' => '供應商認證','type' => '2', 'img' => '/assets/images/cert3.png']
            ]);
        });
    }
}