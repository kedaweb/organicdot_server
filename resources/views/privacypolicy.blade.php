@extends('front_layouts.default')

@section('top_title')
<div class="contact">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.PrivacyPolicy')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="FrmPPCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="contact">
            <div class="row">
                <div class="col-xs-12">
                    {!!trans('front.PrivacyPolicyContent')!!}
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection