@extends('front_layouts.default')

@section('top_title')
<div class="favorites">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.favorites')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="favoritesCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="favorites">
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="item in favorites">
                        <div class="item">
                            <img class="img-responsive ico" ng-src="@{{item.product.img1}}" ng-click="openProduct(item.product.id)"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left" ng-bind="item.product.name_cht"></span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left" ng-bind="item.product.name_en"></span></p>
                            <p class="ovh">                        
                            <span class="pull-left">@{{item.product.price | currency:"$"}}</span>
                            <span class="pull-right" ng-if="locale=='cn'" ng-bind="item.product.weight_unit_cht"></span>
                            <span class="pull-right" ng-if="locale=='en'" ng-bind="item.product.weight_unit_en"></span>
                            </p>
                            <p class="text-center"><button class="btn favorites-btn" ng-click="cancelCollect(item.product.id, $index)"><img src="/image/heart.png">{{trans('front.cancelcollect')}}</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection