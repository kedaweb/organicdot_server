<!DOCTYPE html>
<html>
<head>
<title>{{ 'cn'==$locale ? '密碼重設' : 'Password Reset' }}</title>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
.col-xs-12 p{padding: 15px;font-size: 16px}
</style>
</head>
<body>
<div class="container">
    <div class="row">
        @if ($status == 0)
        <div class="col-xs-12">
            <p class="bg-primary">
                {{ 'cn' == $locale ? '非法請求' : 'Illegal request' }}
            </p>
        </div>
        @elseif ($status == 1) 
        <div class="col-xs-12">
            <p class="bg-primary">
                {{ 'cn' == $locale ? '鏈接失效' : 'Invalid Link' }}
            </p>
        </div>
        @else
        <div class="row" style="margin: 10px 0">
            <div class="col-md-offset-4 col-md-4">
            <img class="img-responsive" src="/assets/images/logo.png" style="margin:0 auto">
            </div>
        </div>
        <div class="col-md-offset-4 col-md-4">
            <form method="post">
                <div class="form-group">
                    <label for="exampleInputPassword1">{{ 'cn' == $locale ? '密碼' : 'Password' }}</label>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{ 'cn' == $locale ? '確認密碼' : 'Confirm Password' }}</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-default">{{ 'cn' == $locale ? '提交' : 'Submit' }}</button>
                <input type="hidden" name="token" value="{{$token}}">
                <input type="hidden" name="email" value="{{$email}}">
            </form>
        </div>
        @endif
        <div class="col-xs-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>
</body>
</html>