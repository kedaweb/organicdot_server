@extends('front_layouts.default')

@section('top_title')
<div class="my-orders">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.orders')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="myOrdersCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="my-orders">
            <div class="orders-box" ng-repeat="order in myOrders">
                <div class="row">
                    <h3>
                        <span class="pull-left">{{trans('front.orderNo')}}: #@{{order.id}}</span>
                        <span class="pull-right">@{{order.buy_date}}</span>
                    </h3>
                </div>
                <div class="row item-list">
                    <ul>
                        <li ng-repeat="item in order.order_items">
                            <span ng-if="locale=='cn'" class="product">@{{item.product.name_cht}}&nbsp;&nbsp;</span>
                            <span ng-if="locale=='en'" class="product">@{{item.product.name_en}}&nbsp;&nbsp;</span>
                            <span class="qty">x @{{item.qty}}</span>
                            <span class="pull-right price">
                                @{{item.price | currency:"$"}}
                                /
                                <span ng-if="locale=='cn'">@{{item.product.weight_unit_cht}}</span>
                                <span ng-if="locale=='en'">@{{item.product.weight_unit_en}}</span>
                            </span>
                        </li>
                    </ul>
                    <p class="order-info pull-left">
                        <span class="pull-left">{{trans('front.payMethod')}}： </span>
                        <img ng-show="order.payment_method==1" style="height:20px;margin-top: 7px;margin-left: 5px;" class="img-responsive pull-left" src="/image/paypal.png">
                        <span ng-show="order.payment_method==2">{{trans('front.COD')}}</span>
                        <span ng-show="order.payment_method==3">{{trans('front.payByBlank')}}</span>
                        <span class="pull-right">{{trans('front.shippingFee')}} @{{order.shipping_fee | currency:"$"}}</span>
                    </p>
                    <p class="order-info pull-left text-right">{{trans('front.total')}} @{{order.total | currency:"$"}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection