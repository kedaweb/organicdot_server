@extends('front_layouts.default')

@section('top_title')
<div class="contact">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.contact')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="FrmContactCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="contact">
            <div class="row">
                <div id="allmap"></div>
                <!-- <img class="img-responsive" src="../image/map.jpg" style="margin-bottom: 10px"> -->
                <div class="col-xs-12">
                    <p>{{trans('front.address')}}：火炭坳背灣街34號豐盛工業中心B座16樓7室</p>
                    <p>{{trans('front.email')}}：<a href="mailto:facebook@organicdot.com">facebook@organicdot.com</a></p>
                    <p>{{trans('front.phone')}}：<a href="tel:+85226868332">+852 26868332</a></p>
                    <p>{{trans('front.website')}}：<a href="#" ng-click="openWebPage('http://organicdot.com')">http://organicdot.com/</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
<script type="text/javascript">
function initMap() {
  var myLatLng = {lat: 22.3996098, lng: 114.1932117};

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('allmap'), {
    center: myLatLng,
    scrollwheel: false,
    zoom: 17
  });

  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: myLatLng,
    title: '有機點(OrganicDot)'
  });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy03muuBN_3Ni6_uA_vck16M6FQ7aRwTU&callback=initMap" async defer></script>
</script>
@endsection