@extends('front_layouts.default')

@section('top_title')
<div class="farms">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.farm')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="farmsCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="farms">
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="farm in farms">
                        <div class="item" ng-click="openFarm(farm.id)">
                            <img class="img-responsive ico" ng-src="@{{farm.img1}}"/>
                            <p class="title products-title" ng-if="locale=='cn'">@{{farm.name_cht}}</p>
                            <p class="title products-title" ng-if="locale=='en'">@{{farm.name_en}}</p>
                            <p class="address" ng-if="locale=='cn'">@{{farm.address_cht}}</p>
                            <p class="address" ng-if="locale=='en'">@{{farm.address_en}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection