@extends('front_layouts.default')

@section('top_title')
<div class="recommend product-detail">
    <h1 class="top-title">
        <img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">
        <span class="product-title" ng-if="locale=='cn'" ng-bind="product.name_cht"></span>
        <span class="product-title" ng-if="locale=='en'" ng-bind="product.name_en"></span>
    </h1>
</div>
@endsection

@section('body')
<body class="product-body" ng-controller="productDetailCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="recommend product-detail">
            <div class="row">
                <data-owl-carousel class="owl-carousel" data-options="{items : 1,dots:true,navRewind:false,autoplay:true,loop:true}">
                    <img class="img-responsive" ng-src="@{{item.imgsrc}}" owl-carousel-item=""  ng-repeat="item in imgs" ng-show="item.imgsrc">
                </data-owl-carousel>
            </div>
            <div class="detail-box">
                <div class="row">     
                    <div class="col-xs-12">     
                        <div class="certs">
                            <ul class="list-unstyled list-inline">
                                <li ng-repeat="cert in product.certs"><img class="img-responsive" ng-src="@{{cert.img}}"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7">                
                        <h4 ng-if="locale=='cn'" ng-bind="product.name_cht"></h4>
                        <h4 ng-if="locale=='en'" ng-bind="product.name_en"></h4>
                    </div>
                    <div class="col-xs-5">                
                        <button ng-if="!isCollect" ng-click="collect(product.id)" class="btn favorites-btn active pull-right"><img src="/image/heart_active.png">{{trans('front.collect')}}</button>
                        <button ng-if="isCollect" ng-click="cancelCollect(product.id)" class="btn favorites-btn pull-right"><img src="/image/heart_active.png">{{trans('front.cancelcollect')}}</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7">                
                        <p class="c777" style="margin-bottom: 5px">
                            <span ng-if="locale=='cn'" ng-bind="product.supplier.name_cht"></span>
                            <span ng-if="locale=='en'" ng-bind="product.supplier.name_en"></span><br>
                            <span ng-if="product.shop_price != 0" style="text-decoration:line-through">{{ trans('front.original_price') }} <span style="text-decoration:line-through" ng-bind="product.price | currency:'$'"></span></span>
                        </p>
                    </div>
                    <div class="col-xs-5">                
                        <p class="red text-right" style="margin: 10px 0">
                            <span ng-if="product.stock_status==1" ng-bind="language.hasStock"></span>
                            <span ng-if="product.stock_status==0" ng-bind="language.soldOut"></span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="cart-item">
                        <div class="ovh item">
                            <div class="col-xs-6">
                                <div class="text-left price">
                                    <span ng-if="product.shop_price == 0" ng-bind="product.price | currency:'$'"></span>
                                    <span ng-if="product.shop_price != 0" ng-bind="product.shop_price | currency:'$'"></span>
                                    /
                                    <span ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                                    <span ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="item-amount text-right" ng-if="product.stock_status==1">
                                    <span class="minus" ng-click="minusQty(product)">-</span>
                                    <input type="text" autocomplete="off" class="text text-amount J_ItemAmount" min="1" step="1" ng-model="product.qty" ng-change='product.qty=checkStock(product,product.qty)' valid-number>
                                    <span class="plus" ng-click="plusQty(product)">+</span>
                                </div>
                                <p class="red text-right" ng-bind="product.stock_tip"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">     
                    <div class="col-xs-4 col-sm-2">
                        <div class="share">
                            <a target="_blank" href="@{{myModel.Url}}" style="display:block"><img class="img-responsive" src="/image/share1.png"></a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-8"></div>
                    <div class="col-xs-4 col-sm-2">
                        <img onclick="show_qrcode()" class="img-responsive" src="/image/show_qrcode.png">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">                
                        <div class="content">
                            {{trans('front.desc')}}：<br>
                            <span ng-if="locale=='cn'" ng-bind-html="product.content_cht"></span>
                            <span ng-if="locale=='en'" ng-bind-html="product.content_en"></span>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px" ng-if="product.stock_status==1">
                    <div class="col-xs-6">                
                        <button class="btn btn-default btn-lg reg-btn col-xs-12" ng-click="addCart()">{{trans('front.addCart')}}</button>
                    </div>
                    <div class="col-xs-6">                
                        <button class="btn btn-default btn-lg submit-btn col-xs-12" ng-click="buyNow()">{{trans('front.buyBtn')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 20px;">
        <div class="other-content" ng-class="isTabActive.one" ng-click="toggle('one')">
            <div class="row">
                <h1 class="top-title"><span class="arrow" ng-class="isTabActive.one"></span>{{trans('front.chooseMethod')}}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12">            
                    <div class="content">
                        <span ng-if="locale=='cn'" ng-bind-html="product.content_choose_cht"></span>
                        <span ng-if="locale=='en'" ng-bind-html="product.content_choose_en"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-content" ng-class="isTabActive.two" ng-click="toggle('two')">
            <div class="row">
                <h1 class="top-title"><span class="arrow" ng-class="isTabActive.two"></span>{{trans('front.nutrition')}}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12">            
                    <div class="content">
                        <span ng-if="locale=='cn'" ng-bind-html="product.content_nutrition_cht"></span>
                        <span ng-if="locale=='en'" ng-bind-html="product.content_nutrition_en"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-content" ng-class="isTabActive.three" ng-click="toggle('three')">
            <div class="row">
                <h1 class="top-title"><span class="arrow" ng-class="isTabActive.three"></span>{{trans('front.efficacy')}}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12">            
                    <div class="content">
                        <span ng-if="locale=='cn'" ng-bind-html="product.content_effect_cht"></span>
                        <span ng-if="locale=='en'" ng-bind-html="product.content_effect_en"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-content" ng-class="isTabActive.four" ng-click="toggle('four')">
            <div class="row">
                <h1 class="top-title"><span class="arrow" ng-class="isTabActive.four"></span>{{trans('front.savingMethod')}}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12">            
                    <div class="content">
                        <span ng-if="locale=='cn'" ng-bind-html="product.content_save_cht"></span>
                        <span ng-if="locale=='en'" ng-bind-html="product.content_save_en"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-content" ng-class="isTabActive.five" ng-click="toggle('five')">
            <div class="row">
                <h1 class="top-title"><span class="arrow" ng-class="isTabActive.five"></span>{{trans('front.shippingDetails')}}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12">            
                    <div class="content">
                        <span ng-if="locale=='cn'" ng-bind-html="product.shipping_detail_cht"></span>
                        <span ng-if="locale=='en'" ng-bind-html="product.shipping_detail_en"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 10px;">
        <div class="related">
            <div class="row">
                <h1 class="top-title2" ng-bind="language.relProduct"></h1>
            </div>
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="product in relproducts">
                        <div class="item" ng-click="openProduct(product.id)">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left" ng-bind="product.name_cht"></span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left" ng-bind="product.name_en"></span></p>
                            <p class="ovh">                        
                            <span class="pull-left" ng-bind="product.price | currency:'$'"></span>
                            <span class="pull-right" ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                            <span class="pull-right" ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="qrcode" class="loading-spiner-holder" style="display: none">
    <div class="loading-spiner text-center">
        <img ng-src="@{{product.qrcode}}">
    </div>
</div>
<div class="modal fade" id="msg3" tabindex="-1" role="dialog" aria-labelledby="msg3Label">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.msg3"></p>
                <br>
                <div class="row">
                    <div class="col-xs-6">                    
                        <button type="button" class="btn btn-default btn-lg submit-btn col-xs-12" data-dismiss="modal">{{trans('front.continueShopping')}}</button>
                    </div>
                    <div class="col-xs-6">                    
                        <button type="button" class="btn btn-default btn-lg submit-btn col-xs-12" ng-click="OpenCart()" >{{trans('front.payNow')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
<script type="text/javascript" src="/assets/carousel-swipe.js"></script>
<script>
$(function () {
    $("#carousel-example-generic").carousel();
})
function show_qrcode () {
    $('#qrcode').show().one('click', function(event) {
        $(this).hide();
    });
}

app.controller('productDetailCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.product, $scope.relproducts;
    $scope.isTabActive = {
        one: "active",
        two: "",
        three: "",
        four: "",
        five: ""
    };
    $scope.isCollect = false;
    $scope.isDisabled = false;
    $scope.token;
    var product_id = $api.getStorage('product_id');

    if (product_id) {
        //检查商品是否已收藏
        if($scope.member.id){    
            angular.forEach($scope.member.favorites, function(item, key) {
                if(item.product_id == product_id) {
                    $scope.isCollect = true;
                    return;
                }
            });
        }
        //获取商品资料
        $scope.imgs = []
        apiService.getProduct('products/id/' + product_id).success(function(data, status) {
            $scope.product = data.root;
            if(data.root.img1){
                $scope.imgs.push({imgsrc: data.root.img1});
            }
            if(data.root.img2){
                $scope.imgs.push({imgsrc: data.root.img2});
            }
            if(data.root.img3){
                $scope.imgs.push({imgsrc: data.root.img3});
            }
            $scope.isDisabled = $scope.product.stock_status==0 ? true : false;
            $scope.product.qty = 1; //购买数量
            $scope.myModel = {
                Url: 'https://www.facebook.com/sharer/sharer.php?u='+domain_link + "api/products/for-facebook/"+$scope.product.id+"/"+$scope.locale
            };
            if (data.root) {
                //获取相关商品
                apiService.getCateogryProduct('products/category/', {category_id: $scope.product.category_id,product_id: $scope.product.id}).success(function(response, status) {
                    $scope.relproducts = response.root;
                });
            }
        });
    };

    $scope.OpenCart = function () {
        window.location = "/cart";
        $('#msg3').modal('hide');
    }

    $scope.toggle = function(key) {
        angular.forEach($scope.isTabActive, function(item, k) {
            $scope.isTabActive[k] = key == k ? 'active' : '';
        });
    };

    $scope.collect = function (pid) {
        if($scope.member == ""){
            apiAlert("",$scope.language.tip3);
            return;
        }
        apiService.createFavorites(pid,$scope.member.id).success(function(data,status){
            $scope.isCollect = true;
            apiService.getMember($scope.member.id, $scope.member.phone);
        });
    };
    $scope.cancelCollect = function (pid) {
        if($scope.member == ""){
            apiAlert("",$scope.language.tip3);
            return;
        }
        apiService.delFavorites(pid,$scope.member.id).success(function(data,status){
            $scope.isCollect = false;
            apiService.getMember($scope.member.id, $scope.member.phone);
        });
    };

    $scope.buyNow = function () {
        $scope.cart = $api.getStorage('cart') ? $api.getStorage('cart') : new Array();
        var exist = false;
        $scope.product.qty = $scope.product.qty > $scope.product.stock ? parseInt($scope.product.stock) : parseInt($scope.product.qty);
        $scope.product.stock_tip = '';
        if($scope.product.qty){
            angular.forEach($scope.cart, function (item, key) {
                if(item.id == $scope.product.id){
                    exist = true;
                    $scope.cart[key].qty += $scope.product.qty;
                    return;
                }
            });

            if(!exist){
                $scope.cart.push($scope.product);
            }
            
            $api.setStorage('cart', $scope.cart);
            $scope.OpenCart();
        }else{
            $scope.product.qty = 1;
            apiAlert("",$scope.language.tip4);
        }
    }

    $scope.addCart = function () {
        $scope.cart = $api.getStorage('cart') ? $api.getStorage('cart') : new Array();
        var exist = false;
        $scope.product.qty = $scope.product.qty > $scope.product.stock ? parseInt($scope.product.stock) : parseInt($scope.product.qty);
        $scope.product.stock_tip = '';
        if($scope.product.qty){
            angular.forEach($scope.cart, function (item, key) {
                if(item.id == $scope.product.id){
                    exist = true;
                    $scope.cart[key].qty += $scope.product.qty;
                    return;
                }
            });

            if(!exist){
                $scope.cart.push($scope.product);
            }
            
            $api.setStorage('cart', $scope.cart);
            $('#msg3').modal('show');
        }else{
            $scope.product.qty = 1;
            apiAlert("",$scope.language.tip4);
        }
    }
}]);
</script>
@endsection