@extends('front_layouts.default')

@section('top_title')
<div class="recommend">
    <h1 class="top-title2" ng-if="locale=='cn'" ng-bind="category.name_cht"></h1>
    <h1 class="top-title2" ng-if="locale=='en'" ng-bind="category.name_en"></h1>
</div>
@endsection

@section('body')
<body ng-controller="CateogryCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="recommend">
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="product in category.products">
                        <div class="item" ng-click="openProduct(product.id)">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left">@{{product.name_cht}}</span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left">@{{product.name_en}}</span></p>
                            <p class="ovh">                        
                            <span class="pull-left"> @{{product.price | currency:"$"}} </span>
                            <span class="pull-right" ng-if="locale=='cn'">@{{product.weight_unit_cht}}</span>
                            <span class="pull-right" ng-if="locale=='en'">@{{product.weight_unit_en}}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection