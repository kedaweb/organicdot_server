<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="有機點後臺">
<meta name="author" content="sam_leung">
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row" style="margin: 10px 0">
        <div class="col-md-offset-4 col-md-4">
        <img class="img-responsive" src="{{url('/assets/images/logo.png')}}" style="margin:0 auto">
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            @if ('cn' == $locale)
                請點擊以下鏈接重設密碼。請點擊以下按鈕重設密碼:
            @else
                To update your password, click the button below:
            @endif
            <br>
            <a class="btn btn-success" href="{{ url('api/auth/reset-pwd?email='.$email.'&token='.$token.'&locale='.$locale) }}" target="_blank">
            @if ('cn' == $locale)
                重置我的密碼
            @else
                Reset my password
            @endif
            </a>
        </div>
        <div class="col-md-offset-4 col-md-4">    
        @if ('cn' == $locale)
            如果點擊無效，請複製下方網頁地址到瀏覽器地址欄中打開:
        @else
            If you click invalid, please copy the URL to the browser address bar.:
        @endif
            <br>
            {{ url('api/auth/reset-pwd?email='.$email.'&token='.$token) }}
        </div>
    </div>
</div>
</body>
</html>