@extends('front_layouts.default')

@section('top_title')
<div class="farms farm-detail">
    <h1 class="top-title">
        <img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">
        <span ng-if="locale=='cn'">@{{farm.name_cht}}</span>
        <span ng-if="locale=='en'">@{{farm.name_en}}</span>
    </h1>
</div>
@endsection

@section('body')
<body class="farms-body" ng-controller="farmDetailCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="farms farm-detail">
            <div class="row">
                <data-owl-carousel class="owl-carousel" data-options="{items : 1,dots:true,navRewind:false,autoplay:true,loop:true}">
                    <img class="img-responsive" ng-src="@{{item.imgsrc}}" owl-carousel-item="" ng-repeat="item in imgs" ng-if="item.imgsrc">
                </data-owl-carousel>
            </div>
            <div class="detail-box">
                <div class="row">
                    <div class="col-xs-12">      
                        <div class="certs">
                            <ul class="list-unstyled">
                                <li ng-repeat="cert in farm.certs"><img class="img-responsive" ng-src="@{{cert.img}}"></li>
                            </ul>
                        </div>
                        <h2 class="title" ng-if="locale=='cn'">@{{farm.name_cht}}</h2>
                        <h2 class="title" ng-if="locale=='en'">@{{farm.name_en}}</h2>         
                    </div>
                    <div class="col-xs-12">                
                        <h4>
                            {{trans('front.address')}}: 
                            <span ng-if="locale=='cn'">@{{farm.address_cht}}</span>
                            <span ng-if="locale=='en'">@{{farm.address_en}}</span>
                        </h4>
                    </div>
                    <div class="col-xs-12" ng-if="farm.website">                
                        <h4>
                            {{trans('front.website')}}: 
                            <a href="#" ng-click="openWebPage(farm.website)">@{{farm.website}}</a>
                        </h4>
                    </div>
                    <div class="col-xs-12" ng-if="farm.facebook">                
                        <h4>
                            facebook: 
                            <a href="#" ng-click="openWebPage(farm.facebook)" >@{{farm.facebook}}</a>
                        </h4>
                    </div>
                    <div class="col-xs-12">                
                        <div class="content">
                            {{trans('front.desc')}}：<br>
                            <span ng-if="locale=='cn'">@{{farm.content_cht}}</span>
                            <span ng-if="locale=='en'">@{{farm.content_en}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 20px;">
        <div class="recommend" ng-repeat="products in farm.grouped" ng-click="toggle($index)">
            <div class="row">
                <h1 class="top-title" ng-if="locale=='cn'"><span class="arrow" ng-init="isTabActive[$index] = $first ? 'active' : '' " ng-class="isTabActive[$index]"></span>@{{products[0].category.name_cht}}</h1>
                <h1 class="top-title" ng-if="locale=='en'"><span class="arrow" ng-init="isTabActive[$index] = $first ? 'active' : '' " ng-class="isTabActive[$index]"></span>@{{products[0].category.name_en}}</h1>
            </div>
            <div class="category-box" ng-init="isTabActive[$index] = $first ? 'active' : '' " ng-class="isTabActive[$index]">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="product in products" ng-click="openProduct(product.id)">
                        <div class="item">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left">@{{product.name_cht}}</span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left">@{{product.name_en}}</span></p>
                            <p class="ovh">                        
                            <span class="pull-left">@{{product.price | currency:"$"}}</span>
                            <span class="pull-right" ng-if="locale=='cn'">@{{product.weight_unit_cht}}</span>
                            <span class="pull-right" ng-if="locale=='en'">@{{product.weight_unit_en}}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection