@extends('front_layouts.default')

@section('top_title')
<div class="cart">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.cart')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="cartCtrl">
@include('front_layouts.nav')
<div id="frm_main">
<div class="container">
    <div class="cart" ng-show="isShowCart()">
        <div class="cart-item" ng-repeat="item in cart track by $index">
            <h4 ng-if="locale=='cn'" ng-bind="item.name_cht"></h4>
            <h4 ng-if="locale=='en'" ng-bind="item.name_en"></h4>
            <div class="ovh item">
                <div class="pull-left item-amount ">
                    <span class="minus" ng-click="cartMinusQty(item)">-</span>
                    <input type="number" autocomplete="off" min="1" class="text text-amount J_ItemAmount" ng-model="item.qty" ng-init='item.qty=checkStock(item,item.qty)'>
                    <span class="plus" ng-click="cartPlusQty(item)">+</span>
                    <p class="red text-left" ng-bind="item.stock_tip"></p>
                </div>
                <div class="pull-left text-right price">
                    <span ng-bind="item.price | currency:'$'"></span>
                    /
                    <span ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                    <span ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                </div>
                <div class="pull-left text-right qty">x<span ng-bind="item.qty"></span></div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <p class="text-right item-sum">
                        <button ng-click="removeItem($index)" class="btn btn-default reg-btn">{{trans('front.delBtn')}}</button>
                        {{trans('front.sum')}} <span ng-bind="item.price*item.qty | currency:'$'"></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="shipping_method">
            <table class="table" style="margin-bottom: 0">
                <tr>
                    <td class="col-xs-4"  style="vertical-align: middle">{{trans('front.shippingMethod')}}:</td>
                    <td class="col-xs-8">
                        <div class="pull-right">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" class="hidden-input" ng-model="shipping_method" ng-click="selectShipping(1)" value="1">
                                    <span></span>
                                    {{trans('front.fare')}} : <span ng-bind="sysconfig.shipping_fee | currency:'$'"></span> <br> ({{trans('front.tip5')}} <span ng-bind="sysconfig.shipping_discount | currency:'$'"></span> {{trans('front.tip5_1')}})
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" class="hidden-input" ng-model="shipping_method" ng-click="selectShipping(2)" value="2">
                                    <span></span>
                                    {{trans('front.deliveryPoint')}} ({{trans('front.free')}})
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" class="hidden-input" ng-model="shipping_method" ng-click="selectShipping(3)" value="3">
                                    <span></span>
                                    {{trans('front.storePickup')}} ({{trans('front.free')}})
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="shipping_method">
            <table class="table" style="margin-bottom: 0">
                <tr>
                    <td class="col-xs-4" style="vertical-align: middle">{{trans('front.payMethod')}}:</td>
                    <td class="">
                        <div class="pull-right">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payRadios" class="hidden-input" ng-model="payment_method" ng-click="selectPayment(1)" value="1">
                                    <span></span>
                                    <img style="height:20px;margin-left: 5px;display: inline" class="img-responsive" src="/image/paypal.png">
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payRadios" class="hidden-input" ng-model="payment_method" ng-click="selectPayment(2)" value="2">
                                    <span></span>
                                    {{trans('front.COD')}}
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payRadios" class="hidden-input" ng-model="payment_method" ng-click="selectPayment(3)" value="3">
                                    <span></span>
                                    {!!trans('front.payByBlank')!!}
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="cart-order">
            <p class="text-right">
                {{trans('front.shippingFee')}}:
                <span ng-bind="shipping_fee | currency:'$'">
            </p>
            <p class="text-right">
            {{trans('front.total')}}:
            <span ng-bind="total() | currency:'$'">
            </p>
            <p class="text-right ovh hidden">
                {{trans('front.payMethod')}}: 
                <img style="height:20px;margin-left: 5px;display: inline" class="img-responsive" src="/image/paypal.png">
            </p>
            <p class="text-center">
                <a data-toggle="modal" data-target="#shippingClause">{{trans('front.shippingClause')}}</a>
            </p>
        </div>
        <div class="row">
            <div style="margin-left: 3%;width:94%;">
                <button type="button" class="btn btn-lg btn-default submit-btn col-xs-12" ng-click="checkOut()" ng-disabled="isDisable">{{trans('front.checkOutBtn')}}</button>
            </div>
        </div>
    </div>
    <h2 class="text-center" ng-show="!isShowCart()">{{trans('front.tip14')}}</h2>
</div>
</div>

<div class="modal fade" id="shippingClause" tabindex="-1" role="dialog" aria-labelledby="shippingClauseLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h2 style="color: #62b946" class="text-center">{{trans('front.shippingClause')}}</h2>
                <p style="font-size: 15px" ng-bind-html="language.shippingClauseContent"></p>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
<script type="text/javascript">
app.controller('cartCtrl', ['$scope', 'apiService', function($scope, apiService) {
    $scope.cart = $api.getStorage('cart') ? $api.getStorage('cart') : null;
    $scope.shipping_method = 1;
    $scope.payment_method = 1;
    $scope.shipping_fee = 0;
    $api.setStorage('shipping_method', $scope.shipping_method);
    $api.setStorage('payment_method', 1);
    $scope.token = "";

    $scope.isShowCart = function () {
        var bool = ($scope.cart==null||$scope.cart==''||$scope.cart==undefined) ? false : true;
        bool = bool && $scope.islanguageLoaded;
        return bool;
    }

    $scope.cartPlusQty = function (item) {
        $scope.plusQty(item);
        $api.setStorage('cart', $scope.cart);
    }

    $scope.cartMinusQty = function (item) {
        $scope.minusQty(item);
        $api.setStorage('cart', $scope.cart);
    }

    apiService.getSysConfig().success(function(data, status) {
        $scope.sysconfig = data.root;
        $scope.shipping_fee = data.root.shipping_fee;
        $scope.total = function() {
            var total = 0;
            angular.forEach($scope.cart, function(item) {
                item.price = item.shop_price != 0 ? item.shop_price : item.price;//如果减价不为零,使用价格代替商品价格;
                total += item.qty * item.price;
            });
            if($scope.shipping_method == 1){
                $scope.shipping_fee = ( parseFloat(total) >= parseFloat($scope.sysconfig.shipping_discount) ) ? 0 : $scope.sysconfig.shipping_fee
            }
            return parseFloat(total) + parseFloat($scope.shipping_fee);
        }
    });

    $scope.removeItem = function(index) {
        $scope.cart.splice(index, 1);
        $api.setStorage('cart', $scope.cart);
    }
        
    $scope.selectShipping = function (method) {
        if(method != 1){
            $scope.shipping_fee = 0;
        }else{
            $scope.shipping_fee = $scope.sysconfig.shipping_fee;
        }
        $api.setStorage('shipping_method', method);
    }

    $scope.selectPayment = function (value) {
        $api.setStorage('payment_method', value);
    }

    $scope.isDisable = false;
    $scope.checkOut = function () {
        //检查购物车是否有商品
        if($scope.cart){
            //检查是否超出货存
            angular.forEach($scope.cart, function (item,key) {
                item.qty = item.qty > item.stock ? item.stock : item.qty;
                item.stock_tip = '';
            });
            //使用paypal支付
            if($scope.payment_method == 1){
                $scope.isDisable = true;
                apiService.getPaypalToken($scope.cart, $scope.shipping_method, $scope.locale).success(function(data, status) {
                    if(data.info){
                        $scope.info = data.info;
                    }else{
                        $scope.isDisable = false;
                        $scope.token = data.TOKEN;
                        $api.setStorage('paypalToken', $scope.token);
                        if(isDevEnv){
                            var url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' + $api.getStorage('paypalToken');
                        }else{
                            var url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='+$api.getStorage('paypalToken');
                        }
                        // console.log(url);
                        window.location = url;
                    }
                });
            }else{ //其他支付方式
                $api.setStorage('cart', $scope.cart);
                if($scope.shipping_method == 1){
                    window.location = "/shipping";
                }else if($scope.shipping_method == 2){
                    window.location = "/pickup";
                }else if($scope.shipping_method == 3){
                    window.location = "/helpyourself";
                }else{
                    apiAlert("", $scope.language.tip7);
                }
            }
        }else{
            apiAlert("", $scope.language.tip6);
        }
    }

    $scope.clearCart = function () {
        $api.rmStorage('cart');
        $scope.cart = new Array();
    }
}]);
</script>
@endsection