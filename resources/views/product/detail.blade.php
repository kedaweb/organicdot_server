<!DOCTYPE html>
<html>
<head>
<title>{{ $lang=="cn" ? "有機點" : "OrganicDot" }}</title>
<meta property="og:url" content="{{ url('api/products/for-facebook/'.$product->id.'/'.$locale) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $lang=="cn" ? "有機點" : "OrganicDot" }}--{{ $product->{'name_'.$lang} }}" />
<meta property="og:description" content="{{ $product->{'content_'.$lang} }}" />
<meta property="og:image" content="{{$product->img1}}" />
<meta property="og:image:width" content="1080" />
<meta property="og:image:height" content="600" />
</head>
<body>
<div class="fb-share-button" data-layout="button"></div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=146809412337845";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>