@extends('front_layouts.default')

@section('top_title')
<div class="recommend">
    <h1 class="top-title2">{{trans('front.recommend')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="FrmIndexCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="recommend">
            <div class="category-box" ng-repeat="item in recommend">
                <div class="row">
                    <h2 ng-if="locale=='cn'" ng-bind="item.category.name_cht"><img ng-src="@{{item.category.img}}"></h2>
                    <h2 ng-if="locale=='en'" ng-bind="item.category.name_en"><img ng-src="@{{item.category.img}}"></h2>
                </div>
                <div class="row">
                    <data-owl-carousel class="owl-carousel" data-options="{items : 2,margin:5,dots:false,navRewind:false}">
                    <div class="col-xs-6" owl-carousel-item="" ng-repeat="product in item.products">
                        <div class="item" ng-click="openProduct(product.id)">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left" ng-bind="product.name_cht"></span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left" ng-bind="product.name_en"></span></p>
                            <p class="ovh">                        
                            <span class="pull-left" ng-bind="product.price | currency:'$'"></span>
                            <span class="pull-right" ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                            <span class="pull-right" ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                            </p>
                        </div>
                    </div>
                    </data-owl-carousel>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection