@extends('front_layouts.default')
@section('body')
<body class="opening">
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_011.jpg"/>
        </div>
    </div>
    <div id="main" class="container welcome">
        <div class="row text-center">
                <div class="col-xs-offset-1 col-xs-10">
                    <a href="https://itunes.apple.com/cn/app/you-ji-dian/id979112832?l=en&mt=8"><img class="img-responsive" src="/assets/images/applestore.png"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.apicloud.A6996487279609"><img  class="img-responsive"src="/assets/images/googlestore.png"></a>
                    <a href="/welcome"><img class="img-responsive" src="/assets/images/mobileweb.png"></a>
                </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_03.jpg"/>
        </div>
    </div>
</body>
@endsection