@extends('front_layouts.default')

@section('top_title')
<div class="login">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.forgetpwd')}}</h1>
</div>
@endsection
@section('body')
<body ng-controller="forgetpwdCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="login">
            <div class="row form-bg">
                <form id="forgetpwd-form">
                <div class="col-xs-offset-2 col-xs-8">
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.email')}}</label>
                        <input type="email" name="email" class="form-control" ng-model="email">
                    </div>
                </div>
                <div style="margin-left: 3%;width:94%;">
                    <button type="button" class="btn btn-lg btn-default reg-btn col-xs-12" ng-click="send()">{{trans('front.sendBtn')}}</button>
                </div>
                </form>
                <div class="col-xs-12">
                    <div class="alert alert-danger" ng-show="errorInfo">
                        <p ng-repeat="(error, info) in errorInfo">
                            @{{error}}: @{{info}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="recommend">
            <div class="row">
                <h1 class="top-title2">{{trans('front.recommend')}}</h1>
            </div>
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="product in recommend">
                        <div class="item" ng-click="openProduct(product.id)">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left" ng-bind="product.name_cht"></span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left" ng-bind="product.name_en"></span></p>
                            <p class="ovh">                        
                            <span class="pull-left" ng-bind="product.price | currency:'$'"></span>
                            <span class="pull-right" ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                            <span class="pull-right" ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection