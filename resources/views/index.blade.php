@extends('front_layouts.default')

@section('body')
<body class="opening step2" ng-controller="OpeningStepCtrl">
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_011.jpg"/>
        </div>
    </div>
    <div id="main" class="container">
        <div class="row text-center">
            <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/login') }}" class="btn btn-default my_btn">{{ trans('front.openlogin') }}</a>
            <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/register') }}" class="btn btn-default my_btn">{{ trans('front.reg') }}</a>
            <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/recommend') }}" class="btn btn-default my_btn">{{ trans('front.jumpToNext') }}</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_03.jpg"/>
        </div>
    </div>
</body>
@endsection