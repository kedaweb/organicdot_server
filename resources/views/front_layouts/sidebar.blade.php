<ul class="nav" role="tablist">
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/recommend') }}">
            <img ng-src="/image/home.png"> {{trans('front.home')}}
        </a>
    </li>
    <li role="presentation">
        <a ng-if="isLogin" href="#profile" aria-controls="profile" role="tab" data-toggle="tab" data-tab-a>
        <img ng-src="/image/member.png"> {{trans('front.account')}}
        </a>
        <a ng-if="!isLogin" href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/login') }}">
            <img ng-src="/image/member.png"> {{trans('front.login')}}
        </a>
    </li>
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/recommend') }}">
            <img ng-src="/image/ico4.png"> {{trans('front.recommend')}}
        </a>
    </li>
    <li role="presentation" ng-repeat="item in categories">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/categorys') }}" ng-if="!item.children">
            <img ng-src="@{{item.img}}">
            <span class="text-left" ng-if="locale=='cn'" ng-bind="item.name_cht"></span>
            <span class="text-left" ng-if="locale=='en'" ng-bind="item.name_en"></span>
        </a>
        <a href="#category_@{{ item.id }}" aria-controls="category_@{{ item.id }}" role="tab" data-toggle="tab" ng-if="item.children" data-tab-a>
            <img ng-src="@{{item.img}}">
            <span class="text-left" ng-if="locale=='cn'" ng-bind="item.name_cht"></span>
            <span class="text-left" ng-if="locale=='en'" ng-bind="item.name_en"></span>
        </a>
    </li>
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), 'farms') }}">
            <img ng-src="/image/ico6.png"> {{trans('front.farm')}}
        </a>
    </li>
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), 'cart') }}">
            <img ng-src="/image/ico9.png"> {{trans('front.cart')}}
        </a>
    </li>
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), 'contact') }}">
            <img ng-src="/image/phone.png"> {{trans('front.contact')}}
        </a>
    </li>
    <li role="presentation">
        <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), 'privacy-policy') }}">
            <img ng-src="/image/ico8.png"> {{trans('front.PrivacyPolicy')}}
        </a>
    </li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade" id="profile">
        <ul>
            <li><a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/orders') }}">{{trans('front.orders')}}</a></li>
            <li><a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/favorites') }}">{{trans('front.favorites')}}</a></li>
            <li><a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/profile') }}">{{trans('front.profile')}}</a></li>
            <li><a href="#" ng-click="logout()">{{trans('front.logoutBtn')}}</a></li>
        </ul>
    </div>
    <div role="tabpanel" class="tab-pane fade" ng-repeat="item in categories" id="category_@{{item.id}}">
        <ul>
            <li><a ng-click="openCategoryWin(item.id)" ng-if="locale=='cn'">全部</a></li>
            <li><a ng-click="openCategoryWin(item.id)" ng-if="locale=='en'">ALL</a></li>
            <li ng-repeat="child in item.children">
                <a ng-click="openCategoryWin(child.id)" ng-if="locale=='cn'" ng-bind="child.name_cht"></a>
                <a ng-click="openCategoryWin(child.id)" ng-if="locale=='en'" ng-bind="child.name_en"></a>
            </li>
        </ul>
    </div>
</div>