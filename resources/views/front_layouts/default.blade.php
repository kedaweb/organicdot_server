<!doctype html>
<html ng-app="myApp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <title>有機點</title>
    <link rel="stylesheet" type="text/css" href="/assets/api.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/common.css" />
    <link rel="stylesheet" type="text/css" href="/assets/owl/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="/assets/owl/owl.theme.default.css" />
    <link rel="stylesheet" type="text/css" href="/assets/spinkit/css/spinkit.css" />
</head>

@section('body')
<div class="loading-spiner-holder" data-loading >
    <div class="loading-spiner">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>
@section('page_js')
<script type="text/javascript" src="/assets/api.js"></script>
<script type="text/javascript" src="/assets/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/owl/owl.carousel.min.js"></script>
<script type="text/javascript" src="/assets/angular/angular.min.js"></script>
<script type="text/javascript" src="/assets/angular/angular-sanitize.min.js"></script>

<script type="text/javascript" src="/assets/global.js"></script>
<script type="text/javascript" src="/assets/ng-ctrl.js"></script>
<script type="text/javascript" src="/assets/ng-services.js"></script>
<script type="text/javascript" src="/assets/ng-directive.js"></script>
<script type="text/javascript">
var $header;
$(function(){
    $header = $('#header-fixed-top');

    var $menu = $('#menu');
    var $sidebar = $('#sidebar');

    $(document).click(function(e) {
        if( !( e.target == $menu[0] || $.contains($menu[0], e.target) ) || !(e.target == $sidebar[0] || $.contains($sidebar[0], e.target)) ) {
            closeSlideBar();
        }
    });

    $menu.on("click", function(e){
        e.stopPropagation();
    });
    $sidebar.on("click", function(e){
        e.stopPropagation();
    });
})
</script>
@show {{-- endpage_js --}}
@show {{-- endsectionbody --}}

</html>