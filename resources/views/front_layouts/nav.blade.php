<div id="header-fixed-top" class="navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div id="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">
                            <img id="menu" class="img-responsive ico" src="/image/menu.png" onclick="toggleSlideBar()" ng-if="isHideMenu==undefined"/>
                        </div>
                        <div class="col-xs-8">
                            <img class="img-responsive" src="/image/logo.png"/>
                        </div>
                        <div class="col-xs-2">
                            <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), 'cart') }}" ng-if="isHideCart==undefined">
                                <img class="img-responsive ico" src="/image/cart.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('top_title')
            <div class="container ovh" style="position: relative">
                <div class="row">
                    <div id="sidebar" class="sidebar">
                        @include('front_layouts.sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>