@extends('front_layouts.default')

@section('top_title')
<div class="profile">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.profile')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="profileCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="profile">
            <div class="row">
                <form id="profile-form">
                <div class="col-xs-offset-2 col-xs-8">
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.name')}}</label>
                        <input type="text" name="name" class="form-control" ng-model="member.name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.email')}}</label>
                        <input type="email" name="email" class="form-control" ng-model="member.email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.phone')}}({{trans('front.tip10')}})</label>
                        <input type="text" name="phone" class="form-control" ng-model="member.phone" disabled>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.pwd')}}</label>
                        <input type="password" name="password" ng-model='member.password' class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.pwd_confirm')}}</label>
                        <input type="password" name="password_confirm" ng-model='member.password_confirmation' class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.langSelect')}}</label>
                        <select class="form-control" name="language" ng-model="locale">
                            <option value="cn">中文</option>
                            <option value="en">English</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.shippingAddress')}}</label>
                        <input type="text" name="shipping_address" class="form-control" ng-model="member.shipping_address" >
                    </div>
                    <div class="form-group">
                        <input type="text" name="bill_address" class="form-control" ng-model="member.bill_address" >
                    </div>
                </div>
                <div style="margin-left: 3%;width:94%">
                    <button type="button" class="btn btn-lg btn-default save-btn col-xs-12" ng-click="send()">{{trans('front.sendBtn')}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="errorInfo" tabindex="-1" role="dialog" aria-labelledby="errorInfoLabel">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-left" style="font-size: 20px" ng-repeat="(field, errors) in errorInfo">
                    <p ng-repeat="error in errors">@{{field}}: @{{error}}</p>
                </div>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection