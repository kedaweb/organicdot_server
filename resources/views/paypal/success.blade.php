<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="有機點後臺">
<meta name="author" content="sam_leung">
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row" style="margin: 10px 0">
        <div class="col-md-offset-4 col-md-4">
        <img class="img-responsive" src="/assets/images/logo.png" style="margin:0 auto">
        </div>
    </div>
    <div class="row" style="margin: 10px 0">
        <div class="col-md-offset-4 col-md-4">
          @if ($fail)
          <p>Paypal 服務暫停.請重新發起支付</p>
          <p>Paypal Service is stop.</p>
          @else
          <p>付款完成, 為保護你的安全，Paypal將審核下列的交易。付款審核將會在 24 小時內完成。<span class="text-danger"> 請按右上角按鈕繼續完成流程。</span></p>
          <p>Payment is done, to protect your security, Paypal will review the following transactions. Payment will be completed within 24 hours. <span class="text-danger">Please press right corner button to complete the process.</span></p>
          @endif
        </div>
    </div>
</div>
@if (!$fail)
<script type="text/javascript">
apiready = function(){
    api.execScript({
      name: "frm_paypal",
      script: "nextProcees();",
    });
};
</script>
@endif
</body>
</html>