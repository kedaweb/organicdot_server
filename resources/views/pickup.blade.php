@extends('front_layouts.default')

@section('top_title')
<div class="shipping pickup">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" ng-click="back()">{{trans('front.shipping')}}</h1>
</div>
@endsection

@section('body')
<body ng-controller="pickupCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="pickup">
            <div class="row" style="margin-top: 10px" ng-hide="isLogin">
                <div style="margin-left: 3%;width:94%;">
                    <button type="button" class="btn btn-lg btn-default login-btn col-xs-12" ng-click="showLoginForm()">{{trans('front.login')}}</button>
                </div>
            </div>
            <div class="row" ng-hide="isLogin">
                <p class="help-block text-center" ng-bind="language.tip19"></p>
            </div>
            <form name="pickupForm">
                <div class="row pickup-bg">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.email')}}</label>
                            <input type="email" name="buyerEmail" class="form-control" ng-model="order.email" required>
                            <p class="help-block red" ng-show="pickupForm.buyerEmail.$error.required">{{trans('front.require')}}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.buyer')}}</label>
                            <input type="text" name="buyerName" class="form-control" ng-model="order.buyer_name" required>
                            <p class="help-block red" ng-show="pickupForm.buyerName.$error.required">{{trans('front.require')}}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.phone')}}</label>
                            <input type="text" name="phone" class="form-control" ng-model="order.phone" required>
                            <p class="help-block red" ng-show="pickupForm.phone.$error.required">{{trans('front.require')}}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.shippingAddress')}}</label>
                            <input type="text" name="shippingAddress" class="form-control" ng-model="order.shipping_address" required>
                            <p class="help-block red" ng-show="pickupForm.shippingAddress.$error.required">{{trans('front.require')}}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.billAddress')}}</label>
                            <input type="text" name="billAddress" class="form-control" ng-model="order.bill_address">
                            <div class="checkbox help-block">
                                <label for="sameAdress">
                                <input id="sameAdress" type="checkbox" ng-click="sameAdress($event)" ng-disabled="sendBtnDisable"/>{{trans('front.tip16')}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.pickupSelect')}}</label>
                            <div class="radio" ng-repeat="item in pickups">
                                <label>
                                    <input name="delivery" class="hidden-input" type="radio" ng-model="order.delivery_id" value="@{{item.id}}" required>
                                    <span></span>
                                    <font ng-if="locale=='cn'">@{{item.name_cht}} (@{{item.address_cht}})</font>
                                    <font ng-if="locale=='en'">@{{item.name_en}} (@{{item.address_en}})</font>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="margin-left: 3%;width:94%;">
                        <div class="checkbox text-center" ng-hide="isLogin">
                            <label for="isReg">
                            <input id="isReg" type="checkbox" ng-model="isReg" />{{trans('front.tip17')}}
                            </label>
                        </div>
                        <button type="button" class="btn btn-lg btn-default login-btn col-xs-12" ng-disabled="pickupForm.$invalid || sendBtnDisable" ng-click="sendOrder()">{{trans('front.sendBtn')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="msg2" tabindex="-1" role="dialog" aria-labelledby="msg2Label" data-backdrop="static" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.msg2"></p>
                <p class="text-center">
                    <a type="button" class="btn btn-default btn-lg submit-btn" href="/recommend">{{trans('front.confirmBtn')}}</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorInfo" tabindex="-1" role="dialog" aria-labelledby="errorInfoLabel" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-left" style="font-size: 20px" ng-repeat="(field, errors) in errorInfo">
                    <p ng-repeat="error in errors">@{{error}}</p>
                </div>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tip15" tabindex="-1" role="dialog" aria-labelledby="tip15Label" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.tip15"></p>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tip20" tabindex="-1" role="dialog" aria-labelledby="tip20Label" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.tip20"></p>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" style="margin-bottom: 0" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginSuccess" tabindex="-1" role="dialog" aria-labelledby="loginSuccessLabel" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.tip18"></p>
                <p class="text-center" style="font-size: 20px" ng-bind-html="language.tip15"></p>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" style="margin-bottom: 0" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" style="display: none">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <form name="loginForm" id="login-form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.email')}}</label>
                        <input type="text" class="form-control" ng-model="loginID">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.pwd')}}</label>
                        <input type="password" name="password" class="form-control"  ng-model="password">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default login-btn" style="color: #fff" ng-click="loginByModal()">{{trans('front.loginBtn')}}</button>
                        <button class="btn btn-default submit-btn" style="margin-bottom: 0" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@endsection