@extends('front_layouts.default')

@section('top_title')
<div class="contact">
    <h1 class="top-title" style="text-indent: 0;">
        <img class="pull-left hidden" src="/image/back.jpg" ng-click="backCart()">
        Paypal
    </h1>
</div>
@endsection

@section('body')
<body ng-controller="FrmPaypalCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="row" style="margin: 10px 0">
            <div class="col-md-offset-4 col-md-4">
              @if ($fail)
              <p>Paypal 服務暫停.請重新發起支付</p>
              <p>Paypal Service is stop.</p>
              @else
              <p>付款完成, 為保護你的安全，Paypal將審核下列的交易。付款審核將會在 24 小時內完成。</p>
              <p>Payment is done, to protect your security, Paypal will review the following transactions. Payment will be completed within 24 hours.</p>
              @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
@if (!$fail)
<script type="text/javascript">
$(function () {
    setTimeout(function () {
        $api.rmStorage('cart');
        if($api.getStorage('shipping_method') == 1){
            window.location = "/shipping";
        }else if($api.getStorage('shipping_method') == 2){
            window.location = "/pickup";
        }else{
            window.location = "/helpyourself";
        }
    }, 2000)
})
</script>
@endif
@endsection