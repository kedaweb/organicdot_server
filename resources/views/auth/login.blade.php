<!DOCTYPE html>
<html lang="zh-CN">
<head>
<title>有機點後臺</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="有機點後臺">
<meta name="author" content="sam_leung">
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/scojs.css" rel="stylesheet">
<link href="/assets/css/sco.message.css" rel="stylesheet">
@yield('page_css')
<link href="/assets/css/admin_style.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="container">
    <div class="row" style="margin: 10px 0">
        <div class="col-md-offset-4 col-md-4">
        <img class="img-responsive" src="/assets/images/logo.png" style="margin:0 auto">
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <form action="" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">用戶名/Email</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="用戶名">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">密碼</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="密碼">
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="remember"> 記住我</label>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary">登錄</button>
            </form>
            <p>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </p>
        </div>
    </div>
</div>
</body>
</html>