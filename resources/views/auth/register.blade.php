<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- 可選的Bootstrap主題文件（一般不用引入） -->
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<!-- jQuery文件。務必在bootstrap.min.js 之前引入 -->
<script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="http://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>有機點<small>後台註冊</small></h2>
    <form action="" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">用戶名</label>
            <input type="text" name="username" class="form-control" placeholder="用戶名/Email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">密碼</label>
            <input type="password" name="password" class="form-control" placeholder="密碼">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">確認密碼</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="確認密碼">
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-lg btn-primary">註冊</button>
    </form>
    <p>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </p>
</div>
</body>
</html>