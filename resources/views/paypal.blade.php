@extends('front_layouts.default')

@section('top_title')
<div class="contact">
    <h1 class="top-title">
        <img class="pull-left" src="/image/back.jpg" ng-click="backCart()">
        Paypal
    </h1>
</div>
@endsection

@section('body')
<body ng-controller="FrmPaypalCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="contact">
            <div class="row">
                <div class="col-xs-12">
                    <div class="embed-responsive embed-responsive-4by3">
                        <iframe class="embed-responsive-item" id="paypal_frm"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection

@section('page_js')
@parent
@include('front_layouts.fixheader')
<script type="text/javascript">
var url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='+$api.  getStorage('paypalToken');
// var url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='+$api.getStorage('paypalToken');
$('#paypal_frm').attr('src', url);
</script>
@endsection