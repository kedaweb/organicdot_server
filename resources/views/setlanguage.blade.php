@extends('front_layouts.default')
@section('body')
<body class="opening">
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_011.jpg"/>
        </div>
    </div>
    <div id="main" class="container">
        <div class="row text-center">
            @foreach(MaLocal::getSupportedLocales() as $localeCode => $properties)
                <button class="btn btn-default my_btn" rel="alternate" hreflang="{{$localeCode}}" onclick="setLang('{{$localeCode}}')">
                    {{{ $properties['native'] }}}
                </button>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="row">
            <img class="img-responsive" src="/image/opening_03.jpg"/>
        </div>
    </div>
    <script type="text/javascript" src="/assets/api.js"></script>
    <script type="text/javascript">
    var setLang = function(language){
        $api.setStorage('language', language);
        window.location.href = '/'+language+'/home';
    };
    </script>
</body>
@endsection
