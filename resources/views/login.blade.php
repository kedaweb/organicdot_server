@extends('front_layouts.default')

@section('top_title')
<div class="login">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.login')}}</h1>
</div>
@endsection
@section('body')
<body ng-controller="LoginCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="login">
            <div class="row form-bg">
                <div class="col-xs-offset-2 col-xs-8">
                    <form id="login-form">
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('front.email')}}</label>
                            <input type="text" class="form-control" ng-model="loginID">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">{{trans('front.pwd')}}</label>
                            <input type="password" name="password" class="form-control"  ng-model="password">
                            <div class="help-block">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember" ng-model="isChecked" ng-click="rememberMe()"> {{trans('front.rememberme')}}</label>
                                    <a href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/forgetpwd') }}">{{trans('front.forgetpwd')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-default login-btn col-xs-12" ng-click="login()">{{trans('front.loginBtn')}}</button>
                        </div>
                        <div class="form-group">
                        <a class="btn btn-default reg-btn col-xs-12" href="{{MaLocal::getLocalizedURL(MaLocal::getCurrentLocale(), '/member/register') }}">{{trans('front.regBtn')}}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="recommend">
            <div class="row">
                <h1 class="top-title2">{{trans('front.recommend')}}</h1>
            </div>
            <div class="category-box">
                <div class="row">
                    <div class="col-xs-6" ng-repeat="product in recommend">
                        <div class="item" ng-click="openProduct(product.id)">
                            <img class="img-responsive ico" ng-src="@{{product.img1}}"/>
                            <p class="products-title" ng-if="locale=='cn'"><span class="text-left" ng-bind="product.name_cht"></span></p>
                            <p class="products-title" ng-if="locale=='en'"><span class="text-left" ng-bind="product.name_en"></span></p>
                            <p class="ovh">                        
                            <span class="pull-left" ng-bind="product.price | currency:'$'"></span>
                            <span class="pull-right" ng-if="locale=='cn'" ng-bind="product.weight_unit_cht"></span>
                            <span class="pull-right" ng-if="locale=='en'" ng-bind="product.weight_unit_en"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection