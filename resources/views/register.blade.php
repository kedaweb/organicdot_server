@extends('front_layouts.default')

@section('top_title')
<div class="login">
    <h1 class="top-title"><img class="pull-left" src="/image/back.jpg" onclick="javascript:history.go(-1)">{{trans('front.reg')}}</h1>
</div>
@endsection
@section('body')
<body ng-controller="regCtrl">
@include('front_layouts.nav')
<div id="frm_main">
    <div class="container">
        <div class="login">
            <div class="row form-bg">
                <form namg="regForm">
                <div class="col-xs-offset-1 col-xs-10">
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.name')}}</label>
                        <input type="text" name="name" ng-model='reg.name' class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('front.phone')}}</label>
                        <div class="phone-form-group ovh">                        
                            <input type="text" name="areacodes" ng-model='reg.areacodes' class="form-control areacodes" placeholder="{{ trans('front.areacodes') }}">
                            <input type="text" name="phone" ng-model='reg.phone' class="form-control phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.email')}}</label>
                        <input type="email" name="email" ng-model='reg.email' class="form-control">
                        <p class="help-block">({{trans('front.tip8')}})</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.pwd')}}</label>
                        <input type="password" name="password" ng-model='reg.password' class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('front.pwd_confirm')}}</label>
                        <input type="password" name="password_confirm" ng-model='reg.password_confirmation' class="form-control">
                    </div>
                    <div class="form-group">
                    </div>
                </div>
                <div style="margin-left: 3%;width:94%">
                    <button type="button" class="btn btn-lg btn-default reg-btn col-xs-12" ng-click="send()">{{trans('front.sendBtn')}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorInfo" tabindex="-1" role="dialog" aria-labelledby="errorInfoLabel">
    <div class="modal-dialog modal-msg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-left" style="font-size: 20px" ng-repeat="(field, errors) in errorInfo">
                    <p ng-repeat="error in errors" ng-bind="error"></p>
                </div>
                <p class="text-center">
                    <button type="button" class="btn btn-default btn-lg submit-btn" data-dismiss="modal">{{trans('front.closeBtn')}}</button>
                </p>
            </div>
        </div>
    </div>
</div>
@parent
</body>
@endsection