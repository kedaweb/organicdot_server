@extends('admin._layouts.default')


@section('title', '類別管理')
@section('breadcrumb', '類別管理')

@section('content')
<div class="jumbotron">
    <h4>新增類別</h4>
    <div class="bg">
        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/add">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">種類</label>
                <div class="col-sm-4">
                    <select class="form-control" name="parent_id">
                        <option value="0">頂級類別</option>
                        @foreach ($categories as $item)
                        <option value="{{ $item->id }}">{{ $item->name_cht }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">網頁位置</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="sorting" value="1">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">相片<br>(尺寸:300x100)</label>
                <div class="col-sm-4">
                    <input type="file" name="img">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 10px">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">English</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Trad.Chinese</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">類別名稱(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_en" value="">
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">類別名稱</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_cht" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">顯示</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input style="margin-left: 0" name="is_show" type="checkbox" value="1" checked >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default">新增</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection