@extends('admin._layouts.default')


@section('title', '類別管理')
@section('breadcrumb', '類別管理')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增類別</h4>
    <div class="bg">
        <a class="btn btn-default" href="{{ $redirect_path }}/add/">新增類別</a>
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>所有類別</h4>
    <form id="query" class="form-horizontal" method="post" action="{{ $redirect_path }}">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label">類別</label>
            <div class="col-sm-2">
                <select class="form-control" name="id" id="selecter">
                    <option value="all">全部</option>
                    <option value="0" {{ $id==='0' ? "selected" : "" }} >頂級類別</option>
                    @foreach ($top_cates as $item)
                    <option value="{{ $item->id }}"  {{ $id==$item->id ? "selected" : "" }}>{{ $item->name_cht }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </form>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>類別名稱</th>
                    <th>所屬</th>
                    <th>網頁位置</th>
                    <th>顯示</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td>{{ $item->name_cht }}</td>
                    <td>{{ isset($item->category->name_cht) ? $item->category->name_cht : "頂級類別" }}</td>
                    <td>位置: {{ $item->sorting }} </td>
                    <td><input onclick="edit_show({{ $item->id }}, 'is_show', this)" name="is_show" type="checkbox" {{ $item->is_show ? "checked" : "" }} ></td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
$(function(){
    $('#selecter').on('change', function(event) {
        window.location = "{{ $redirect_path }}/?" + $('#query').serialize();
    });
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection