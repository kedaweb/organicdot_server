<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
    <thead>
        <tr>
            <th>訂單號</th>
            <th>狀態</th>
            <th>購買人</th>
            <th>電話</th>
            <th>支付方式</th>
            <th>賬單地址</th>
            <th>郵寄地址</th>
            <th>運送方式</th>
            <th>運送費用</th>
            <th>送貨時段</th>
            <th>自提地址</th>
            <th>下單時間</th>
            <th>支付狀態</th>
        </tr>
    </thead>
    @foreach ($rs as $item)
    <tr>
        <td>{{'order#'.$item->id}}</td>
        <td>{{trans('site.order_status')[$item->status]}}</td>
        <td>{{$item->buyer_name}}</td>
        <td>{{$item->phone}}</td>
        <td>{{trans('site.order_payment')[$item->payment_method]}}</td>
        <td>{{$item->bill_address}}</td>
        <td>{{$item->shipping_address}}</td>
        <td>{{$item->shipping_method}}</td>
        <td>{{$item->shipping_fee}}</td>
        <td>
        @if(is_array($item->shipping_time_id))
        {{ implode('|', $item->shippingTimes()->lists('name_cht')->toArray()) }}
        @endif
        </td>
        <td>
        @if($item->pickupLocation)
        {{ $item->pickupLocation->name_cht."(".$item->pickupLocation->address_cht.")" }}
        @endif
        </td>
        <td>{{$item->buy_date}}</td>
        <td>{{$item->is_paid ? '未支付' : '已支付' }}</td>
    </tr>
    @endforeach
</table>
</html>