@extends('admin._layouts.default')


@section('title', '帳戶修改')
@section('breadcrumb', '帳戶修改')

@section('content')
<div class="jumbotron">
    <div class="bg">
        <form id="add_user" class="form-horizontal" method="post" action="/admin/users/edit">
            <div class="form-group">
                <label class="col-sm-2 control-label"><h4>修改管理員</h4></label>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">管理員名稱</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="username" placeholder="username" value="{{ $user->username }}">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">用戶組</label>
                <div class="col-sm-4">
                    <select class="form-control" name="role_id">
                        @foreach ($roles as $item)
                            <option value="{{ $item->id }}" {{ !empty($user->role()) && $user->role()->role_id==$item->id ? "selected" : "" }} >{{ $item->display_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">狀態</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input type="radio" value="0" name="is_ban" {{ $user->is_ban ? "" : "checked" }}> 顯示
                        <input type="radio" value="1" name="is_ban" {{ $user->is_ban ? "checked" : "" }}> 隱藏
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-default">修改</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection