@extends('admin._layouts.default')


@section('title', '帳戶')
@section('breadcrumb', '帳戶')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增管理員</h4>
    <div class="bg">
        <form id="add_user" class="form-horizontal" method="post" action="/admin/users/create">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">管理員名稱</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="username" placeholder="username">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">用戶組</label>
                <div class="col-sm-4">
                    <select class="form-control" name="role_id">
                        @foreach ($roles as $item)
                            <option value="{{ $item->id }}">{{ $item->display_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">確認密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">狀態</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                    <input type="radio" value="0" name="is_ban" checked> 顯示
                    <input type="radio" value="1" name="is_ban"> 隱藏
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default">新增</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>管理員</h4>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>管理員名稱</th>
                    <th>角色</th>
                    <th>狀態</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                <tr>
                    <td>{{ $key+1 }}</th>
                    <td>{{ $user->username }}</th>
                    <td>{{ $user->role()['display_name'] }}</th>
                    <td> 
                    @if (1 != $user->id)
                    <input onclick="edit_ban({{ $user->id }}, 'is_ban', this)" type="checkbox" {{ $user->is_ban ? "" : "checked" }} > 
                    @endif
                    </th>
                    <td>
                        @if (1 != $user->id)
                            <a class="btn btn-success" href="/admin/users/edit/{{ $user->id }}">編輯</a>
                        @endif
                        <a class="btn btn-info" href="/admin/users/reset-pwd/{{ $user->id }}">更改密碼</a>
                        @if (1 != $user->id)
                        <a class="btn btn-danger" data-action="/admin/users/delete/{{ $user->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function edit_ban(id, field, obj) {
    var value = $(obj).is(":checked") ? 0 : 1;
    $.ajax({
        url: '/admin/users/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
$(function(){
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection