@extends('admin._layouts.default')


@section('title', '角色權限')
@section('breadcrumb', '角色權限')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>所有角色權限</h4>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>名稱</th>
                    <th>描述</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td>{{ $item->display_name }}</td>
                    <td>{{ $item->description }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit-permission/{{ $item->id }}">編輯</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
$(function(){
    $('#nav').find(".roles").addClass('active');
    $('#selecter').on('change', function(event) {
        window.location = "{{ $redirect_path }}/?" + $('#query').serialize();
    });
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection