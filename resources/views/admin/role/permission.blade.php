@extends('admin._layouts.default')

@section('page_css')
<link href="/assets/switch/css/bootstrap-switch.min.css" rel="stylesheet">
@endsection

@section('title', '角色權限')
@section('breadcrumb', '角色權限')

@section('content')
<div class="jumbotron">
    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/edit-permission">
    <h4>修改 {{$row->display_name}} 權限</h4>
    <div class="bg">
            <table class="table table-striped">
                <thead>
                    <tr>                    
                        <th>權限</th>
                        <th>動作</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($lists as $item)
                    <tr>
                        <td>{{$item->display_name}}</td>
                        <td>
                            <div class="switch">
                                @if ( in_array($item->id, $role_permissions) )
                                <input name="{{$item->name}}" type="checkbox" value="{{$item->id}}" checked />
                                @else
                                <input name="{{$item->name}}" type="checkbox" value="{{$item->id}}" />
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="100">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $row->id }}">
                            <button type="submit" class="btn btn-success">修改</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </form>
</div>
@endsection

@section('page_js')
<script src="/assets/switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
$(function(){
    $("input:checkbox").bootstrapSwitch();
})
</script>
@endsection