@extends('admin._layouts.default')


@section('title', '認證管理')
@section('breadcrumb', '認證管理')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增認證</h4>
    <div class="bg">
        <a class="btn btn-default" href="{{ $redirect_path }}/add/1">新增產品認證</a>
        <a class="btn btn-default" href="{{ $redirect_path }}/add/2">新增供應商認證</a>
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>所有認證</h4>
    <form id="query" class="form-horizontal" method="post" action="{{ $redirect_path }}">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label">認證</label>
            <div class="col-sm-2">
                <select class="form-control" name="type" id="type_select">
                    <option {{ $type=="" ? "selected" : "" }} value="">全部</option>
                    <option {{ $type=="1" ? "selected" : "" }} value="1">產品認證</option>
                    <option {{ $type=="2" ? "selected" : "" }} value="2">供應商認證</option>
                </select>
            </div>
        </div>
    </form>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>名稱</th>
                    <th>網頁位置</th>
                    <th>認證類型</th>
                    <th>顯示</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td>{{ $item->name_cht }}</td>
                    <td>位置: {{ $item->sorting }} </td>
                    <td>{{ $item->type==1 ? '產品認證' : '供應商認證'  }} </td>
                    <td><input onclick="edit_show({{ $item->id }}, 'is_show', this)" name="is_show" type="checkbox" {{ $item->is_show ? "checked" : "" }} ></td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
$(function(){
    $('#type_select').on('change', function(event) {
        window.location = "{{ $redirect_path }}/?" + $('#query').serialize();
    });
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection