@extends('admin._layouts.default')

@section('page_css')
<link href="/assets/DataTables/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection

@section('title', '產品管理')
@section('breadcrumb', '產品管理')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增產品</h4>
    <div class="bg">
        <a class="btn btn-default" href="{{ $redirect_path }}/add/">新增產品</a>
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>搜索產品</h4>
    <div class="bg">
        <form id="query" class="form-inline" method="get" action="{{ $redirect_path }}">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="產品名稱" name="name" value="{{ isset($name) ? $name : '' }}">
            </div>
            <div class="form-group">
                <select id="p_cate" class="form-control" name="parent_id">
                    <option value="0">全部</option>
                    @foreach ($top_cates as $item)
                    <option value="{{ $item->id }}">{{ $item->name_cht }}</option>
                    @endforeach
                </select>
                <select id="category" class="form-control" name="category_id">
                    <option value="">全部</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">搜索</button>
        </form>
    </div>
    </div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>所有產品</h4>
    <div>
    <button class="btn btn-default" onclick="copy();">複製</button>
    <button class="btn btn-default" onclick="batchEdit()">批次管理</button>
    </div>
    <div class="bg">
        <table id="products_table" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>圖片</th>
                    <th>編號</th>
                    <th>產品名稱</th>
                    <th>種類</th>
                    <th>價錢</th>
                    <th>減價</th>
                    <th>供應商</th>
                    <th>庫存</th>
                    <th>今日推介</th>
                    <th>銷售顯示</th>
                    <th>操作</th>
                </tr>
            </thead>
            <form id="table_form">
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td><input name="product_ids[]" type="checkbox"  value="{{ $item->id }}"></td>
                    <td><img width="30" src="{{ $item->img1 }}"></td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name_cht }}</td>
                    <td>{{ $item->category->name_cht }}</td>
                    <td>{{ $item->price }} /{{ $item->weight_unit_cht }}</td>
                    <td>{{ $item->shop_price }} /{{ $item->weight_unit_cht }}</td>
                    <td>{{ $item->supplier ? $item->supplier->name_cht : "" }}</td>
                    <td>{{ $item->stock_status ? '尚有貨存' : '補貨中' }}({{ $item->stock }})</td>
                    <td><input style="width:45px" class="form-control text-center recommend_input" type="text" value="{{ $item->recommend ? $item->recommend : '-' }}" name="recommend" data-pid="{{ $item->id }}"  data-orgval="{{ $item->recommend ? $item->recommend : '-' }}" ></td>
                    <td><input onclick="edit_show({{ $item->id }}, 'is_show', this)" name="is_show" type="checkbox" {{ $item->is_show ? "checked" : "" }} ></td>
                    <td>
                        <a class="btn btn-success btn-sm" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger btn-sm" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                        @if (!file_exists(public_path($qrcode_dir.$item->id.'.png')))
                        {{ QrCode::format('png')->margin(1)->size(150)->encoding('UTF-8')->generate('product_'.$item->id, public_path($qrcode_dir.$item->id.'.png')) }}
                        @endif
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="showQrcode('{{url($qrcode_dir.$item->id.'.png')}}', '{{ $item->name_cht }}', '{{ $item->id }}')">二維碼</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </form>
        </table>
    </div>
    </div>
</div>
@endsection

@section('modal-title')
<span id="qrcode_title">二維碼</span>
@endsection
@section('modal-body')
<div class="modal-body">
    <img class="img-responsive" id="qrcode" src="" style="margin:0 auto">
</div>
@endsection

@section('page_js')
<script type="text/javascript" src="/assets/DataTables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/DataTables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
function copy () {
    var url_params = getCheckedBoxValues('product_ids');
    window.location = '/admin/products/copy/?' + url_params;
}
function batchEdit () {
    var url_params = getCheckedBoxValues('product_ids');
    window.location = '/admin/products/batch-edit/?' + url_params;
}
function showQrcode (src, title, id) {
    console.log(src);
    $("#qrcode").attr('src', src);
    $("#qrcode_title").html(title+"_"+id);
}
$(function(){

    $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('input', td).val() == "-" ? 0 :parseInt($('input', td).val());
        } );
    }

    $('#products_table').DataTable({
        stateSave: true,
        "columnDefs": [ 
            {
                "targets": [0,1,10,11],
                "orderable": false,
                "searchable": false
            }
        ],
        "columns": [
            null,null,null,null,null,null,null,null,null,
            { "orderDataType": "dom-text", type: 'numeric' },
        ]
    });
    $('#products_table').delegate('.recommend_input', 'blur', function(event) {
        var $this = $(this);
        var val = $this.val();
        var orgval = $this.data('orgval');
        var pid = $this.data('pid');
        if(val > 10) {
            $.scojs_message("今日推介只有10個欄位", $.scojs_message.ERROR);
            $this.val(orgval);
            return;
        }
        if(val != orgval){
            $.ajax({
                url: '/admin/products/set-recommend',
                type: 'post',
                dataType: 'json',
                data: {product_id: pid, value: val},
            })
            .done(function(response) {
                var type = response.success ? $.scojs_message.TYPE_OK : $.scojs_message.ERROR;
                if(response.success){
                    $this.data('orgval', val);
                }else{
                    $this.val(orgval);
                }
                $.scojs_message(response.info, type);
            });
        }
    })

    @include('admin._layouts.commonjs')
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection