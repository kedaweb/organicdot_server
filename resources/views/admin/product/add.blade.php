@extends('admin._layouts.default')


@section('title', '產品管理')
@section('breadcrumb', '產品管理')

@section('content')
<div class="jumbotron">
    <h4>新增產品</h4>
    <div class="bg">
        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/add">
            <div class="form-group">
                <label  class="col-sm-2 control-label">價錢</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="price" value="{{ old('price') }}">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">減價</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="shop_price" value="{{ old('shop_price') }}">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">種類</label>
                <div class="col-sm-2">
                    <select id="p_cate" class="form-control" name="parent_id">
                        <option value="0">--請選擇種類--</option>
                        @foreach ($top_cates as $item)
                        <option value="{{ $item->id }}">{{ $item->name_cht }}</option>
                        @endforeach
                    </select>
                    <select id="category" class="form-control" name="category_id">
                        <option value="">---</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">供應商</label>
                <div class="col-sm-2">
                    <select class="form-control" name="supplier_id">
                        @foreach ($suppliers as $item)
                        <option value="{{ $item->id }}">{{ $item->name_cht }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">庫存</label>
                <div class="col-sm-2">
                    <select class="form-control" name="stock_status">
                        <option value="1">尚有存貨</option>
                        <option value="0">補貨中</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">貨存數量</label>
                <div class="col-sm-10">
                    <div class="row">                    
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="stock" value="{{ old('stock') }}">
                        </div>
                        <label  class="col-sm-2 control-label">新增數量</label>
                        <div class="col-md-2">
                        <input type="text" class="form-control" name=" increase_stock" value="{{ old('increase_stock') }}">
                        </div>
                        <label  class="col-sm-2 control-label">損耗數量</label>
                        <div class="col-md-2">
                        <input type="text" class="form-control" name="loss_stock" value="{{ old('loss_stock') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">網頁位置</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="sorting" value="{{ old('sorting') }}">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">專業認證</label>
                <div class="col-sm-10">
                    @foreach ($certs as $item)
                    <label class="checkbox-inline">
                    <input type="checkbox" name="cert_ids[]" value="{{ $item->id }}"> {{ $item->name_cht }}
                    </label>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">相片<br>(尺寸:1080x600 <br> *最多3張圖片)</label>
                <div class="col-sm-4">
                    <input type="file" name="img1">
                    <input type="file" name="img2">
                    <input type="file" name="img3">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label">今日推介</label>
                <div class="col-sm-10">
                    <label id="recommend_first" class="checkbox-inline">
                    <input type="radio" name="recommend" checked value=""> 無
                    </label>
                    <span id="recommend_box"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 10px">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">English</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Trad.Chinese</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">產品名稱(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_en" value="{{ old('name_en') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">重量單位(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="weight_unit_en" value="{{ old('weight_unit_en') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">產品簡介(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_en">{{ old('content_en') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">挑選方法(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_choose_en">{{ old('content_choose_en') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">營養成份(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_nutrition_en">{{ old('content_nutrition_en') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">功效作用(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_effect_en">{{ old('content_effect_en') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">保存方法(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_save_en">{{ old('content_save_en') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">送貨詳情(英)</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="shipping_detail_en">{{ old('shipping_detail_en') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">產品名稱</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_cht" value="{{ old('name_cht') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">重量單位</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="weight_unit_cht" value="{{ old('weight_unit_cht') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">產品簡介</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_cht">{{ old('content_cht') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">挑選方法</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_choose_cht">{{ old('content_choose_cht') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">營養成份</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_nutrition_cht">{{ old('content_nutrition_cht') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">功效作用</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_effect_cht">{{ old('content_effect_cht') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">保存方法</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="content_save_cht">{{ old('content_save_cht') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">送貨詳情</label>
                                <div class="col-sm-8">
                                    <textarea rows="10" class="form-control ckeditor" name="shipping_detail_cht">{{ old('shipping_detail_cht') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">銷售顯示</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input style="margin-left: 0" name="is_show" type="checkbox" value="1" checked >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default">新增</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection

@section('page_js')
<script src="/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(function(){
    @include('admin._layouts.commonjs')

    $parent_category.on('change',function(){
        $('#recommend_box').empty();
    })
    $category.on('change', function(event) {
        $.ajax({
            url: '/admin/products/active-recommend',
            type: 'post',
            dataType: 'json',
            data: {category_id: $(this).val()},
        })
        .done(function(response) {
            $('#recommend_box').empty();
            if(response.root){
                $.each(response.root, function (key,value) {
                    var str = '<label class="checkbox-inline"><input type="radio" name="recommend" value="'+value+'"> '+value+' </label>';
                    $('#recommend_box').append(str);
                })
            }
        })
    });

    CKEDITOR.replaceClass = 'ckeditor';
});
</script>
@endsection