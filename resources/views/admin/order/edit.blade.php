@extends('admin._layouts.default')


@section('title', '交易資料')
@section('breadcrumb', '交易資料')

@section('content')
<div class="jumbotron">
    <div class="bg">
        <h4>交易資料</h4>
        <div class="row">
            <div class="col-sm-6">
                <form class="form-horizontal" method="post" action="{{ $redirect_path }}/edit">
                <table class="table talbe-no-border">
                    <caption>#{{$row->id}} 訂單內容</caption>
                    <tbody>
                        <tr>
                            <td>名稱</td>
                            <td> {{ $row->buyer_name }} </td>
                        </tr>
                        <tr>
                            <td>電話</td>
                            <td> {{ $row->phone }} </td>
                        </tr>
                        <tr>
                            <td>賬單地址</td>
                            <td> {{ $row->bill_address }} </td>
                        </tr>
                        <tr>
                            <td>收貨地址</td>
                            <td> {{ $row->shipping_address }} </td>
                        </tr>
                        <tr>
                            <td>電郵</td>
                            <td> @if ($row->member) {{ $row->member->email }} @endif  </td>
                        </tr>
                        <tr>
                            <td>購買日期</td>
                            <td> {{ $row->buy_date }} </td>
                        </tr>
                        <tr>
                            <td>運送方式</td>
                            <td> {{ $row->shipping_method }} </td>
                        </tr>
                        @if ($row->pickupLocation)
                        <tr>
                            <td>提貨點</td>
                            <td> {{ $row->pickupLocation->name_cht }}({{ $row->pickupLocation->address_cht }}) </td>
                        </tr>
                        @endif
                        <tr>
                            <td>狀態</td>
                            <td>
                                <select name="status" class="form-control">
                                    @foreach (trans('site.order_status') as $key => $item)
                                    <option value="{{$key}}" {{$row->status==$key ? "selected" : "" }} >{{$item}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{ $row->id }}">
                                <button type="submit" class="btn btn-default">儲存</button>
                                <a target="_blank" href="/admin/orders/print-receipt/{{ $row->id }}" class="btn btn-success">列出收據</a>
                                <a target="_blank" href="/admin/orders/print-itemlist/{{ $row->id }}" class="btn btn-success">列出貨單</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </div>
            <div class="col-sm-6">
                <table class="table no-border">
                    <caption>購買產品</caption>
                    <thead>                        
                        <tr>
                            <th>產品</th>
                            <th>數量</th>
                            <th>統計</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($row->orderItems as $item)
                        <tr>
                            <td>
                                <label>
                                <input type="checkbox" value="{{$item->id}}" name="order_item_id[]">
                                {{$item->product->name_cht or $item->product_name}}
                                </label>
                            </td>
                            <td>{{$item->qty}}</td>
                            <td>{{ number_format($item->qty*$item->price, 2)}}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" class="p_td">
                                <p><a class="btn btn-danger" data-title="是否確定刪除?" data-trigger="confirm">刪除</a></p> 
                                <p><a onclick="AddOrderItemsAction()" class="btn btn-info">補貨</a></p> 
                            </td>
                            <td class="p_td">
                                <div id="total_1">
                                    <p>運費:    {{$row->shipping_fee}} </p>
                                    <p>折扣:    0 </p>
                                    <p>總收費:  {{ number_format( ($total+$row->shipping_fee),2 ) }}</p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <form id="addOrderItems" class="dn" method="post" action="{{$redirect_path}}/add-items">
                    <table class="table table-striped">
                        <thead>
                            <th></th>
                            <th>產品</th>
                            <th>數量</th>
                            <th>單價</th>
                            <th>總計</th>
                        </thead>
                        <tbody>
                            <tr class="product_row">
                                <td><i onclick="cloneRow()" class="glyphicon glyphicon-plus-sign"></i></td>
                                <td>
                                    <select class="form-control" name="product_id[]">
                                        @foreach ($products as $item)
                                        <option value="{{$item->id}}">{{$item->name_cht}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input type="text" class="form-control input-sm" name="qty[]" placeholder="價錢"></td>
                                <td><input type="text" class="form-control input-sm" name="price[]" placeholder="單價"></td>
                                <td><input type="text" class="form-control input-sm" name="total[]" placeholder="總計"></td>
                            </tr>
                            <tr id="toolbar">
                                <td colspan="4">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="order_id" value="{{ $row->id }}">
                                    <button type="submit" class="btn btn-default">確認補貨</button>
                                </td>
                                <td class="p_td">
                                    <p>運費:    {{$row->shipping_fee}} </p>
                                    <p>折扣:    0 </p>
                                    <p>總收費:  <span id="total"> {{number_format($total,2)}}</span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
var total = {{$total}};
function delOrderItem () {
    var url_params = getCheckedBoxValues('order_item_id');
    $.ajax({
        url: '/admin/orders/del-item',
        type: 'post',
        dataType: 'json',
        data: url_params,
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
        location.reload(true);
    });   
}
function cloneRow(){
    var obj = $('.product_row:eq(0)').clone(true);
    $('#toolbar').before(obj);
}
function AddOrderItemsAction(){
    $('#addOrderItems').toggle();
    $('#total_1').toggle();
}
function calculateTotal(){
    $.each($('input[name*="total"]'), function(){
        total += parseFloat($(this).val());
    });
    $('#total').text(total.toFixed(2));
}
$(function(){
    var confirm = $.scojs_confirm({
        action: function() {
            delOrderItem();
            this.close();
        }
    });

    $('input[name*="qty"]').on('keyup', function(event) {
        var val = parseInt($(this).val());
        $parent = $(this).parents('.product_row');
        $price = $parent.find('input[name*="price"]');
        $total = $parent.find('input[name*="total"]');
        var total = $price.val()*val;
        $total.val(total.toFixed(2));
        calculateTotal();
    }).on('blur', function(event) {
        var val = parseInt($(this).val());
        $(this).val(val);
    });

    $('input[name*="price"]').on('keyup', function(event) {
        var val = parseFloat($(this).val());
        $parent = $(this).parents('.product_row');
        $qty = $parent.find('input[name*="qty"]');
        $total = $parent.find('input[name*="total"]');
        var total = $qty.val()*val;
        $total.val(total.toFixed(2));
        calculateTotal();
    }).on('blur', function(event) {
        var val = parseFloat($(this).val());
        $(this).val( val.toFixed(2) );
    });

    $('input[name*="total"]').on('keyup', function(event) {
        var val = parseFloat($(this).val());
        $parent = $(this).parents('.product_row');
        $qty = $parent.find('input[name*="qty"]');
        $price = $parent.find('input[name*="price"]');
        var price = val/$qty.val();
        $price.val(price.toFixed(2));
        calculateTotal();
    }).on('blur', function(event) {
        var val = parseFloat($(this).val());
        $(this).val( val.toFixed(2) );
    });
})
</script>
@endsection