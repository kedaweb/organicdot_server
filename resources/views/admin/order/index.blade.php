@extends('admin._layouts.default')

@section('page_css')
<link href="/assets/DataTables/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection

@section('title', '交易資料')
@section('breadcrumb', '交易資料')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>
    <a href=" {{ $redirect_path }} ">全部</a> &nbsp;|&nbsp; 
    @foreach (trans('site.order_status') as $key => $item)
    <a href="{{ $redirect_path }}/?status={{$key}}">{{$item}}</a> &nbsp;|&nbsp;
    @endforeach
    </h4>
    <div>
        <button class="btn btn-default" onclick="batchExportCsv()">批次匯出CSV</button>
    </div>
    <div class="bg">
        <table id="orders_table" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>狀態</th>
                    <th>訂單</th>
                    <th>運送方式</th>
                    <th>支付方式</th>
                    <th>已購買</th>
                    <th>購買日期</th>
                    <th>總計</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td><input name="order_ids[]" type="checkbox"  value="{{ $item->id }}"></td>
                    <td>{{ trans('site.order_status')[$item->status] }}</td>
                    <td>
                        #{{ $item->id }} 買家 {{ $item->buyer_name }} <br>
                        {{ $item->email }}
                    </td>
                    <td>{{ $item->shipping_method }}</td>
                    <td>{{ trans('site.order_payment')[$item->payment_method] }}</td>
                    <td>{{ count($item->orderItems) }}</td>
                    <td>{{ $item->buy_date }}</td>
                    <td>{{number_format( ($item->total+$item->shipping_fee),2)}}</td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                        <br>
                        <br>
                        <a target="_blank" href="/admin/orders/export-csv/{{ $item->id }}"  class="btn btn-info">匯出CSV</a>
                        <a target="_blank" href="/admin/orders/print-receipt/{{ $item->id }}" class="btn btn-info">列出收據</a>
                        <a target="_blank" href="/admin/orders/print-itemlist/{{ $item->id }}" class="btn btn-info">列出貨單</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript" src="/assets/DataTables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/DataTables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
function batchExportCsv () {
    var url_params = getCheckedBoxValues('order_ids');
    window.location = '{{ $redirect_path }}/batch-export-csv/?' + url_params;
}
$(function(){
    $('#type_select').on('change', function(event) {
        window.location = "{{ $redirect_path }}/?" + $('#query').serialize();
    });

    $('#orders_table').DataTable({
        "columnDefs": [{
            "targets": [0],
            "searchable": false
        }]
    });
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection