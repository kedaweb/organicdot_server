<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>有機點 - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/scojs.css" rel="stylesheet">
<link href="/assets/css/sco.message.css" rel="stylesheet">
@yield('page_css')
<link href="/assets/css/admin_style.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/css/printstylesheet.css" media="screen, print" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="printHtml">
<div class="container">
    <div class="row">
        <div class="col-xs-8">
            <img src="/assets/images/logo2.png">
            <span class="fontcolor2" style="font-size: 30px">Organic Dot 有&nbsp;&nbsp;機&nbsp;&nbsp;點</span>
        </div>
        <div class="col-xs-4">
            <table class="table table-bordered table-condensed ">
                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;發票編號:</td>
                        <td>&nbsp;&nbsp;W15{{ str_pad($row->id,6,"0",STR_PAD_LEFT) }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;賬單日期:</td>
                        <td>&nbsp;&nbsp;{{date("d/m/Y")}}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;付款方式:</td>
                        <td>&nbsp;&nbsp;{{ trans('site.order_payment')[$row->payment_method] }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;頁數:</td>
                        <td>&nbsp;&nbsp;1/1</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered customer">
                <thead>
                    <tr>
                        <th class="color1">客戶資料</th>
                        <th class="color2">送貨資料</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>姓名: {{ $row->member->name or "" }}</td>
                        <td>姓名: {{ $row->buyer_name }}</td>
                    </tr>
                    <tr>
                        <td>地址: {{ $row->member->shipping_address or "" }}</td>
                        <td>地址: {{ $row->shipping_address }}</td>
                    </tr>
                    <tr>
                        <td>電話: {{ $row->member->phone or "" }}</td>
                        <td>電話: {{ $row->phone }}</td>
                    </tr>
                    <tr>
                        <td>電郵: {{ $row->member->email or "" }}</td>
                        <td>電郵: {{ $row->email }}</td>
                    </tr>
                    <tr>
                        <td>備註: </td>
                        <td>
                            送貨時段:
                            @if ($row->shippingTimes())
                            @foreach ($row->shippingTimes()->lists('name_cht') as $item)
                                {{ $item }} &nbsp;
                            @endforeach
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th class="color2">標籤</th>
                        <th class="color2" width="40%">產品說明</th>
                        <th class="color2">數量</th>
                        <th class="color2">單位</th>
                        <th class="color2">單價</th>
                        <th class="color2">金額</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($row->orderItems as $item)
                    <tr>
                        <td></td>
                        <td>{{ $item->product->name_cht or $item->product_name }}</td>
                        <td class="text-center">{{$item->qty}}</td>
                        <td class="text-center">{{ $item->product->weight_unit_cht or "" }}</td>
                        <td class="text-right">{{$item->price}}</td>
                        <td class="text-right">{{ number_format($item->qty*$item->price, 2)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container checkout">
    <div class="row">
        <div class="col-md-9"></div>
        <div class=" col-xs-3 pull-right">
            <ul class="list-unstyled">
                <li>小結 + <span class="pull-right">{{ number_format($total,2) }}</span></li>
                <li>折扣  - <span class="pull-right">{{number_format(0,2)}}</span></li>
                <li>運費 + <span class="pull-right" >{{ number_format($row->shipping_fee,2) }}</span></li>
            </ul>
            <div class="amoutline">總金額(港幣): <span class="pull-right">{{ number_format($total+$row->shipping_fee,2) }}</span></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <p>
                謝謝選擇 <span class="fontcolor1">Organic Dot 有機點</span> <br>
                如有任何貨品問題,請致電 +852 2686 8332與我們聯絡。<br>
            </p>
            <table>
                <tbody>
                    <tr>
                        <td valign="top">熱線開放時間：</td>
                        <td>星期一至五 早上9時半至下午5時半 <br>星期六 早上9時半至下午12時半 (公眾假期除外)</td>
                    </tr>
                </tbody>
            </table>
            <p><span class="fontcolor1">最新消息: </span></p>
            <p class="text-left companyInfo">香港火炭坳背灣街34-36號豐盛工業中心B座16樓7室 <br>
            Flat 7, 16F, Block B, Veristrong Industrial Center, 34-36 Au Pui Wan Street, Fo Tan, N.T.H.K <br>   
            電話:+852 2686 8332&nbsp;傳真:+852 3956 9389&nbsp; Facebook: facebook@organicdot.com&nbsp;Website: http://www.organicdot.com
            </p>
        </div>
    </div>
</div>
</div>
</body>
</html>