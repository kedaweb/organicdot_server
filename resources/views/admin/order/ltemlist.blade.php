<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>有機點 - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/scojs.css" rel="stylesheet">
<link href="/assets/css/sco.message.css" rel="stylesheet">
@yield('page_css')
<link href="/assets/css/admin_style.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
.container{width: 800px !important}
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-8"><img src="/assets/images/logo.png"></div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>標籤</th>
                        <th>產品說明</th>
                        <th>數量</th>
                        <th>單價</th>
                        <th>金額</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($row->orderItems as $item)
                    <tr>
                        <td></td>
                        <td>{{$item->product->name_cht or $item->product_name}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{ number_format($item->qty*$item->price, 2)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>