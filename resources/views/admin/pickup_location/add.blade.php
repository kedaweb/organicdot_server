@extends('admin._layouts.default')


@section('title', '提貨地址')
@section('breadcrumb', '提貨地址')

@section('content')
<div class="jumbotron">
    <h4>新增提貨地址</h4>
    <div class="bg">
        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/add">
            <div class="form-group">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 10px">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">English</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Trad.Chinese</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">名稱(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_en" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">地址(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="address_en" value="">
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">名稱</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_cht" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">地址</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="address_cht" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">顯示</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input style="margin-left: 0" name="is_show" type="checkbox" value="1" checked >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default">新增</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection