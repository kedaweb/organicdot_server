@extends('admin._layouts.default')


@section('title', '提貨地址')
@section('breadcrumb', '提貨地址')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增提貨地址</h4>
    <div class="bg">
        <a class="btn btn-default" href="{{ $redirect_path }}/add/">新增提貨地址</a>
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron"> 
    <div class="jumbotron_wrap">
    <h4>所有提貨地址</h4>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>提貨點</th>
                    <th>顯示</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td>{{ $item->name_cht }} ({{ $item->address_cht }})</td>
                    <td><input onclick="edit_show({{ $item->id }}, 'is_show', this)" name="is_show" type="checkbox" {{ $item->is_show ? "checked" : "" }} ></td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function edit_show(id, field, obj) {
    var value = $(obj).is(":checked") ? 1 : 0;
    $.ajax({
        url: '{{ $redirect_path }}/edit-field',
        type: 'POST',
        dataType: 'json',
        data: {id: id, field: field,value: value},
    })
    .done(function(response) {
        $.scojs_message(response.info, $.scojs_message.TYPE_OK);
    })
}
$(function(){
    $('#selecter').on('change', function(event) {
        window.location = "{{ $redirect_path }}/?" + $('#query').serialize();
    });
})
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection