<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
    <thead>
        <tr>
            <th>姓名</th>
            <th>email</th>
            <th>電話區號</th>
            <th>電話</th>
            <th>郵寄地址</th>
            <th>賬單地址</th>
        </tr>
    </thead>
    @foreach ($rs as $item)
    <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->email}}</td>
        <td>{{$item->areacodes}}</td>
        <td>{{$item->phone}}</td>
        <td>{{$item->shipping_address}}</td>
        <td>{{$item->bill_address}}</td>
    </tr>
    @endforeach
</table>
</html>