@extends('admin._layouts.default')


@section('title', '會員管理')
@section('breadcrumb', '會員管理')

@section('content')
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>新增會員</h4>
    <div class="bg">
        <a class="btn btn-default" href="{{ $redirect_path }}/add/">新增會員</a>
    </div>
    </div>
</div>
<div class="jumbotron" style="padding: 0 15px">
    <div class="line"></div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>搜索會員</h4>
    <div class="bg">
        <form id="query" class="form-inline" method="get" action="{{ $redirect_path }}">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="會員名稱" name="name" value="{{ isset($name) ? $name : '' }}">
            </div>
            <button type="submit" class="btn btn-primary">搜索</button>
        </form>
    </div>
    </div>
</div>
<div class="jumbotron">
    <div class="jumbotron_wrap">
    <h4>所有會員 (現有會員數目: {{$lists->count()}})</h4>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-2">
                <button class="btn btn-default" onclick="batchExportCsv()">批次匯出CSV</button>
            </div>
        </div>
    </div>
    <div class="bg">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>會員名稱</th>
                    <th>電話</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lists as $key => $item)
                <tr>
                    <td><input name="ids[]" type="checkbox"  value="{{ $item->id }}"></td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->phone }} </td>
                    <td>
                        <a class="btn btn-success" href="{{ $redirect_path }}/edit/{{ $item->id }}">編輯</a>
                        <a class="btn btn-danger" data-action="{{ $redirect_path }}/delete/{{ $item->id }}" data-title="是否確定刪除" data-trigger="confirm">刪除</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
function batchExportCsv () {
    var url_params = getCheckedBoxValues('ids');
    window.location = '{{ $redirect_path }}/batch-export-csv/?' + url_params;
}
</script>
@if ( session('message') )
<script type="text/javascript">
$(function(){
    $.scojs_message("{{ session('message') }}", $.scojs_message.TYPE_OK);
})
</script>
@endif

@endsection