@extends('admin._layouts.default')


@section('title', '會員管理')
@section('breadcrumb', '會員管理')

@section('content')
<div class="jumbotron">
    <h4>修改會員</h4>
    <div class="bg">
        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/edit">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">姓名</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="name" placeholder="姓名" value="{{ $row->name }}">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">電郵</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="email" placeholder="電郵" value="{{ $row->email }}">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">聯繫電話</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="phone" placeholder="聯繫電話" value="{{ $row->phone }}">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">確認密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">收貨地址</label>
                <div class="col-sm-5">
                    <textarea class="form-control" name="shipping_address">{{ $row->shipping_address }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $row->id }}">
                    <button type="submit" class="btn btn-default">修改</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection