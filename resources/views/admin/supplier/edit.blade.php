@extends('admin._layouts.default')


@section('title', '供應商管理')
@section('breadcrumb', '供應商管理')

@section('content')
<div class="jumbotron">
    <h4>修改供應商</h4>
    <div class="bg">
        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/edit">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">網頁位置</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="sorting" value="{{ $row->sorting }}">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">專業認證</label>
                <div class="col-sm-4">
                    @foreach ($certs as $item)
                    <label class="checkbox-inline">
                    @if (in_array($item->id, $row_certs))
                    <input type="checkbox" name="cert_ids[]" value="{{ $item->id }}" checked> {{ $item->name_cht }}
                    @else
                    <input type="checkbox" name="cert_ids[]" value="{{ $item->id }}" > {{ $item->name_cht }}
                    @endif
                    </label>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">相片<br>(尺寸:1080x600 <br> *最多3張圖片)</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-4">
                            @if ($row->img1)
                            <img class="img-responsive" src="{{ $row->img1 }}">
                            @endif
                            <input type="file" name="img1">
                        </div>
                        <div class="col-sm-4">
                            @if ($row->img2)
                            <img class="img-responsive" src="{{ $row->img2 }}">
                            @endif
                            <input type="file" name="img2">
                        </div>
                        <div class="col-sm-4">
                            @if ($row->img3)
                            <img class="img-responsive" src="{{ $row->img3 }}">
                            @endif
                            <input type="file" name="img3">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 10px">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">English</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Trad.Chinese</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">供應商名稱(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_en" value="{{ $row->name_en }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">供應商地址(英)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="address_en" value="{{ $row->address_en }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">農場簡介(英)</label>
                                <div class="col-sm-5">
                                    <textarea rows="10" class="form-control" name="content_en" value="{{ $row->content_en }}"></textarea>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">供應商名稱</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name_cht" value="{{ $row->name_cht }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">供應商地址</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="address_cht" value="{{ $row->address_cht }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">農場簡介</label>
                                <div class="col-sm-5">
                                    <textarea rows="10" class="form-control" name="content_cht" value="{{ $row->content_cht }}"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Website</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input type="text" class="form-control" name="website" value="{{ $row->website }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Facebook</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input type="text" class="form-control" name="facebook" value="{{ $row->facebook }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">顯示</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <input style="margin-left: 0" name="is_show" type="checkbox" value="1" {{ $row->is_show ? "checked" : "" }} >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $row->id }}">
                    <button type="submit" class="btn btn-default">修改</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection