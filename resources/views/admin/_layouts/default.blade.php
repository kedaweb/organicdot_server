<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>有機點 - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/scojs.css" rel="stylesheet">
<link href="/assets/css/sco.message.css" rel="stylesheet">
@yield('page_css')
<link href="/assets/css/admin_style.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="backend">
<div class="skin">
    <div class="navbar backend-navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/images/logo.png"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <ul class="list-unstyled">
                    <li><a class="btn btn-danger logout" href="/admin/auth/logout">退出</a></li>
                </ul>
            </div>
        </div>
        <div class="container"><div class="line"></div></div>
    </div>
    <nav>
        <div class="container">
            <ul id="nav" class="nav nav-pills nav-justified">
                @foreach (Auth::user()->role()->permissions() as $perm)
                @if ($redirect_path == $perm->url)
                <li class="active" role="presentation"><a href="{{$perm->url}}">{{$perm->description}}</a></li>
                @else
                <li role="presentation"><a href="{{$perm->url}}">{{$perm->description}}</a></li>
                @endif
                @endforeach
            </ul>
        </div>
    </nav>
    <div class="warp container">
        <div class="breadbg">
            <ol class="breadcrumb">
                <li><a href="#">主頁</a></li>
                <li><a href="#">@yield('breadcrumb')</a></li>
            </ol>
        </div>
        @yield('content')
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="text-right">&copy; 2015 OrganicDot All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@yield('modal-title')</h4>
            </div>
            @yield('modal-body')
        </div>
    </div>
</div>
<script src="/assets/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/sco.message.js"></script>
<script src="/assets/js/sco.modal.js"></script>
<script src="/assets/js/sco.confirm.js"></script>
<script src="/assets/js/public.js"></script>
<script type="text/javascript">
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})
</script>
@yield('page_js')
</body>
</html>