$category = $('#category');
$parent_category = $('#p_cate');
$parent_category.on('change', function(event) {
    var val = $(this).val();
    if(val != 0)
    {
        $.ajax({
            url: '/admin/category/category-json/',
            type: 'GET',
            dataType: 'json',
            data: {parent_id: $(this).val()},
        })
        .done(function(response) {
            $category.empty();
            $category.append('<option value="">全部</option>');
            $.each(response.root, function(index, val) {
                var str = '<option value="'+ val.id +'">'+val.name_cht+'</option>'
                $category.append(str);
                @if (isset($category_id))
                $category.val({{ $category_id }});
                @endif
            });
        })
    }else{
        $category.empty();
        $category.append('<option value="">---</option>');
    }
});