<?php 

$pp_text = <<<TEXT
<p><strong>We protect your personal information and data</strong></p>
<p><strong>1. Protecting You</strong></p>
<p>The Organic Dot Hong Kong website www.OrganicDot.com is owned and operated by Organic Dot Ltd., Organic Dot Ltd., ("Organic Dot", "we," "us," "our") is committed to protecting your privacy. We will only use the information that we collect about you lawfully.</p>
<p>Please note that use of the Website by you is subject to this privacy policy (the "Policy") and the Website’s&nbsp;terms and conditions&nbsp;of use ("Terms and Conditions"). Where this Policy uses a word starting with a capital letter, that term will be defined in the Terms and Conditions or elsewhere in this Policy.</p>
<p>Our Policy explains what information we collect on the Website, how we use and/or share this information, and how such information is maintained. By using this Website, you signify your acceptance of this Policy. If you do not agree to the terms of this Policy, in whole or part, you should not use this Website. Please note that this Policy applies only with respect to the information collected on the Website and not any information collected or obtained through other methods or sources.</p>
<p>Our Policy ensures that any information you provide us remains private and secure. To reassure you, below we provide details of the information you provide to us, and how it will and, more importantly, will not be used. We will never collect sensitive information about you without your explicit consent. The information we hold will be accurate and up to date. You can check the information that we hold about you by contacting us (and there may be an administrative fee payable for this). If you find any inaccuracies we will delete or correct it promptly.</p>
<p><strong>2. Personally Identifiable Information &ndash; Collection of PII that we collect</strong></p>
<p>We collect personally identifiable information ("PII") that is volunteered during Registration or in response to specific information requests explicitly presented to you.</p>
<p>We may also collect your IP (Internet Protocol) address to help diagnose problems with our server, and to administer this Website. An IP address is a number that is assigned to your computer when you use the Internet. Your IP address is also used to help identify you during a particular session and to gather broad demographic data.</p>
<p>We may ask for your e-mail address during your use of the Website. Also, to purchase, you may be requested to provide any or all of the following PII: first and last name, postal address, e-mail address, date of birth, telephone number and details of a payment mechanism, such as credit card details.</p>
<p><strong>3. Use and Sharing of PII</strong></p>
<p>All PII is retained in accordance with the Data Protection Principles and other applicable data protection laws in Hong Kong.</p>
<p>We use PII to provide you with information about Organic Dot, to properly provide you with the Service and for marketing ourselves to you. It is our policy not to sell or pass any PII to any other organizations (outside our group companies) unless we are required to do so for fraud prevention and detection.&nbsp;<strong>We will never sell or rent your PII to anyone else for their marketing purposes without your consent.</strong></p>
<p>However, we may pass your PII to our agents and subcontractors to help us with any of our uses of your data set out in this Policy. For example, we may use third parties to provide us with marketing or customer service assistance or send it to a credit card provider to process a payment. Our third party online payment system collects and records your first payment information as convenience, which will be transmitted by SSL directly to the bank for processing payment for your transaction and future transactions. Please note that Organic Dot does not store any of this data and does not have any access to this data..</p>
<p>We also always reserve the right to disclose PII in order to:<br>
(a) comply with applicable laws;<br>
(b) respond to governmental enquiries (or enquiries from a legal, governmental or quasi-governmental or local authority agency or Internet protection agency of any type);<br>
(c) comply with a valid legal process or procedure; or<br>
(d) protect our rights or property, this Website, and/or other users of this Website.</p>
<p>By submitting PII to us, you acknowledge that provided we have used your data in the ways set out in this Policy, we cannot be held responsible for any use of your data by third parties who receive and process your data.</p>
<p><strong>4. Cookies</strong></p>
<p>We use "cookies", technology to store data in your computer using the functionality of your browser. A lot of websites do this, because cookies allow the website publisher to do useful things like find out whether the computer (and probably its user) has visited the site before. You can usually modify your browser to prevent cookie use &ndash; but if you do this the Service (and the Website) may not work properly. The information stored in the cookie is used to identify you. This enables us to operate an efficient service and to track the patterns of behavior of visitors to the website.</p>
<p>Also, in the course of serving advertisements to this Website if any), third-party advertisers or ad servers may place or recognize a unique cookie on your browser. The use of cookies by such third party advertisers or ad servers is not subject to this Policy, but is subject to their own respective privacy policies. (Please note that use of the Website, is neither intended for, nor directed to, children under the age of 18.)</p>
<p><strong>5. Emails</strong></p>
<p>Please note that we may email you for the following purposes:</p>
<ul>
<li>as part of the Service. For example, we send (or may send) emails to you in some of the following example circumstances:</li>
<li>after Registration, notifying you of your account details;</li>
<li>as reminder emails about the services we offer (particularly if you have not used them yet or not used them for a while);</li>
<li>to send you information or Vouchers which you have asked for;</li>
<li>as a newsletter;</li>
<li>as promotional emails; and</li>
<li>to offer related services to you from Organic Dot Ltd.</li>
</ul>
<p><strong><br>
<strong>However, in terms of any marketing or promotional emails we may send, we will always give you the chance to opt-out (or unsubscribe) from them in the future.</strong></strong></p>
<p><strong>6. Security and retention</strong></p>
<p>The PII which we hold will be held securely within our systems in accordance with our internal security policy and the law.</p>
<p>Regarding information transmitted between the Website and and/or users, while we take reasonable precautions to safeguard this information, we may be unable to prevent unauthorized access to such information by third parties or inadvertent disclosure of such information during transit. Users acknowledge this risk when communicating with the Website.</p>
<p><strong>7. Transfer in Certain Circumstances</strong></p>
<p>If there is a sale, merger, consolidation, change in control, transfer of substantial assets, reorganization or liquidation or Organic Dot Ltd. then, in our sole discretion, we may transfer, sell or assign information collected on and through this Website (including, without limitation, PII and other information), to one or more relevant third parties.</p>
<p><strong>8. Customer Service</strong></p>
<p>If you have any questions or concerns, please feel free to send enquiries to<a href="mailto:info@organicdot.com">info@organicdot.com</a></p>
<p><strong>9. Changes to this Policy</strong></p>
<p>Please note that this Policy forms part of the&nbsp;Terms &amp; Conditionsfor use of this Website and forms part of the Agreement between you and us. We may, from time to time, amend this Policy, in whole or part, in our sole discretion. Any changes to this Policy will be effective immediately upon the posting of the revised policy on the Website. Depending on the nature of the change, we may announce the change: (a) on the home page of the Website, or (b) by email, if we have your email address. However, in any event, by continuing to use the Website following any changes, you will be deemed to have agreed to such changes. If you do not agree with the terms of this Policy, as it may be amended from time to time, in whole or part, you must terminate your use of the Website.</p>
<p>&nbsp;</p>
TEXT;

return [
    "openlogin"=>"Login",
    "login"=>"Member Login",
    "reg"=>"Register",
    "jumpToNext"=>"Skip",
    "home"=>"Home",
    "recommend"=>"Recommend",
    "shipping"=>"Shipping Data",
    "cart"=>"My Cart",
    "contact"=>"Contact Us",
    "farm"=>"Suppliers",
    "forgetpwd"=>"Forget Password",
    "orders"=>"My Orders",
    "favorites"=>"My Favorites",
    "profile"=>"My Profile",
    "BackHome"=>"Back to Home",
    "pickupSelect"=>"Select Pickup-location",
    "desc"=>"Brief",
    "orderNo"=>"Order No.",
    "payMethod"=>"Payment",
    "shippingFee"=>"shipping Fee",
    "total"=>"Total",
    "website"=>"Web",
    "address"=>"Address",
    "name"=>"Name",
    "email"=>"Email",
    "phone"=>"Phone",
    "pwd"=>"Password",
    "pwd_confirm"=>"Password Conifrm",
    "buyer"=>"Consignee",
    "shippingAddress"=>"Consignee Address",
    "shippingTime"=>"Shipping Time",
    "shippingClause"=>"Shipping Disclaimer",
    "collect"=>"Collect",
    "cancelcollect"=>"Cancel",
    "addCart"=>"Add Cart",
    "buyBtn"=>"Buy",
    "loginBtn"=>"Login",
    "regBtn"=>"Register",
    "logoutBtn"=>"Logout",
    "closeBtn"=>"Close",
    "sendBtn"=>"Send",
    "checkOutBtn"=>"Check Out",
    "confirmBtn"=>"Confirm",
    "cancelBtn"=>"Cancel",
    "hasStock"=>"On sale",
    "soldOut"=>"sold out",
    "account"=>"My Account",
    "PrivacyPolicy"=>"Privacy Policy",
    "shippingMethod"=>"Shipping Method",
    "sum"=>"sum",
    "langSelect"=>"Select Language",
    "chooseMethod"=>"Choose Method",
    "nutrition"=>"Nutrition",
    "efficacy"=>"Efficacy",
    "savingMethod"=>"Saving Method",
    "shippingDetails"=>"Shipping Details",
    "relProduct"=>"Related Products",
    "continueShopping"=>"Continue",
    "payNow"=>"Pay Now",
    "fare"=>"Fare",
    "deliveryPoint"=>"Delivery Point Pickup",
    "storePickup"=>"Store Pickup",
    "free"=>"Free",
    "billAddress"=>"Bill Address",
    "delBtn"=>"Delete",
    "require"=>"Require",
    "tip_title"=>"Tip Info",
    "COD"=>"COD",
    "payByBlank"=>"Bank payment<br>Bank account: 124-079914-001<br> HSBC Organic Dot Ltd.<br>After payment, please WhatsApp to +852 68062567 or call +852 26868332",
    "areacodes"=>"Area Codes",
    "rememberme"=>"Remember me",
    "original_price"=>"original price:",
    "tip1"=>"Please Enter Phone And Password",
    "tip2"=>"Phone or Password Incorrect",
    "tip3"=>"Please Login",
    "tip4"=>"Please Enter Number",
    "tip5"=>"over",
    "tip5_1"=>"for Free",
    "tip6"=>"Please Buy Goods",
    "tip7"=>"System is busy, please try again later",
    "tip8"=>"Used as a login, can not be modified registration",
    "tip9"=>"Register Success!",
    "tip10"=>"Can not Modify!",
    "tip11"=>"Success!",
    "tip12"=>"Email not exists!",
    "tip13"=>"Loading...",
    "tip14"=>"Cart is empty, please add product first!",
    "tip15"=>"Please pay the order",
    "tip16"=>"If same as shipping address, check the checkbox",
    "tip17"=>"If you use the data to register, please check the checkbox",
    "tip18"=>"Login Success",
    "tip19"=>"If you're not a member, please fill in the shipping information",
    "tip20"=>"Please fill in the shipping information, and then click the send to complete the shopping process",
    "tip21"=>"Please scan the correct QR code!",
    "tip22"=>"Insufficient number of selected goods store",
    "msg1"=>"Success!<br>You have completed shopping program<br>We will follow the requirements of delivery",
    "msg2"=>"Success!<br>You have completed shopping program<br>We will follow the requirements of delivery",
    "msg3"=>"Congratulations<br>Goods have been added to Cart",
    "msg4"=>"Are you sure you want to exit the program?",
    "PrivacyPolicyContent"=> $pp_text,
    "shippingClauseContent"=>"送貨方式所有香港訂單,均由本公司負責運送,在每次安排付運前,客戶服務員將以電話聯絡您,以便安排最合適的送貨時間。在一些特殊情況下,貨品可能會由香港郵政提供的本地郵政快遞付運。<br /><br />國內訂單均由速遞公司付運,而海外訂單則由香港郵政提供的特快專遞付運,所需時間視乎您所選的運送目的地而定。<br />所有訂單必須完成網上付款後,貨品方會寄出。<br><br>運費<br>所有寄往香港的訂單均包含免費送貨服務,但請注意,離島地區不提供送貨服務。而寄往香港以外地區,客人需要支付有關運費,運費則視乎您所選的運送目的地及送貨方式而定。<br><br>運送時間<br>當收到您的訂單,i simple會盡快處理包裝及付運,香港訂單一般會於7個工作天內收到貨品。而其他地區訂單須視乎您所選的運送目的地及送貨方式而定,所有國內及海外訂單被送出後,我們會發出電郵通知。請注意,所有列明的送貨時間均為預計運送時間,我們並不能完全保證運送時間不受延誤,但我們會竭盡所能,為您盡快處理及付運訂單。<br><br>無法派送的訂單<br>部分訂單會因收貨地址不詳、郵政信箱、酒店地址、多次派送均無人收件、收件人拒絕繳付稅項/ 拒絕提供身份證/ 未能為貨物進行報關或收件人拒收包裹等原因而被退回。<br>當貨品被退回時,我們會扣除訂單貨品總額的百分之十五作為手續費,餘數將按「退款政策」及「退款時間」處理。"
];

 ?>