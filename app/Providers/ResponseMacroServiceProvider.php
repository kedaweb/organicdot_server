<?php namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('CORS_header', function() use ($response)
        {
            return $response->header('Access-Control-Allow-Origin' , '*')->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin');
        });
    }

    public function register()
    {

    }

}