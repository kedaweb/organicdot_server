<?php

namespace App\Http\Controllers;

use App\Model\System_config;
use App\Model\Order;
use App\Model\Order_items;

use Illuminate\Http\Request;

abstract class abstractPaypalControllers extends Controller {

    public $params;
    public $paypalNvp;

    public function __construct() 
    {
        $this->paypalNvp = 'https://api-3t.paypal.com/nvp';
        $this->params['USER'] = 'carson_api1.organicdot.com';
        $this->params['PWD'] = 'TBQVXLJWTVN3H6UQ';
        $this->params['SIGNATURE'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AfRN7W8IUxDAfMwpLQPdodSNAdF3';
        //测试环境
        if(env('APP_DEBUG')){        
            $this->paypalNvp = 'https://api-3t.sandbox.paypal.com/nvp';
            $this->params['USER'] = '42565372-facilitator_api1.qq.com';
            $this->params['PWD'] = 'PB49BZU7FUGXV24R';
            $this->params['SIGNATURE'] = 'A3r-q0pNLtSobLxaS0P-AizYOY38AFWrM5UpgyjV8ZcSzqlOZ3-YwpD4';
        }
        $this->params['VERSION'] = '124.0';
    }
    /**
     * 保存订单数据
     * @param  Array $ECDetails paypal返回的订单数据
     * @return no return
     */
    protected function saveOrder($ECDetails)
    {
        //檢查訂單內有多少商品
        $i = 0;
        foreach ($ECDetails as $key => $value) {
            if(strstr($key, 'L_PAYMENTREQUEST_0_NUMBER')){
                $i++;
            }
        }
        $shipping_address = "";
        $shipping_address .= isset($ECDetails['PAYMENTREQUEST_0_SHIPTOSTATE']) ? $ECDetails['PAYMENTREQUEST_0_SHIPTOSTATE'] : "";
        $shipping_address .= isset($ECDetails['PAYMENTREQUEST_0_SHIPTOCITY']) ? $ECDetails['PAYMENTREQUEST_0_SHIPTOCITY'] : "";
        $shipping_address .= isset($ECDetails['PAYMENTREQUEST_0_SHIPTOSTREET']) ? $ECDetails['PAYMENTREQUEST_0_SHIPTOSTREET'] : "";
        //創建訂單
        $order = Order::create([
            'payment_method' => 1,
            'buy_date' => date('Y-m-d H:i:s'),
            'buyer_name' => isset($ECDetails['PAYMENTREQUEST_0_SHIPTONAME']) ? $ECDetails['PAYMENTREQUEST_0_SHIPTONAME'] : "",
            'phone' => isset($ECDetails['PAYMENTREQUEST_0_SHIPTOPHONENUM']) ? $ECDetails['PAYMENTREQUEST_0_SHIPTOPHONENUM'] : "",
            'shipping_address' => $shipping_address,
            'paypal_token' => $ECDetails['TOKEN'],
            'payer_id' => $ECDetails['PAYERID'],
            'shipping_fee' => isset($ECDetails['PAYMENTREQUEST_0_SHIPPINGAMT']) ? $ECDetails['PAYMENTREQUEST_0_SHIPPINGAMT'] : "",
        ]);
        //保存訂單內產品
        $items = [];
        for ($j= 0; $j < $i; $j++) {
            $orderItem = new order_items([
                "product_name" => $ECDetails['L_PAYMENTREQUEST_0_NAME'.$j],
                "product_id" => $ECDetails['L_PAYMENTREQUEST_0_NUMBER'.$j],
                "qty" => $ECDetails['L_PAYMENTREQUEST_0_QTY'.$j],
                "price" => $ECDetails['L_PAYMENTREQUEST_0_AMT'.$j],
            ]);
            $orderItem->product->stock -= $ECDetails['L_PAYMENTREQUEST_0_QTY'.$j];
            if($orderItem->product->stock <= 0){
                $orderItem->product->stock_status = 0;
            }
            $orderItem->product->save();
            $items[] = $orderItem;
        }
        $order->orderItems()->saveMany($items);
    }

    public function getIndex(Request $request) 
    {
        $data['fail'] = true;
        return view('paypal.success', $data);
    }

    public function getToken(Request $request)
    {
        $sysConfig = System_config::getConfig();
        $ECparams = $this->params;
        $ECparams['METHOD'] ='SetExpressCheckout';
        $ECparams['RETURNURL'] = $request->has('returnurl') ? url($request->input('returnurl')) : url('api/paypal/sec');
        $ECparams['CANCELURL'] = $request->has('cancelurl') ? url($request->input('cancelurl')) :url('api/paypal');
        if($request->has('cart')){
            $total = 0;
            $input = $request->only('cart', 'locale', 'shipping_method');
            $shipping_fee = $input['shipping_method'] == 1 ? $sysConfig['shipping_fee'] : 0;
            foreach ($input['cart'] as $key => $item) {
                $item = json_decode($item);
                $ECparams['L_PAYMENTREQUEST_0_NAME'.$key] = 'cn' == $input['locale'] ? $item->name_cht : $item->name_en;
                $ECparams['L_PAYMENTREQUEST_0_AMT'.$key] = $item->price;
                $ECparams['L_PAYMENTREQUEST_0_QTY'.$key] = $item->qty;
                $ECparams['L_PAYMENTREQUEST_0_NUMBER'.$key] = $item->id;
                $total += $item->qty*$item->price;
            }
            $shipping_fee = $total >= (float)$sysConfig['shipping_discount'] ? 0 : $shipping_fee;

            $ECparams['PAYMENTREQUEST_0_PAYMENTACTION'] = 'SALE';
            $ECparams['PAYMENTREQUEST_0_AMT'] = $total + $shipping_fee;
            $ECparams['PAYMENTREQUEST_0_CURRENCYCODE'] = 'HKD';
            $ECparams['PAYMENTREQUEST_0_SHIPPINGAMT'] = $shipping_fee;
            $ECparams['PAYMENTREQUEST_0_ITEMAMT'] = $total;
            
            $token_params = curlGet($this->paypalNvp, $ECparams);
            $response = convertUrlQuery( urldecode($token_params) );
            if('Success' == $response['ACK']){
                $response['ECparams'] = $ECparams;
                return response()->json($response)->setCallback($request->input('callback'));
            }else{
                return response()->json(['info' => 'paypal 服務暫停'])->setCallback($request->input('callback'));;
            }
        }
        return response()->json(['info' => 'paypal 服務暫停'])->setCallback($request->input('callback'));;
    }

    abstract public function getSec(Request $request);
}