<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\Supplier;
use App\Model\Category;
use App\Model\Member;
use App\Model\Order;
use App\Model\Order_items;

use App\Http\Controllers\abstractPaypalControllers;
use Illuminate\Http\Request;
use Validator;
use Lang;
use QrCode;
use Log;

class frontController extends abstractPaypalControllers {

    public function __construct() 
    {
        parent::__construct();
    }

    //语言选择页
    public function getWelcome(Request $request)
    {
        return view('welcome');
    }

    //语言选择页
    public function getSetLanguage(Request $request)
    {
        return view('setlanguage');
    }

    //首页
    public function getIndex(Request $request)
    {
        return view('index');
    }
    
    //今期推荐页
    public function getRecommend(Request $request)
    {
        $data['top_title'] = Lang::get('front.recommend');
        return view('recommend', $data);
    }

    //分类列表页
    public function getCategory($id)
    {
        $data['row'] = Category::find($id);
        return view('category', $data);
    }

    //产品详细页
    public function getProduct($id)
    {
        $data['product'] = Product::find($id);
        return view('productDetail', $data);
    }

    //购物车
    public function getCart()
    {
        return view('cart');
    }

    //供应商列表页
    public function getFarms()
    {
        return view('farms');
    }

    //供应商详细页
    public function getFarm($id)
    {
        $data['row'] = Supplier::find($id);
        return view('farmDetail');
    }

    //联系页
    public function getContact()
    {
        return view('contact');
    }

    //私隐权政策页
    public function getPrivacyPolicy()
    {
        return view('privacypolicy');
    }

    //
    // public function getPaypal()
    // {
    //     return view('paypal');
    // }

    //paypal支付完成后返回页
    public function getSec(Request $request)
    {
        $sToken = $request->input('token');
        $sPayerID = $request->input('PayerID');
        
        $data['fail'] = true;
        if ($sToken == '' && $sPayerID == '') {
            return view('paypal_return', $data);
        } else {
            //獲取支付訂單的信息
            $ECparams = $this->params;
            $ECparams['METHOD'] ='GetExpressCheckoutDetails';
            $ECparams['TOKEN'] = $sToken;

            $response = curlGet($this->paypalNvp, $ECparams);
            $ECDetails = convertUrlQuery( urldecode($response) );
            
            if('Success' == $ECDetails['ACK'])
            {

                $ECparams['METHOD'] = 'DoExpressCheckoutPayment';
                $ECparams['PAYERID'] = $ECDetails['PAYERID'];
                $ECparams['PAYMENTREQUEST_0_AMT'] = $ECDetails['AMT'];
                $ECparams['PAYMENTREQUEST_0_CURRENCYCODE'] = $ECDetails['CURRENCYCODE'];
                $ECparams['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
                $response = curlGet($this->paypalNvp, $ECparams);
                $ECPayment = convertUrlQuery( urldecode($response) );

                //支付成功.支付成功時才創建訂單
                if('Success' == $ECPayment['ACK']){
                    
                    $this->saveOrder($ECDetails);

                    $data['fail'] = false;

                }else{ 
                    //支付失敗.記錄日誌
                    Log::info('User failed to use paypal', ['orderInfo' => $ECDetails, 'context' => $ECPayment]);
                }
            }
        }
        
        return view('paypal_return', $data);
    }

    //填写运送资料页
    public function getShipping()
    {
        return view('shipping');
    }
    
    //填写运送资料页--提货点
    public function getPickup()
    {
        return view('pickup');
    }
    
    //填写运送资料页--自取
    public function getHelpyourself()
    {
        return view('helpyourself');
    }
}
