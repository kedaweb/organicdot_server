<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\Category;
use App\Model\System_config;
use App\Model\Member;
use Validator;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class memberController extends BaseController
{
    public function getLogin(Request $request)
    {
        return view('login');
    }

    public function getRegister()
    {
        return view('register');
    }

    public function getOrders(Request $request)
    {
        return view('myorders');
    }

    public function getFavorites(Request $request)
    {
        return view('favorites');
    }

    public function getProfile(Request $request)
    {
        return view('profile');
    }

    public function getForgetpwd(Request $request)
    {
        return view('forgetpwd');
    }
}
