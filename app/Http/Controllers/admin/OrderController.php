<?php

namespace App\Http\Controllers\admin;

use App\Model\Order;
use App\Model\Order_items;
use App\Model\Product;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Excel;

class OrderController extends BaseController
{
    protected $redirect_path = '/admin/orders';
    protected $fields = ['status', 'buyer_name', 'member_id', 'phone', 'shop_price', 'bill_address', 'shipping_address', 'shipping_method', 'shipping_fee', 'shipping_time_id', 'delivery_id', 'buy_date'];
    protected $error;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct()
    {
        view()->share('redirect_path', $this->redirect_path);
    }

    public function getIndex(Request $request)
    {
        $query = Order::orderBy('id', 'desc');
        if( $request->has('status') ){
            $data['lists'] = $query->where( 'status', $request->input('status') )->get();
        }else{
            $data['lists'] = $query->get();
        }

        foreach ($data['lists'] as $key => $row) {
            $data['lists'][$key]['total'] = $row->orderItems->sum(function ($p) {
                return $p['qty']*$p['price'];
            });
        }

        return view('admin.order.index', $data);
    }

    public function getAdd($type)
    {
        $data['type'] = $type;
        return view('admin.order.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            Order::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    protected function upload_file($file_name, $request)
    {
        $file_path = "";
        $allow_type = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

        if( $request->hasFile($file_name) ){
            $file = $request->file($file_name);

            $clientName = $file->getClientOriginalName();
            $entension = $file->getClientOriginalExtension();
            if( in_array($entension, $allow_type) ){
                $newName = md5(date('ymdhis').$clientName).".".$entension;
                $destinationPath = public_path().'/uploads/Order/';
                $file_path = $file->move($destinationPath, $newName) ? '/uploads/Order/'.$newName : "";
            }else{
                $this->error = '該格式文件禁止上傳!';
            }

        }
        return $file_path;
    }

    public function getEdit($id)
    {
        $data['row'] = Order::find($id);
        $data['products'] = Product::all();
        $data['total'] = $data['row']->orderItems->sum(function ($p) {
            return $p['qty']*$p['price'];
        });
        
        return view('admin.order.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();

        $row = Order::find($input_data['id']);
        $row->status = $input_data['status'];
        $row->save();
        
        return redirect($this->redirect_path."/edit/".$input_data['id'])->with('message', '修改成功!');
    }

    public function getDelete($id)
    {
        $row = Order::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Order::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function postDelItem(Request $request)
    {
        if($request->has('order_item_id')){        
            foreach ($request->input('order_item_id') as $id) {
                $row = Order_items::find($id);
                $row->delete();
            }
            return response()->json(['info' => "刪除成功"]);
        }else{
            return response()->json(['info' => "沒有動作"]);
        }
    }

    public function postAddItems(Request $request)
    {

        if($request->has('order_id')){
            $input_data = $request->all();
            $order = Order::find($input_data['order_id']);
            foreach ($input_data['product_id'] as $key => $value) {
                $product = Product::find($value);
                $insert_data = [
                    'product_id' => $value,
                    'product_name' => $product->name_cht,
                    'qty' => $input_data['qty'][$key],
                    'price' => $input_data['price'][$key]
                ];
                $orderItem = $order->orderItems()->create($insert_data);
            }
            return redirect($this->redirect_path."/edit/".$input_data['order_id'])->with('message', '補貨成功!');
        }else{
            return redirect($this->redirect_path."/edit/".$input_data['order_id'])->with('message', '補貨失敗!');
        }
    }

    public function getPrintReceipt($id)
    {
        $data['row'] = Order::with('member')->find($id);
        $data['total'] = $data['row']->orderItems->sum(function ($p) {
            return $p['qty']*$p['price'];
        });
        
        return view('admin.order.receipt', $data);
    }

    public function getPrintItemlist($id)
    {
        $data['row'] = Order::find($id);
        $data['total'] = $data['row']->orderItems->sum(function ($p) {
            return $p['qty']*$p['price'];
        });
        
        return view('admin.order.ltemlist', $data);
    }

    public function getExportCsv($id)
    {
        $data['rs'] = $row = Order::where('id', $id)->get();
        
        Excel::create('訂單', function($excel) use($data) {
            $excel->sheet('order', function($sheet) use($data) {
                $sheet->loadView('admin.order_xls', $data);
            });
        })->download('xls');
    }

    public function getBatchExportCsv(Request $request)
    {
        $ids = $request->input('order_ids');
        if( $request->has('order_ids') ) {

            $data['rs'] = Order::whereIn('id', $ids)->get();
            Excel::create('訂單', function($excel) use($data) {
                $excel->sheet('order', function($sheet) use($data) {
                    $sheet->loadView('admin.order_xls', $data);
                });
            })->download('xls');
        }else{
            return redirect($this->redirect_path)->with('message', '沒有動作!');
        }
    }
}
