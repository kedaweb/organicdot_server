<?php

namespace App\Http\Controllers\admin;

use App\Model\Cert;
use Auth;
use Validator;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class CertController extends BaseController
{
    protected $redirect_path = '/admin/cert';
    protected $fields = ['sorting', 'name_en', 'name_cht', 'type', 'is_show'];
    protected $error;

    public function __construct()
    {
        $this->dirname = 'cert';
    }

    public function getIndex(Request $request)
    {
        $type = $request->input('type');
        if($type){
            $data['lists'] = cert::where('type', $type)->get();
        }else{
            $data['lists'] = cert::all();
        }
        $data['type'] = $type;
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.cert.index', $data);
    }

    public function getAdd($type)
    {
        $data['redirect_path'] = $this->redirect_path;
        $data['type'] = $type;
        return view('admin.cert.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $messages = [
            'img.required' => '你必須上傳圖片.',
        ];
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
            'img' => 'required|image',
        ],$messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            $insert_data['img'] = $this->upload_file('img', $request);
            $this->resizeImage($insert_data['img'], 300, 100);
            cert::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    public function getEdit($id)
    {
        $data['row'] = cert::find($id);
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.cert.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = cert::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            $row->img = $this->upload_file('img', $request);
            $this->resizeImage($row->img, 300, 100);
            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = cert::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = cert::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }
}
