<?php

namespace App\Http\Controllers\admin;

use App\Model\Member;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Excel;

class MemberController extends BaseController
{
    protected $redirect_path = '/admin/members';
    protected $fields = ['name', 'email', 'phone', 'shipping_address'];
    protected $error;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $data['name']  = $name = $request->input('name');
        if( empty($name) || ! isset($name) ){
            $data['lists'] = Member::orderBy('id', 'desc')->get();
        }else{
            $data['lists'] = Member::where('name', 'like', '%'.$name.'%')->orderBy('id', 'desc')->get();
        }
        

        $data['redirect_path'] = $this->redirect_path;
        return view('admin.member.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.member.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:members',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }
            $insert_data['password'] = bcrypt($data['password']);

            Member::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    public function getEdit($id)
    {
        $data['row'] = Member::find($id);
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.member.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:members,email,'.$input_data['id'],
            'password' => 'confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Member::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }
            if(!empty($input_data['password'])){
                $row->password = bcrypt($input_data['password']);
            }

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = Member::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Member::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function getBatchExportCsv(Request $request)
    {
        $ids = $request->input('ids');
        if( $request->has('ids') ){

            $data['rs'] = Member::whereIn('id', $ids)->get();
            Excel::create('會員資料', function($excel) use($data) {
                $excel->sheet('會員', function($sheet) use($data) {
                    $sheet->loadView('admin.member_xls', $data);
                });
            })->download('xls');
    
        }else{
            return redirect($this->redirect_path)->with('message', '沒有動作!');
        }
    }
}
