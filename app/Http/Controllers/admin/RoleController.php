<?php

namespace App\Http\Controllers\admin;

use App\Model\Role;
use App\Model\Permission;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class RoleController extends BaseController
{
    use EntrustUserTrait;

    protected $redirect_path = '/admin/roles';
    protected $fields = ['name', 'display_name', 'description'];
    protected $error;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $data['lists'] = Role::orderBy('id', 'asc')->get();

        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.add', $data);
    }    

    protected function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            Role::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    protected function getEdit($id)
    {
        $data['row'] = Role::find($id);
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.edit', $data);
    }

    protected function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Role::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    protected function getDelete($id)
    {
        $role = Role::findOrFail($id); // Pull back a given role

        // Regular Delete
        $role->delete(); // This will work no matter what

        // Force Delete
        $role->users()->sync([]); // Delete relationship data
        $role->perms()->sync([]); // Delete relationship data

        $role->forceDelete();

        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Role::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function getEditPermission($id)
    {
        $data['row'] = Role::find($id);
        $data['role_permissions'] = $data['row']->permissions()->lists('id')->toArray();
        $data['lists'] = Permission::orderBy('id', 'desc')->get();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.permission', $data);
    }

    public function postEditPermission(Request $request)
    {
        $role = Role::find($request->input('id'));
        $permissions = $request->except(['_token', 'id']);
        
        $role->detachPermissions($role->permissions());
        $role->perms()->sync($permissions);

        return redirect($this->redirect_path)->with('message', '修改成功!');
    }
}
