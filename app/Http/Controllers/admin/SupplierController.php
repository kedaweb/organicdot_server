<?php

namespace App\Http\Controllers\admin;

use App\Model\Supplier;
use App\Model\Cert;
use Auth;
use Validator;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class SupplierController extends BaseController
{
    protected $redirect_path = '/admin/suppliers';
    protected $fields = ['sorting', 'name_en', 'name_cht', 'address_en', 'address_cht', 'content_en', 'content_cht', 'website', 'facebook', 'is_show', 'cert_ids'];
    protected $error;

    public function __construct()
    {
        $this->dirname = 'category';
    }

    public function getIndex()
    {
        $data['lists'] = Supplier::all();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.supplier.index', $data);
    }

    public function getAdd()
    {
        $data['certs'] = Cert::where('type', 2)->get();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.supplier.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $messages = [
            'img1.required' => '你必須上傳至少一張圖片.',
        ];
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
            'img1' => 'required|image',
            'img2' => 'image',
            'img3' => 'image',
        ],$messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            $insert_data['img1'] = $this->upload_file('img1', $request);
            $insert_data['img2'] = $this->upload_file('img2', $request);
            $insert_data['img3'] = $this->upload_file('img3', $request);

            if($insert_data['img1']){
                $this->resizeImage($insert_data['img1'], 1080, 600);
            }
            if($insert_data['img2']){
                $this->resizeImage($insert_data['img2'], 1080, 600);
            }
            if($insert_data['img3']){
                $this->resizeImage($insert_data['img3'], 1080, 600);
            }           
            Supplier::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    public function getEdit($id)
    {
        $data['row'] = Supplier::find($id);
        $data['certs'] = Cert::where('type', 2)->get();
        $data['row_certs'] = $data['row']->cert_ids;
        
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.supplier.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Supplier::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            for ($i=1; $i < 4; $i++) { 
                $f = 'img'.$i;
                if($img = $this->upload_file($f, $request)){
                    @unlink(public_path().$row->$f);
                    $row->$f = $img;
                    $this->resizeImage($img, 1080, 600);
                }
            }
            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = Supplier::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Supplier::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }
}
