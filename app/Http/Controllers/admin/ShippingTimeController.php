<?php

namespace App\Http\Controllers\admin;

use App\Model\Shipping_time;
use App\Model\System_config;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class ShippingTimeController extends BaseController
{
    protected $redirect_path = '/admin/shipping-time';
    protected $fields = ['name_en', 'name_cht', 'is_show'];
    protected $error;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $data['lists'] = Shipping_time::orderBy('id', 'desc')->get();
        $data['system_config'] = System_config::getConfig();

        $data['redirect_path'] = $this->redirect_path;
        return view('admin.shipping_time.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        $data['categories'] = Shipping_time::all();
        return view('admin.shipping_time.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            Shipping_time::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    protected function upload_file($file_name, $request)
    {
        $file_path = "";
        $allow_type = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

        if( $request->hasFile($file_name) ){
            $file = $request->file($file_name);

            $clientName = $file->getClientOriginalName();
            $entension = $file->getClientOriginalExtension();
            if( in_array($entension, $allow_type) ){
                $newName = md5(date('ymdhis').$clientName).".".$entension;
                $destinationPath = public_path().'/uploads/Shipping_time/';
                $file_path = $file->move($destinationPath, $newName) ? '/uploads/Shipping_time/'.$newName : "";
            }else{
                $this->error = '該格式文件禁止上傳!';
            }

        }
        return $file_path;
    }

    public function getEdit($id)
    {
        $data['row'] = Shipping_time::find($id);
        $data['redirect_path'] = $this->redirect_path;
        $data['categories'] = Shipping_time::all();
        return view('admin.shipping_time.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Shipping_time::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = Shipping_time::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Shipping_time::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function postSetShipping(Request $request)
    {
        $row = System_config::where('name', 'shipping_fee')->first();
        $row->value = $request->input('fee');

        $row = System_config::where('name', 'shipping_discount')->first();
        $row->value =  round ($request->input('discount'), 2);
        $row->save();

        return response()->json(['info' => '修改成功']);
    }
}
