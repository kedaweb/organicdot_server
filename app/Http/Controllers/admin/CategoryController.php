<?php

namespace App\Http\Controllers\admin;

use App\Model\Category;
use Auth;
use Validator;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{

    protected $redirect_path = '/admin/category';
    protected $fields = ['sorting', 'name_en', 'name_cht', 'parent_id', 'is_show'];
    protected $error;

    public function __construct()
    {
        $this->dirname = 'category';
    }

    public function getIndex(Request $request)
    {
        $data['id']  = $id = $request->input('id');
        $query = Category::orderBy('parent_id', 'asc')->orderBy('sorting', 'desc');
        if("all" == $id || ! isset($id)){
            $data['lists'] = $query->get();
        }else{
            $data['lists'] = $query->where('parent_id', $id)->get();
        }

        $data['top_cates'] = Category::where('parent_id', 0)->get();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.category.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        $data['categories'] = Category::where('parent_id', 0)->get();
        return view('admin.category.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            $insert_data['img'] = $this->upload_file('img', $request);
            
            Category::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    public function getEdit($id)
    {
        $data['row'] = Category::find($id);
        $data['redirect_path'] = $this->redirect_path;
        $data['categories'] = Category::where('parent_id', 0)->get();
        return view('admin.category.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Category::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            if( $img = $this->upload_file('img', $request) ){
                @unlink(public_path().$row->img);
                $row->img = $img;
            }

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = Category::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Category::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function getCategoryJson(Request $request)
    {
        $pid = $request->input('parent_id');
        $data['root'] = Category::where('parent_id', $pid)->get();

        return response()->json($data);
    }
}
