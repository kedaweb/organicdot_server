<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\Model\Product;
use App\Model\Category;
use App\Model\Supplier;
use App\Model\Cert;

use Auth;
use Validator;
use Illuminate\Http\Request;
use QrCode;

class ProductController extends BaseController
{
    protected $redirect_path = '/admin/products';
    protected $fields = ['sorting', 'name_en', 'name_cht', 'price', 'shop_price', 'category_id', 'supplier_id', 'stock_status', 'stock', 'content_en', 'content_cht',  'content_choose_en', 'content_choose_cht',  'content_nutrition_en', 'content_nutrition_cht',  'content_effect_en', 'content_effect_cht',  'content_save_en', 'content_save_cht',  'shipping_detail_en', 'shipping_detail_cht', 'weight_unit_en', 'weight_unit_cht', 'is_show', 'recommend', 'cert_ids'];
    protected $error;

    public function __construct()
    {
        $this->dirname = 'products';
    }

    public function getIndex(Request $request)
    {
        $data['name']  = $name = $request->input('name');
        $data['category_id']  = $category_id = $request->input('category_id');
        $where = array();

        $query = Product::orderBy('id', 'desc');
        if( $request->has('name') ) {
            $query->where('name_cht', 'like', '%'.$name.'%');
        }
        if( $request->has('category_id') ) {
            $query->where('category_id', $category_id);
        }else if( $request->has('parent_id') && $request->input('parent_id') && !$request->has('category_id') ){
            $children_category = Category::where('parent_id', $request->input('parent_id'))->lists('id');
            $query->whereIn('category_id', $children_category);
        }
        $data['lists'] = $query->get();
        $data['qrcode_dir'] = "uploads/qrcodes/";

        $data['top_cates'] = Category::where('parent_id', 0)->get();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.product.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        $data['top_cates'] = Category::where('parent_id', 0)->get();
        $data['suppliers'] = Supplier::all();
        $data['certs'] = Cert::where('type', 1)->get();
        return view('admin.product.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $messages = [
            'img1.required' => '你必須上傳至少一張圖片.',
        ];
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
            'category_id' => 'required',
            'img1' => 'required|image',
            'img2' => 'image',
            'img3' => 'image',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            $insert_data['img1'] = $this->upload_file('img1', $request);
            $insert_data['img2'] = $this->upload_file('img2', $request);
            $insert_data['img3'] = $this->upload_file('img3', $request);
            $insert_data['stock'] = $this->calculate_stock($request);
            if($insert_data['img1']){
                $this->resizeImage($insert_data['img1'], 1080, 600);
            }
            if($insert_data['img2']){
                $this->resizeImage($insert_data['img2'], 1080, 600);
            }
            if($insert_data['img3']){
                $this->resizeImage($insert_data['img3'], 1080, 600);
            }

            $product = Product::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    protected function calculate_stock($request)
    {
        $stock = $request->input('stock') + $request->input('increase_stock') - $request->input('loss_stock');
        $stock = $stock < 0 ? 0 : $stock;
        return $stock;
    }

    public function getEdit($id)
    {
        $data['row'] = Product::find($id);
        $data['row_certs'] = $data['row']->cert_ids;
        $data['redirect_path'] = $this->redirect_path;

        $data['top_cates'] = Category::where('parent_id', 0)->get();
        $data['suppliers'] = Supplier::all();
        $data['certs'] = Cert::where('type', 1)->get();

        return view('admin.product.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Product::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            for ($i=1; $i < 4; $i++) { 
                $f = 'img'.$i;
                if($img = $this->upload_file($f, $request)){
                    @unlink(public_path().$row->$f);
                    $row->$f = $img;
                    $this->resizeImage($img, 1080, 600);
                }
            }
            $row->stock = $this->calculate_stock($request);

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        $row = Product::find($id);
        $row->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Product::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function postSetRecommend(Request $request)
    {
        $response = ['success' => FALSE, 'info' => "沒有動作"];
        $product_id = $request->input('product_id');
        $recommend = $request->input('value');

        $row = Product::find($product_id);
        if($request->has('product_id') || $row) {
            if(preg_match ("/10|[1-9]/", $recommend)){            
                $query = Product::where('category_id', $row->category_id)->where('recommend', $recommend);
                if(count($query->get()) > 0){
                    $response['info'] = "已存在相同推介";
                }else{            
                    $row->recommend = $recommend;
                    $row->save();
                    $response = ['success' => TRUE, 'info' => "修改成功"];
                }
            }else{
                $row->recommend = 0;
                $row->save();
                $response = ['success' => TRUE, 'info' => "修改成功"];
            }
        }
        return response()->json($response);
    }

    public function getCopy(Request $request)
    {
        $product_ids = $request->input('product_ids');
        foreach ($product_ids as $id) {
            $data = Product::find($id)->toArray();
            unset($data['recommend']);
            Product::create($data);
        }

        return redirect($this->redirect_path)->with('message', '複製成功!');
    }

    public function getBatchEdit(Request $request)
    {
        $data['ids'] = $product_ids = $request->input('product_ids');
        if( $request->has('product_ids') ){
            $data['row'] = Product::find($product_ids[0]);
            $data['redirect_path'] = $this->redirect_path;

            $data['top_cates'] = Category::where('parent_id', 0)->get();
            $data['suppliers'] = Supplier::all();
            $data['certs'] = Cert::where('type', 1)->get();

            return view('admin.product.batch', $data);
        }else{
            return redirect($this->redirect_path)->with('message', '沒有動作!');
        }
    }

    public function postBatchEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name_en' => 'required|max:255',
            'name_cht' => 'required|max:255',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            foreach ($input_data['ids'] as $key => $id) {
                $row = Product::find($id);

                foreach ($this->fields as $key => $field) {
                    if( isset($input_data[$field]) ) {
                        $row->$field = $input_data[$field];
                    }
                }

                for ($i=1; $i < 4; $i++) { 
                    $f = 'img_'.$i;
                    if($img = $this->upload_file($f, $request)){
                        @unlink(public_path().$row->$f);
                        $row->$f = $img;
                    }
                }
                $row->stock = $this->calculate_stock($request);
                $row->cert_ids = empty($input_data['cert_ids']) ? '' : implode(',', $input_data['cert_ids']);

                $row->save();
            }


            return redirect($this->redirect_path)->with('message', '修改成功!');
        }
    }

    public function postActiveRecommend(Request $request)
    {
        $response['root'] = array();
        if($request->has('category_id')){
            $result = Product::where('category_id', $request->input('category_id'))->where('recommend', "<>", "0")->lists('recommend')->toArray();

            $collection = [1,2,3,4,5,6,7,8,9,10];
            $filtered = [];
            foreach ($collection as $key => $value) {
                if( !in_array($value, $result) ){
                    $filtered[] = $value ;
                }
            }
            $response['root'] = $filtered;
        }
        return response()->json($response);
    }
}
