<?php

namespace App\Http\Controllers\api;

use App\Model\System_config;
use App\Model\Shipping_time;
use App\Model\Pickup_location;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Lang;
use MaLocal;

class SystemController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getConfig(Request $request)
    {
        $response['root'] = system_config::getConfig();
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getShippingTime(Request $request)
    {
        $response['root'] = Shipping_time::where('is_show', 1)->get();
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getPickup(Request $request)
    {
        $response['root'] = Pickup_location::where('is_show', 1)->get();
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getLanguageJson(Request $request)
    {
        $language = $request->has('language') ? $request->input('language') : config('app.local');
        MaLocal::setLocale($language);
        $response['root'] = Lang::get('front');
        return response()->json($response)->setCallback($request->input('callback'));
    }
}
