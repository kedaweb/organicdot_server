<?php

namespace App\Http\Controllers\api;

use App\Model\Order;
use App\Model\Order_items;
use App\Model\System_config;
use App\Model\Product;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Hash;

class OrderController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getId(Request $request, $id)
    {
        $order = Order::find($id);
        $response['root'] = $order;
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getToken(Request $request, $token)
    {
        $order = Order::where('paypal_token', $token)->first();
        $response['root'] = $order;
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function putCreate(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('cart')){
            $input = $request->all();
            $inputOrder = json_decode($input['order'], true);
            $shipping_fee = $inputOrder['shipping_method'] == 1 ? system_config::where('name', 'shipping_fee')->first()->value : 0;
            $order_data = $inputOrder;
            $order_data['buy_date'] = date('Y-m-d H:i:s');

            $items = [];
            $total = 0; //計算訂單總價
            foreach ($input['cart'] as $item) {
                $item = json_decode($item);
                $orderItem = new order_items([
                    "product_name" => $item->name_cht,
                    "product_id" => $item->id,
                    "qty" => $item->qty,
                    "price" => $item->price
                ]);
                $orderItem->product->stock -= $item->qty;
                if($orderItem->product->stock <= 0){
                    $orderItem->product->stock_status = 0;
                }
                $orderItem->product->save();
                $items[] = $orderItem;
                $total += $item->qty*$item->price;
            }

            $order_data['shipping_fee'] = $total >= 300 ? 0 : $shipping_fee;

            $order = Order::create($order_data);
            $order->orderItems()->saveMany($items);
            $response['root'] = $order;
            $response['success'] = $order ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function postUpdate(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('order')){

            $input_order = json_decode($request->input('order'));

            $order = Order::find($input_order->id);
            unset($input_order->id);
            foreach ($input_order as $key => $item) {
                switch ($key) {
                    case 'id':
                        break;
                    case 'status':
                        $order->$key = 1;
                        break;
                    case 'shipping_time_id':
                        $collection = collect($item);
                        $filtered = $collection->reject(function ($item) {
                            return $item == 'false' || $item == '' || $item == null;
                        });
                        $order->$key = $filtered->toArray();
                        break;
                    default:
                        $order->$key = $input_order->$key;
                        break;
                }
            }

            $response['success'] = $order->save() ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }
}
