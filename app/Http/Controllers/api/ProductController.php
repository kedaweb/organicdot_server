<?php

namespace App\Http\Controllers\api;

use App\Model\Product;
use App\Model\Cert;
use App\Model\Category;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;
use QrCode;

class ProductController extends BaseController
{
    protected $fields = ['sorting', 'name_en', 'name_cht', 'parent_id', 'is_show'];

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getForFacebook($id, $lang = "cn")
    {
        $root = Product::where('id', $id)->first();
        $data['lang'] = $lang == "cn" ? "cht" : "en";
        $data['locale'] = $lang;
        $data['product'] = $root;
        return view('product.detail', $data);
    }

    public function getId(Request $request, $id)
    {
        $root = Product::with('supplier')->where('id', $id)->first();
        $root['certs'] = $root->certs();
        $qrcode_dir = "uploads/qrcodes/";
        $qrcode_file = $qrcode_dir.$root->id.'.png';
        QrCode::format('png')->margin(1)->size(150)->encoding('UTF-8')->generate('product_'.$root->id, public_path($qrcode_file));
        $root['qrcode'] = url($qrcode_file);
        $response['root'] = $root;
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getRecommend(Request $request)
    {
        $categories = Category::with('children')->orderBy('sorting', 'desc')->where('parent_id', 0)->where('is_show', 1)->get();
        foreach ($categories as $key => $category) {    
            $root[$key]['category'] = $category;
            $root[$key]['products'] = Product::orderBy('recommend', 'desc')->orderBy('sorting', 'desc')->whereIn('category_id', $category->children->lists('id'))->where('is_show', 1)->get();
        }

        $response['root'] = $root;
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getRec(Request $request)
    {
        $take_num = $request->has('number') ? $request->input('number') : 4;
        $query = Product::orderBy('sorting', 'desc')->orderBy('recommend', 'desc')->where('is_show', 1);
        $root = $query->take($take_num)->get();

        $response['root'] = $root;
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getCategory(Request $request)
    {
        $root = [];
        if($request->has('category_id')) {
            $root = Product::where('category_id', $request->input('category_id'))->where('id', '<>', $request->input('product_id'))->where('is_show', 1)->take(4)->get();
        }

        $response['root'] = $root;
        return response()->json($response)->setCallback($request->input('callback'));
    }
}
