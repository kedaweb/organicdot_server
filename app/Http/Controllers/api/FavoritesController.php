<?php

namespace App\Http\Controllers\api;

use App\Model\Favorites;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;

class FavoritesController extends BaseController
{
    protected $fields = ['member_id', 'product_id'];

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getCreate(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('member_id') && $request->has('product_id')){        
            $f = Favorites::create([
                'member_id' => $request->input('member_id'),
                'product_id' => $request->input('product_id')
            ]);
            $response['success'] = $f ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getDelete(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('member_id') && $request->has('product_id')){
            $f = Favorites::where('member_id', $request->input('member_id'))->where('product_id', $request->input('product_id'))->first();
            $f->delete();
            $response['success'] = TRUE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }
}
