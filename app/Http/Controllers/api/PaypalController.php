<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\abstractPaypalControllers;
use Log;

class PaypalController extends abstractPaypalControllers {

    public function __construct() 
    {
        parent::__construct();
    }

    public function getIndex(Request $request) 
    {
        $data['fail'] = true;
        return view('paypal.success', $data);
    }

    public function getSec(Request $request)
    {
        $sToken = $request->input('token');
        $sPayerID = $request->input('PayerID');
        
        $data['fail'] = true;
        if ($sToken == '' && $sPayerID == '') {
            return view('paypal.success', $data);
        } else {
            //獲取支付訂單的信息
            $ECparams = $this->params;
            $ECparams['METHOD'] ='GetExpressCheckoutDetails';
            $ECparams['TOKEN'] = $sToken;

            $response = curlGet($this->paypalNvp, $ECparams);
            $ECDetails = convertUrlQuery( urldecode($response) );
            
            if('Success' == $ECDetails['ACK'])
            {

                $ECparams['METHOD'] = 'DoExpressCheckoutPayment';
                $ECparams['PAYERID'] = $ECDetails['PAYERID'];
                $ECparams['PAYMENTREQUEST_0_AMT'] = $ECDetails['AMT'];
                $ECparams['PAYMENTREQUEST_0_CURRENCYCODE'] = $ECDetails['CURRENCYCODE'];
                $ECparams['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
                $response = curlGet($this->paypalNvp, $ECparams);
                $ECPayment = convertUrlQuery( urldecode($response) );

                //支付成功.支付成功時才創建訂單
                if('Success' == $ECPayment['ACK']){
                    
                    $this->saveOrder($ECDetails);

                    $data['fail'] = false;
                    return view('paypal.success', $data);
                }else{ 
                    //支付失敗.記錄日誌
                    Log::info('User failed to use paypal', ['orderInfo' => $ECDetails, 'context' => $ECPayment]);
                }
            }
        }
        return view('paypal.success', $data);
    }
}
