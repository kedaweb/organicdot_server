<?php

namespace App\Http\Controllers\api;

use App\Model\Member;
use App\Model\Favorites;
use App\Model\Order;
use App\Model\order_items;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Hash;

class MemberController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //由于某个版本客户要求phone为必填.所以,接口修改使用phone作检测字段
    //现在由于有部分用户还是使用旧版本,为了兼容旧版现在只使用id直接就能获取数据
    public function postId(Request $request, $id)
    {
        $response['root'] = [];
        // if( $request->has('phone') ){
            // $member = Member::with('favorites')->where('phone', $request->input('phone'))->where('id', $id)->first();
        // }
        $member = Member::with('favorites')->where('id', $id)->first();
        $response['root'] = $member;

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getFavorites(Request $request)
    {
        $response['root'] = [];
        if( $request->has('member_id') ){
            $f = Favorites::with(['product' => function ($query){
                $query->where('is_show', 1);
             }])->where('member_id', $request->input('member_id'))->get();
            foreach ($f as $key => $item) {
                if( empty($item->product) ){
                    unset($f[$key]);
                }
            }
            $response['root'] = $f;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function putFavorites(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('member_id') && $request->has('product_id')){
            $f = Favorites::firstOrCreate([
                'member_id' => $request->input('member_id'),
                'product_id' => $request->input('product_id')
            ]);
            $response['success'] = $f ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function deleteFavorites(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('member_id') && $request->has('product_id')){
            $f = Favorites::where('member_id', $request->input('member_id'))->where('product_id', $request->input('product_id'))->first();
            if($f){
                $f->delete();
                $response['success'] = TRUE;
            }
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getOrders(Request $request)
    {
        $response['root'] = [];
        if( $request->has('member_id') ){
            $orders = Order::with('orderItems', 'orderItems.product')->where('member_id', $request->input('member_id'))->orderBy('id', 'desc')->get();
            $response['root'] = $orders;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function postProfile(Request $request)
    {
        $response['success'] = FALSE;
        $language = $request->has('language') ? $request->input('language') : 'cn';
        $messages = [];
        if($language == 'cn'){        
            $messages = [
                'email.required'    => '請填寫電郵',
                'email.email'    => '電郵地址不規範.',
                'email.unique' => '該電郵地址已被使用',
                'password.required'      => '請填寫密碼',
                'password.confirmed'      => '密碼不一致',
                'password.min'      => '密碼最少為6位',
            ];
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:members,email,'.$request->input('id'),
            'password' => 'confirmed|min:6'
        ],$messages);

        if ($validator->fails()) {
            $response['info'] = $validator->messages();
        }else{
            $m = Member::find($request->input('id'));
            $m->name = $request->input('name');
            $m->email = $request->input('email');
            // $m->phone = $request->input('phone');
            if( !empty($request->input('password')) ){
                $m->password = bcrypt($request->input('password'));
            }
            $m->shipping_address = $request->input('shipping_address');
            $m->bill_address = $request->input('bill_address');
            $m->save();

            $response['root'] = $m;
            $response['success'] = $m ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }
}
