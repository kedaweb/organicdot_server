<?php

namespace App\Http\Controllers\api;

use App\Model\Category;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    protected $redirect_path = '/api/category';
    protected $fields = ['sorting', 'name_en', 'name_cht', 'parent_id', 'is_show'];
    protected $error;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $query = Category::with(['children' => function ($query){
                $query->where('is_show', '1')->orderBy('sorting', 'desc');
        }])
        ->where('is_show', '1')->orderBy('sorting', 'desc')->orderBy('parent_id', 'asc')->orderBy('id', 'asc');
        
        if( $request->has('parent_id') ) {
            $query->where('parent_id', $request->input('parent_id'));
            $response['root'] = $query->take(5)->get();
        }else{
            $response['root'] = $query->get();
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getId(Request $request, $id)
    {
        $root = Category::with(['products' => function ($query){
                $query->where('is_show', 1);
        }])->where('id', $id)->first();

        $root_arr = $root->toArray();
        if( empty($root_arr['products']) ){
            $root_arr['products'] = $root->children_products();
        }
        $response['root'] = $root_arr;
        return response()->json($response)->setCallback($request->input('callback'));
    }
}
