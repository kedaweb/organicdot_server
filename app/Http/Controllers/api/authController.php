<?php

namespace App\Http\Controllers\api;

use App\Model\Member;
use App\Model\Member_pwd_resets as MPresets;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Hash;
use Mail;

class authController extends BaseController
{
    protected $fields = ['member_id', 'product_id'];

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $this->messages = [
            'email.required'    => '請填寫電郵',
            'email.email'    => '電郵地址不規範.',
            'email.unique' => '該電郵地址已被使用',
            'phone.required' => '請填寫電話號碼',
            'phone.unique' => '該電話號碼已被使用',
            'password.required'      => '請填寫密碼',
            'password.confirmed'      => '密碼不一致',
            'password.min'      => '密碼最少為6位',
        ];
    }

    public function postReg(Request $request)
    {
        $language = $request->has('language') ? $request->input('language') : 'cn';
        $messages = [];
        if($language == 'cn'){        
            $messages = $this->messages;
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:255|email|unique:members',
            'password' => 'required|confirmed|min:6'
        ], $messages);

        $response['success'] = FALSE;
        if ($validator->fails()) {
            $response['info'] = $validator->messages();
        }else{
            $m = Member::create([
                'email' => $request->input('email'),
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'areacodes' => $request->input('areacodes'),
                'shipping_address' => $request->input('shipping_address'),
                'password' => bcrypt($request->input('password')),
            ]);

            $response['root'] = $m;
            $response['success'] = $m ? TRUE : FALSE;
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function postLogin(Request $request)
    {
        $response['success'] = FALSE;
        if($request->has('email') && $request->has('password')){
            $member = Member::with('favorites')->where('email', $request->input('email'))->first();
            if($member && Hash::check($request->input('password'), $member->password)){
                $response['root'] = $member;
                $response['success'] = TRUE;
            }
        }
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getResetPwd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'token' => 'required',
        ]);
        $data['status'] = 0;
        $data['token'] = $request->input('token');
        $data['email'] = $request->input('email');
        $data['locale'] = $request->has('locale') ? $request->input('locale') : 'cn';

        if ($validator->fails()) {
            $data['status'] = 0;
        }else{
            $row = MPresets::where('email', $request->input('email'))->where('token', $request->input('token'))->first();
            if($row){
                $expirationTime = strtotime($row->created_at) + config('auth.password.expire')*60;
                if($expirationTime < time()){
                    $data['status'] = 1;
                    MPresets::where('email', $request->input('email'))->delete();
                }else{
                    $data['status'] = 2;
                }
            }else{
                $data['status'] = 1;
            }
        }
        return view("member.resetpwd", $data);
    }

    public function postResetPwd(Request $request)
    {
        $response['locale'] = $request->has('locale') ? $request->input('locale') : 'cn';
        $response['post_status'] = FALSE;

        $messages = [];
        if($response['locale'] == 'cn'){        
            $messages = $this->messages;
        }
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ],$messages);

        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $row = Member::where('email', $credentials['email'])->first();
            $row->password = bcrypt($credentials['password']);
            $row->save();
            MPresets::where('email', $credentials['email'])->delete();
            return 'cn' == $response['locale'] ? '修改成功!' : 'Success!';
        }

    }

    public function postForgetPassword(Request $request)
    {
        $response['error_code'] = 0;
        $locale = $request->has('locale') ? $request->input('locale') : "cn";
        $messages = [];
        if($locale== 'cn'){        
            $messages = $this->messages;
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ],$messages);

        if ($validator->fails()) {
            $response['error_code'] = 1;
            $response['info'] = $validator->messages();
        }else{
            $input_data = $request->only('email');
            $m = Member::where('email', $input_data['email'])->first();
            if($m){
                $token = $this->createNewToken();
                MPresets::where('email', $input_data['email'])->delete();
                MPresets::create([
                    'email' => $input_data['email'],
                    'token' => $token
                ]);
                $data = ['email'=>$input_data['email'], 'locale'=>$locale, 'token'=>$token];
                Mail::send('forgetpwdmail', $data, function($message) use($data)
                {
                    if($data['locale'] == "cn"){
                        $subject = "[有機點] 密碼重設";
                        $name = "顧客";
                    }else{
                        $subject = "[有機點] Password Reset";
                        $name = "Customer";
                    }
                    $message->to($data['email'], $name)->subject($subject);
                });
            }else{
                $response['error_code'] = 404;
            }
        }

        return response()->json($response)->setCallback($request->input('callback'));
    }

    protected function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), config('app.key'));
    }
}
