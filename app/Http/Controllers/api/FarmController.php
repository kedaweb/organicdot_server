<?php

namespace App\Http\Controllers\api;

use App\Model\Supplier;
use App\Model\Product;
use Auth;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;

class FarmController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $response['root'] = Supplier::all();
        return response()->json($response)->setCallback($request->input('callback'));
    }

    public function getId(Request $request, $id)
    {
        $root = Supplier::with(['products' => function ($query){
            $query->orderBy('sorting', 'desc');
        },'products.category'])->where('id', $id)->first();

        $root['certs'] = $root->certs();
        $collection = collect($root['products']); 
        $root['grouped'] = $collection->groupBy('category_id');
        
        $response['root'] = $root;
        return response()->json($response)->setCallback($request->input('callback'));
    }
}
