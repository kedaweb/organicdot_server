<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Event::listen("illuminate.query", function($query, $bindings){
//     var_dump($query);//sql 预处理 语句
//     var_dump($bindings);// 替换数据
//     echo "</br>";
// });

Route::get('/', function(){
    return "執行成功";
});

//api接口路由
Route::group(['prefix' => 'api','namespace' => 'api'],function() {
    Route::controller('auth', 'authController');
    Route::controller('sys', 'SystemController');
    Route::controller('category', 'CategoryController');
    Route::controller('products', 'ProductController');
    Route::controller('farms', 'FarmController');
    Route::controller('member', 'MemberController');
    Route::controller('order', 'OrderController');
    Route::controller('paypal', 'PaypalController');
});
// forget password email routes
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['prefix' => 'admin','namespace' => 'Auth'],function() {
    Route::controller('auth', 'AuthController');
});

//后台权限
Entrust::routeNeedsPermission('admin/users*', "users-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/roles*', "roles-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/products*', "products-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/suppliers*', "suppliers-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/category*', "category-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/cert*', "cert-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/orders*', "orders-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/pickup-location*', "pickup-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/shipping-time*', "shippingtime-admin", redirect('/admin/'));
Entrust::routeNeedsPermission('admin/members*', "members-admin", redirect('/admin/'));
// 后台控制器路由
Route::group(
    ['middleware' => "auth", 'prefix' => 'admin', 'namespace' => 'admin'],
    function() {
        Route::get('/', function(){
            $user = Auth::user();
            foreach ($user->role()->permissions() as $perm) {
                return redirect($perm->url);
            }
        });
        Route::controller('users', 'UserController');
        Route::controller('suppliers', 'SupplierController');
        Route::controller('cert', 'CertController');
        Route::controller('category', 'CategoryController');
        Route::controller('shipping-time', 'ShippingTimeController');
        Route::controller('members', 'MemberController');
        Route::controller('products', 'ProductController');
        Route::controller('orders', 'OrderController');
        Route::controller('pickup-location', 'PickupLocationController');
        Route::controller('roles', 'RoleController');
    }
);

Route::group([
        'prefix' => MaLocal::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ], 
    function() {
        Route::get('/', 'frontController@getWelcome');
        Route::get('/welcome', 'frontController@getSetLanguage');
        Route::get('/home', 'frontController@getIndex');
        Route::get('/recommend', 'frontController@getRecommend');
        Route::get('/category/{id}', 'frontController@getCategory');
        Route::get('/product/{id}', 'frontController@getProduct');
        Route::get('/farms', 'frontController@getFarms');
        Route::get('/farm/{id}', 'frontController@getFarm');
        Route::get('/cart', 'frontController@getCart');
        Route::get('/contact', 'frontController@getContact');
        Route::get('/privacy-policy', 'frontController@getPrivacyPolicy');
        Route::controller('member', 'memberController');
        Route::get('/paypal-return', 'frontController@getSec');
        Route::get('/shipping', 'frontController@getShipping');
        Route::get('/pickup', 'frontController@getPickup');
        Route::get('/helpyourself', 'frontController@getHelpyourself');
    }
);
