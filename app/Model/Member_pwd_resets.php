<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Member_pwd_resets extends Model
{
    protected $table = 'member_pwd_resets';
    
    protected $fillable = ['email', 'token'];
}
