<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class system_config extends Model
{
    protected $table = 'system_config';
    
    protected $fillable = ['name', 'value'];

    static function getConfig()
    {
        $arr = [];
        foreach (System_config::all() as $key => $value) {
            $arr[$value->name] = $value->value;
        }
        return $arr;
    }
}
