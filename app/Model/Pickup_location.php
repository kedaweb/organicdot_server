<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pickup_location extends Model
{
    protected $table = 'pickup_location';
    
    protected $fillable = ['name_en', 'name_cht', 'address_en', 'address_cht', 'is_show'];
}
