<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    protected $table = 'favorites';

protected $fillable = ['product_id', 'member_id'];

    public function product()
    {
      return $this->belongsTo('App\Model\Product',"product_id", "id");
    }

    public function member()
    {
        return $this->belongsTo('App\Model\Member',"member_id", "id");
    }
}
