<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';
    protected $guarded = ['id'];

    protected $hidden = ['password'];

    public function favorites()
    {
        return $this->hasMany('App\Model\Favorites', 'member_id', 'id');
    }
}
