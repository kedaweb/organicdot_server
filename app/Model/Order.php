<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['status', 'buyer_name', 'member_id', 'phone', 'bill_address', 'shipping_address', 'shipping_method', 'shipping_fee', 'shipping_time_id', 'delivery_id', 'buy_date', 'paypal_token', 'payment_method', 'email'];

    public function getShippingTimeIdAttribute($value)
    {
       return $value ? explode(',', $value) : null; 
    }

    public function setShippingTimeIdAttribute($value)
    {
        $value = array_filter($value);
        $this->attributes['shipping_time_id'] = $value ? implode(',', $value) : "";
    }

    public function getShippingMethodAttribute($value)
    {
        switch ($value) {
            case 1:
                $str = '一般運費';
                break;
            case 2:
                $str = '到提貨點';
                break;
            case 3:
                $str = '自取';
                break;
            default:
                $str = "";
                break;
        }
        return $str;
    }

    public function orderItems()
    {
        return $this->hasMany('App\Model\Order_items', 'order_id', 'id');
    }

    public function member()
    {
        return $this->hasOne('App\Model\Member', 'id', 'member_id');
    }

    public function shippingTimes()
    {
        $result = Shipping_time::whereIn('id', explode(',', $this->attributes['shipping_time_id']))->get();
        return $result ? $result : FALSE;
    }

    public function pickupLocation()
    {
        return $this->hasOne('App\Model\Pickup_location', 'id', 'delivery_id');
    }

}
