<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';

    protected $fillable = ['sorting', 'cert_ids', 'name_en', 'name_cht', 'address_en', 'address_cht','content_en', 'content_cht', 'website', 'facebook', 'is_show', 'img1', 'img2', 'img3','cert_ids'];

    public function getImg1Attribute($value)
    {
       return $value ? url($value) : "";
    }    

    public function getImg2Attribute($value)
    {
       return $value ? url($value) : "";
    }

    public function getImg3Attribute($value)
    {
       return $value ? url($value) : "";
    }

    public function getCertIdsAttribute($value)
    {
       return explode(',', $value);
    }

    public function setCertIdsAttribute($value)
    {
       $this->attributes['cert_ids'] = $value ? implode(',', $value) : "";
    }

    public function certs()
    {
        $certs = Cert::whereIn('id', explode(',', $this->attributes['cert_ids']))->get();
        return $certs ? $certs : FALSE;
    }

    public function products()
    {
        return $this->hasMany('App\Model\Product', 'supplier_id', 'id');
    }
}
