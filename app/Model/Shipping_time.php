<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shipping_time extends Model
{
    protected $table = 'shipping_time';
    
    protected $fillable = ['name_en', 'name_cht', 'is_show'];
}
