<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class order_items extends Model
{
    protected $table = 'order_items';

    protected $fillable = ['order_id', 'product_id', 'qty', 'product_name', 'price'];

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'id', 'order_id');
    }

    public function product()
    {
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
}
