<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    
    protected $fillable = ['sorting', 'name_en', 'name_cht', 'parent_id', 'is_show', 'img'];

    public function getImgAttribute($value)
    {
       return $value ? url($value) : "";
    }    

    public function category() 
    {
        return $this->belongsTo('App\Model\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Model\Category', 'parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('App\Model\Product', 'category_id', 'id');
    }

    public function children_products()
    {
        if($this->parent_id == 0){
            $children_ids = $this->children()->lists('id');
            return Product::whereIn('category_id', $children_ids)->get();
        }else{
            return [];
        }
    }
}
