<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cert extends Model
{
    protected $table = 'certification';

    protected $fillable = ['sorting', 'name_en', 'name_cht', 'type', 'is_show', 'img'];

    public function getImgAttribute($value)
    {
       return url($value);
    }
}
