<?php 
namespace App\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public function permissions()
    {
        $permissions = Permission::join('permission_role', 'permissions.id', '=', 'permission_role.permission_id')
            ->where('permission_role.role_id', '=', $this->id)
            ->get();
        if($permissions){
          return $permissions;
        }else{
          return FALSE;
        }
    }
}