<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = ['id'];

    public function getImg1Attribute($value)
    {
       return $value ? url($value) : "";
    }    

    public function getImg2Attribute($value)
    {
       return $value ? url($value) : "";
    }

    public function getImg3Attribute($value)
    {
       return $value ? url($value) : "";
    }

    public function getCertIdsAttribute($value)
    {
       return explode(',', $value);
    }

    public function setCertIdsAttribute($value)
    {
       $this->attributes['cert_ids'] = $value ? implode(',', $value) : "";
    }

    public function supplier()
    {
        return $this->belongsTo('App\Model\Supplier',"supplier_id", "id");
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category',"category_id", "id");
    }

    public function certs()
    {
        $certs = Cert::whereIn('id', explode(',', $this->attributes['cert_ids']))->get();
        return $certs ? $certs : FALSE;
    }
}
