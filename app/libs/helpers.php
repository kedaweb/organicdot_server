<?php 

/** 
 * Returns the url query as associative array 
 * 
 * @param    string    query 
 * @return    array    params 
 */
function convertUrlQuery($query)
{ 
    $queryParts = explode('&', $query); 
     
    $params = array(); 
    foreach ($queryParts as $param) 
    { 
        $item = explode('=', $param); 
        $params[$item[0]] = $item[1]; 
    } 
    return $params; 
}

function curlPost($url, $postData)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $output = curl_exec($ch);
    curl_close($ch);

    return ($output);
}

function curlGet($url, $getData)
{
    $params = http_build_query($getData);
    $url = $url."/?".$params;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    $output = curl_exec($ch) ; 

    return ($output);
}

 ?>